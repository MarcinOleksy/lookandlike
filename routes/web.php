<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//
//    Route::name('dashboard.')->prefix('/panel')->group(function (){
//
//        Route::name('users.')->prefix('/uzytkownicy')->group(function (){
//            Route::get('/', 'Dashboard\UserController@index')->name('index');
//            Route::get('/create', 'Dashboard\UserController@create')->name('create');
//            Route::post('/create', 'Dashboard\UserController@store')->name('store');
//            Route::get('/{user_id}', 'Dashboard\UserController@edit')->name('edit');
//            Route::put('/{user_id}', 'Dashboard\UserController@update')->name('update');
//            Route::delete('/{user_id}', 'Dashboard\UserController@destroy')->name('delete');
//        });
//
//        Route::name('locals.types.')->prefix('/typy-lokali')->group(function (){
//            Route::get('/', 'Dashboard\LocalTypeController@index')->name('index');
//            Route::get('/create', 'Dashboard\LocalTypeController@create')->name('create');
//            Route::post('/create', 'Dashboard\LocalTypeController@store')->name('store');
//            Route::get('/{type_id}', 'Dashboard\LocalTypeController@edit')->name('edit');
//            Route::put('/{type_id}', 'Dashboard\LocalTypeController@update')->name('update');
//            Route::delete('/{type_id}', 'Dashboard\LocalTypeController@destroy')->name('delete');
//        });
//
//        Route::name('locals.')->prefix('/lokale')->group(function (){
//            Route::get('/', 'Dashboard\LocalController@index')->name('index');
//            Route::get('/create', 'Dashboard\LocalController@create')->name('create');
//            Route::post('/create', 'Dashboard\LocalController@store')->name('store');
//            Route::get('/edit/{premise_id}', 'Dashboard\LocalController@edit')->name('edit');
//            Route::put('/edit/{premise_id}', 'Dashboard\LocalController@update')->name('update');
//            Route::delete('/{premise_id}', 'Dashboard\LocalController@destroy')->name('delete');
//        });
//    });
//
//
//});
