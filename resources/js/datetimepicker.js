require('bootstrap4-datetimepicker');

$(document).ready(function() {
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'pl',
        icons: icons()
    });

    $('.datetimepicker').datetimepicker({
        format: 'DD-MM-YYYY HH:mm',
        locale: 'pl',
        icons: icons()
    });

    $('.timepicker').each(function() {
        $(this).datetimepicker({
            format: 'HH:mm',
            locale: 'pl',
            stepping: parseInt($(this).attr('data-timepicker-step')) || 1,
            icons: icons()
        });
    });

    $('.timepickerDefault').each(function() {
        $(this).datetimepicker({
            format: 'HH:mm',
            locale: 'pl',
            stepping: parseInt($(this).attr('data-timepicker-step')) || 1,
            icons: icons(),
            defaultDate: new Date("2019-07-02 03:00"),
        });
    });

    $('.timefortaskDefault').each(function () {
        $(this).datetimepicker({
            format: 'HH:mm',
            locale: 'pl',
            stepping: parseInt($(this).attr('data-timepicker-step')) || 1,
            icons: icons(),
            defaultDate: new Date("2019-07-02 00:20"),
        });
    });

    function icons()
    {
        return {
            time: 'far fa-clock',
            date: 'far fa-calendar-alt',
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-arrow-left',
            next: 'fas fa-arrow-right',
            today: 'fas fa-calendar-times',
            clear: 'fas fa-trash-alt',
            close: 'fas fa-times'
        }
    }
});