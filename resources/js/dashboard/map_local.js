$(document).ready(function(){

    L.Icon.Default.prototype.options.shadowSize = [0,0];

    var zoom = 6;
    var center = [52.2, 19.636713];
    var marker;

    var longitude = parseFloat($('#myMap').data('longitude'));
    var latitude = parseFloat($('#myMap').data('latitude'));

    if(!(isNaN(longitude) || isNaN(latitude)))  {
        zoom = 16;
        center = [latitude, longitude];
        marker = new L.marker({lat: latitude, lng: longitude});
    }

    var map = L.map('myMap').setView(center, zoom);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    if(marker)
        marker.addTo(map);

    putMarker(marker,map);
});

function putMarker(marker,map) {

    map.on('click', function (e) {
        if(marker)
            map.removeLayer(marker);

        marker = new L.marker(e.latlng);

        map.addLayer(marker);

        changeLatLngFields(e.latlng);
    });
}

function changeLatLngFields(latlng) {
    $('input#longitude').val(latlng.lng);
    $('input#latitude').val(latlng.lat);
}