$(document).ready(function(){
    L.Icon.Default.prototype.options.shadowSize = [0,0];

    var longitude = parseFloat($('#myMap').data('longitude'));
    var latitude = parseFloat($('#myMap').data('latitude'));

    var center = [latitude, longitude];

    var map = L.map('myMap').setView(center, 16);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var marker = new L.marker({lat: latitude, lng: longitude});
    marker.addTo(map);

    $('#pills-tab #pills-map-tab').on('shown.bs.tab', function (e) {
        map.invalidateSize(true);
    })
});

