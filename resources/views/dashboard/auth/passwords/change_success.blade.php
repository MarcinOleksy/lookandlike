@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.change_password') !!}
    </h2>
@endsection
@section('navigation')
@endsection
@section('content')
    <div>
        <span class="alert alert-success">{{trans('messages.password_change.success')}}</span>
    </div>
@endsection