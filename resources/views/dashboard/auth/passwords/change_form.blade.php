@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.change_password') !!}
    </h2>
@endsection
@section('navigation')
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">

                {!! form_start($form) !!}

                <div class="row">
                    <div class="col-12">
                        {!! form_until($form, "password_confirmation" ) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-10">
                    </div>
                    <div class="col-2">
                        {!! form_row($form->submit) !!}
                    </div>
                </div>

                {!! form_end($form) !!}

            </div>
        </div>
    </div>
@endsection