@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.locals_types_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.localsTypes.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{{trans('labels.name')}}</th>
                    <th>{{trans('labels.created_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($locals_types as $local_type)
                    <tr>
                        <td>{{ $local_type->name }}</td>
                        <td>{{ $local_type->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.localsTypes.edit', ['local_type_id' => $local_type->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.localsTypes.delete', ['local_type_id' => $local_type->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $locals_types])
            </div>
        </div>
    </div>
@endsection