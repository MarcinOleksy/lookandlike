@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.create_local_type') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.locals_types.form')
            </div>
        </div>
    </div>
@endsection