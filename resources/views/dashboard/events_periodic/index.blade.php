@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.events_periodic_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.eventsPeriodic.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.name')}}</th>
                        <th>{{trans('labels.type')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($events as $event)
                    <tr>
                        <td>{{ $event->name }}</td>
                        <td>{{ $event->local_type->name }}</td>
                        <td>{{ $event->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.events_periodic.edit', ['event_id' => $event->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.events_periodic.delete', ['event_id' => $event->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $events])
            </div>
        </div>
    </div>
@endsection