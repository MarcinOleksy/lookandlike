@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.create_event_periodic') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.events_periodic.form')
            </div>
        </div>
    </div>
@endsection