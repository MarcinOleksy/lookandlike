<div>
    {!! form_start($form) !!}

    <div>{{trans('labels.days_of_week.monday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->monday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->monday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.tuesday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->tuesday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->tuesday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.wednesday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->wednesday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->wednesday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.thursday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->thursday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->thursday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.friday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->friday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->friday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.saturday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->saturday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->saturday_close) !!}
        </div>
    </div>

    <div>{{trans('labels.days_of_week.sunday.pl')}}</div>
    <div class="row">
        <div class="col-6">
            {!! form_row($form->sunday_open) !!}
        </div>
        <div class="col-6">
            {!! form_row($form->sunday_close) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-10">
        </div>
        <div class="col-2">
            {!! form_row($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
</div>



