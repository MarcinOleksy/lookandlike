@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.edit_local_opening_hours') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.locals_opening_hours.form')
            </div>
        </div>
    </div>
@endsection