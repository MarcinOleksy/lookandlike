@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.users_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.users.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            {!! form_start($formSearch) !!}
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->search_engine) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->acl_group) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->created_from) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->created_to) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    {!! form_row($formSearch->submit) !!}
                </div>
            </div>
            {!! form_end($formSearch, false) !!}
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{{trans('labels.name')}}</th>
                    <th>{{trans('labels.email')}}</th>
                    <th>{{trans('labels.created_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.users.edit', ['id' => $user->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.users.delete', ['id' => $user->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $users])
            </div>
        </div>
    </div>
@endsection