{!! form_start($form) !!}
<div class="row">
    <div class="col-12">
        {!! form_until($form, 'description')  !!}

        {!! form_row($form->address) !!}

        <div id="myMap" data-longitude="{{ $form->getModel()->longitude }}" data-latitude="{{ $form->getModel()->latitude }}"></div>

        {!! form_row($form->longitude) !!}
        {!! form_row($form->latitude) !!}

        {!! form_row($form->image_id) !!}
    </div>
</div>
<div class="row">
    <div class="col-10">
    </div>
    <div class="col-2">
        {!! form_row($form->submit) !!}
    </div>
</div>
{!! form_end($form) !!}