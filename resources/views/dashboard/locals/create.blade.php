@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.create_local') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.locals.form')
            </div>
        </div>
    </div>
@endsection