@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.locals_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.locals.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')

    <div class="card mb-3">
        <div class="card-body">
            {!! form_start($formSearch) !!}
            <div class="row">
                <div class="col-md-6 col-12">
                    {!! form_row($formSearch->local_types) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->rating_from) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->rating_to) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->count_rating_from) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->count_rating_to) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    {!! form_row($formSearch->submit) !!}
                </div>
            </div>
            {!! form_end($formSearch, false) !!}
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{{trans('labels.name')}}</th>
                    <th>{{trans('labels.type')}}</th>
                    <th>{{trans('labels.created_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($locals as $local)
                    <tr>
                        <td>{{ $local->name }}</td>
                        <td>{{ $local->local_type->name }}</td>
                        <td>{{ $local->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.relations.index', ['local_id' => $local->id])}}" >{{ trans('labels.relations') }}</a>
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.opening_hours.edit', ['local_id' => $local->id])}}">{{ trans('labels.opening_hours') }}</a>
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.menu.index', ['local_id' => $local->id])}}">{{ trans('labels.menu') }}</a>
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.visitCodes.index', ['local_id' => $local->id])}}">{{ trans('labels.local_visit_codes') }}</a>

                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.ratings.index', ['local_id' => $local->id])}}">{{ trans('labels.ratings') }}</a>
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.locals.edit', ['local_id' => $local->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.locals.delete', ['local_id' => $local->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $locals])
            </div>
        </div>
    </div>
@endsection