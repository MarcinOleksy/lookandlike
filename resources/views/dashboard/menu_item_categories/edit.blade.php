@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.edit_menu_item_category') !!}
    </h2>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            @include('dashboard.menu_item_categories.form')
        </div>
    </div>
@endsection