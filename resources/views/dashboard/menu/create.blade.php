@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.create_menu') !!}
    </h2>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            @include('dashboard.menu.form')
        </div>
    </div>
@endsection