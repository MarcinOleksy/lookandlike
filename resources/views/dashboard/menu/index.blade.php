@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.menu_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.locals.menu.store', ['local_id' => $local->id])}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{{trans('labels.created_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($menu as $row)
                    <tr>
                        <td>{{ $row->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.menu.items.index', ['local_id' => $local->id, 'menu_id' => $row->id])}}">{{ trans('labels.menu_items') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.locals.menu.delete', ['local_id' => $local->id, 'id' => $row->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $menu])
            </div>
        </div>
    </div>
@endsection