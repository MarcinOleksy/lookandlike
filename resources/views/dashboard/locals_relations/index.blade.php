@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.local_relations_list') !!}
    </h2>
@endsection
@section('navigation')
@endsection
@section('content')

    <div class="relations_list">
        <div class="row">
            @foreach($relations as $relation)
                <div class="col-lg-4 col-sm-6">
                    <div class="relation">
                        <div class="card">
                            <div class="relation_author">
                                {{ $relation->user->name }}
                            </div>
                            <div class="relation_image">
                                <img class="image" src="{{  $relation->image ? '/storage/'.$relation->image->thumbnail_image_path : '' }}"/>
                            </div>
                            <div class="relation_comment">
                                {{ $relation->comment }}
                            </div>
                            <div class="row">
                                <div class="col-6 relation_created_at">
                                    {{ $relation->createdAt }}
                                </div>
                                <div class="col-6 btn-destroy text-right">
                                    <a class="btn btn-sm btn-danger" href="{{route('dashboard.locals.relations.delete', ['local_id' => $relation->local_id, 'relation_id' => $relation->id])}}"> {{ trans('labels.delete') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div>
        @include('dashboard.layout.paginate', ['data' => $relations])
    </div>

@endsection