@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.local_ratings_list') !!}
    </h2>
@endsection
@section('navigation')
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            {!! form_start($formSearch) !!}
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->created_from) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->created_to) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->rating_from) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->rating_to) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    {!! form_row($formSearch->submit) !!}
                </div>
            </div>
            {!! form_end($formSearch, false) !!}
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.rate')}}</th>
                        <th>{{trans('labels.comment')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ratings as $rating)
                    <tr>
                        <td>{{ $rating->rating }}</td>
                        <td>{{ $rating->comment }}</td>
                        <td>{{ $rating->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $ratings])
            </div>
        </div>
    </div>
@endsection