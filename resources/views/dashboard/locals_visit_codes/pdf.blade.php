<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            body {

            }
            #list_codes #item{
                width: 33%;
                float: left;
            }
            #list_codes #item #image{
                width: 100%;
                height: 200px;
            }
            #list_codes #item #code{
                text-align: center;
            }
        </style>

    </head>
    <body>
        @if(count($visitCodes))
            <div id="list_codes">
                @foreach($visitCodes as $visitCode)
                    <div id="item">
                        <img id="image" src="{{$visitCode->path}}">
                        <div id="code">
                            {{ $visitCode->code }}
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div>
                Brak nowych kodów. Nie ma kodów lub zostały wcześniej pobrane.
            </div>
        @endif
    </body>
</html>