@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.local_visit_code') !!}
    </h2>
@endsection
@section('navigation')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row col-12">
                <div class="col-6">
                    <dl>
                        <dt>Nazwa lokalu:</dt>
                        <dd>{{$local->name}}</dd>

                        <dt>Nazwa lokalu:</dt>
                        <dd>{{$local->local_type->name}}</dd>
                    </dl>
                </div>
                <div class="col-6">
                    <dl>
                        <dt>Kod wizyty:</dt>
                        <dd>{{$code->code}}</dd>
                    </dl>
                    <img src="{{$code->path}}">
                </div>
            </div>
        </div>
    </div>
@endsection