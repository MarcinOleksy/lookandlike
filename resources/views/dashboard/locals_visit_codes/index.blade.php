@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.locals_visit_codes_list') !!}
    </h2>
@endsection
@section('navigation')
    <a class="btn btn-sm btn-success" href="{{route('dashboard.locals.visitCodes.print', ['local_id' => $local->id])}}">{{trans('labels.print')}}</a>
    <a class="btn btn-sm btn-success" href="{{route('dashboard.locals.visitCodes.create', ['local_id' => $local->id])}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            {!! form_start($formSearch) !!}
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_widget($formSearch->printed_at) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->submit) !!}
                </div>
            </div>
            {!! form_end($formSearch, false) !!}
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.code')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($codes as $code)
                    <tr>
                        <td>{{ $code->code }}</td>
                        <td>{{ $code->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{route('dashboard.locals.visitCodes.show',['local_id' => $local->id, 'code_id' => $code->id])}}">{{trans('labels.preview')}}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $codes])
            </div>
        </div>
    </div>
@endsection