@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.menu_items_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.locals.menu.items.create', ['local_id' => $local->id, 'menu_id' => $menu->id])}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.name')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.locals.menu.items.edit', ['local_id' => $local->id, 'menu_id' => $menu->id, 'menu_item_id' => $item->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.locals.menu.items.delete', ['local_id' => $local->id, 'menu_id' => $menu->id, 'menu_item_id' => $item->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $items])
            </div>
        </div>
    </div>
@endsection