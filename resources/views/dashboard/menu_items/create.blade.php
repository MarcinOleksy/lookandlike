@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.create_menu_item') !!}
    </h2>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            @include('dashboard.menu_items.form')
        </div>
    </div>
@endsection