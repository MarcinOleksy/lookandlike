@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.events_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-primary" href="{{route('dashboard.eventsPeriodic.index')}}">{{trans('labels.events_periodic_list ')}}</a>
        <a class="btn btn-sm btn-success" href="{{route('dashboard.events.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            {!! form_start($formSearch) !!}
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->local_types) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->event_types) !!}
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->start_from) !!}
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    {!! form_row($formSearch->start_to) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->rating_from) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->rating_to) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->count_rating_from) !!}
                </div>
                <div class="col-md-3 col-6">
                    {!! form_row($formSearch->count_rating_to) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    {!! form_row($formSearch->submit) !!}
                </div>
            </div>
            {!! form_end($formSearch, false) !!}
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.title')}}</th>
                        <th>{{trans('labels.type')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($events as $event)
                    <tr>
                        <td>{{ $event->title }}</td>
                        <td>{{ $event->event_type->name }}</td>
                        <td>{{ $event->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.events.edit', ['event_id' => $event->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.events.delete', ['event_id' => $event->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $events])
            </div>
        </div>
    </div>
@endsection