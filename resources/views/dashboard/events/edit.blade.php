@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.edit_event') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.events.form')
            </div>
        </div>
    </div>
@endsection