@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.events_types_list') !!}
    </h2>
@endsection
@section('navigation')
        <a class="btn btn-sm btn-success" href="{{route('dashboard.eventsTypes.create')}}">{{trans('labels.create')}}</a>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('labels.name')}}</th>
                        <th>{{trans('labels.created_at')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($events_types as $event_type)
                    <tr>
                        <td>{{ $event_type->name }}</td>
                        <td>{{ $event_type->created_at }}</td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-warning" href="{{route('dashboard.eventsTypes.edit', ['event_type_id' => $event_type->id])}}">{{ trans('labels.edit') }}</a>
                            <a class="btn btn-sm btn-danger" href="{{route('dashboard.eventsTypes.delete', ['event_type_id' => $event_type->id])}}">{{ trans('labels.delete') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                @include('dashboard.layout.paginate', ['data' => $events_types])
            </div>
        </div>
    </div>
@endsection