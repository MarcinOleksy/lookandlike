@extends('dashboard.layout.body')
@section('header')
    <h2>
        {!! trans('labels.edit_event_type') !!}
    </h2>
@endsection
@section('content')
    <div>
        <div class="card">
            <div class="card-body">
                @include('dashboard.events_types.form')
            </div>
        </div>
    </div>
@endsection