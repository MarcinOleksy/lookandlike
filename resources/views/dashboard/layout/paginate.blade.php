@if($data)
    {{ $data->appends(request()->query())->render() }}
@endif