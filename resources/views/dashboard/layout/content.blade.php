<div class="wrapper">
    <div class="row">
        <div class="header col-8">
            @yield('header')
        </div>
        <div class="navigation text-right col-4">
            @yield('navigation')
        </div>
    </div>
    <div class="content">
        @yield('content')
    </div>
    {{--<div class="footer">--}}
        {{--@include('dashboard.layout.footer')--}}
    {{--</div>--}}
</div>