<nav>
    <ul class="nav flex-column">
        @foreach(\App\Navigation\ConvertNavigation::getInstance()->menu('dashboard-main') as $row)
            @if($row->getPermission())
                <li class="nav-item">
                    <a class="nav-link" href="{{$row->getUrl()}}"><i class="{{ $row->getIcon() }}"></i> {{$row->getLabel()}}</a>
                </li>
            @endif
        @endforeach
    </ul>
</nav>
