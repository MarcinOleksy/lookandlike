<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

        <link href="/css/app_dashboard.css?v={{ time() }}" rel="stylesheet" type="text/css">

    </head>

    <body>

        @yield('body')

        <script type="text/javascript" src="/js/app_dashboard.js"></script>

    </body>
</html>