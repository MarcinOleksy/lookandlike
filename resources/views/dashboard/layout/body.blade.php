@extends('dashboard.layout.index')
@section('body')
    <header class="navbar navbar-expand-lg">
        @include('dashboard.layout.navbar')
    </header>
    <aside id="sidebar">
        @include('dashboard.layout.sidebar')
    </aside>

    <main id="content">
        @include('dashboard.layout.message_content')
        @include('dashboard.layout.content')
    </main>
@endsection