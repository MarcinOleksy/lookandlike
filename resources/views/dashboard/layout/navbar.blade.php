<div class="navbar-brand">
    <div class="logo">
        <strong><img src="/logo/lornetka.png" alt="LOGO" /></strong>
    </div>
</div>

<div class="navbar-nav mr-auto mt-2 mt-lg-0">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('dashboard.home')}}">
                <i class="fas fa-home"></i>
            </a>
        </li>
        @foreach(\App\Navigation\ConvertNavigation::getInstance()->breadcrumb() as $item)
            <li class="breadcrumb-item">
                <a href="{{route($item['urlObj']->getRoute(),$item['routeParams'])}}">
                    {{$item['urlObj']->getBreadcrumb()}}
                </a>
            </li>
        @endforeach
    </ol>
</div>

<div class="navbar-loged-user">
    <div class="nav-item dropdown">
        <div class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Admin
        </div>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('dashboard.accountChange.edit')}}">{{trans('labels.change_account')}}</a>

            <a class="dropdown-item" href="{{route('dashboard.passwordChange.showChangePasswordForm')}}">{{trans('labels.change_password')}}</a>
            <a class="dropdown-item" href="{{route('dashboard.logout')}}" >{{trans('labels.logout')}}</a>
        </div>
    </div>
</div>