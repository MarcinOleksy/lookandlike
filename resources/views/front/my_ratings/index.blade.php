@extends('front.commons.base.main')

@section('content')

    <div class="card filters">
        <a id="headingOne" class="filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters">
            <div class="card-header">
                <b>
                    Filtry wyszukiwania opinii
                </b>
            </div>
        </a>
        <div id="collapseFilters" class="collapse card-body">
            <div>
                {!! form_start($formSearch) !!}
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12">
                        {!! form_row($formSearch->local_id) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->created_from) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->created_to) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->rating_from) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->rating_to) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        {!! form_row($formSearch->submit) !!}
                    </div>
                </div>
                {!! form_end($formSearch, false) !!}
            </div>
        </div>
    </div>

    <div class="comments_content">
        <div class="row">
            <div class="col-12">
                <div class="card ">
                    <div class="card-body">
                        <div class="comments">
                            @foreach($ratings as $rating)
                                <div class="comment">
                                    <div class="row">
                                        <div class="col-lg-1 col-sm-2 comment_local_image">
                                            <a href="{{ route('front.locals.show', ['local_id' => $rating->local_id]) }}">
                                                <img class="image" src="/images/pub.jpg"/>
                                            </a>
                                        </div>
                                        <div class="col-lg-11 col-sm-10">
                                            <div class="comment_local row">
                                                <div class="col-md-9 col-sm-12">
                                                    <a href="{{ route('front.locals.show', ['local_id' => $rating->local_id]) }}">
                                                        {{ $rating->local->local_type->name }}
                                                        {{ $rating->local->name }}
                                                    </a>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <small>
                                                        {{ $rating->created_at }}
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="comment_rating">
                                                @for($i = 0; $i < $rating->rating; $i++)
                                                    <i class="fas fa-star"></i>
                                                @endfor
                                            </div>
                                            <div class="comment_content">
                                                {{ $rating->comment }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection


