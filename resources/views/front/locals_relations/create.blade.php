@extends('front.commons.base.main')
@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-7 col-md-9 col-sm-9 col-12">
            <div class="card card-shadow">
                <div class="card-body">
                        <h4>{{ $local->local_type->name }}</h4>
                        <h2><strong>{{ $local->name }}</strong></h2>
                        <strong>Adres:</strong> {{ $local->address }}
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-7 col-md-9 col-sm-9 col-12">
            <div class="card card-shadow">
                <div class="card-header">
                    {{ trans('labels.add_local_relation') }}
                </div>
                <div class="card-body">
                    {!! form_start($form) !!}
                    {!! form_row($form->image_id) !!}
                    {!! form_row($form->comment) !!}
                    <div class="row">
                        <div class="col-8">
                        </div>
                        <div class="col-4">
                            {!! form_row($form->submit) !!}
                        </div>
                    </div>
                    {!! form_end($form) !!}
                </div>
            </div>
        </div>
    </div>
@endsection