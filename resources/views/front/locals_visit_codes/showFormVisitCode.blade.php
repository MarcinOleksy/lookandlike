@extends('front.commons.base.main')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-7 col-sm-9 col-12">
            <div class="card card-shadow">
                <div class="card-header">
                    {{ trans('labels.local_visit_code') }}
                </div>
                <div class="card-body">
                    {!! form_start($form) !!}
                        {!! form_row($form->code) !!}
                        <div class="row">
                            <div class="col-8">
                            </div>
                            <div class="col-4">
                                {!! form_row($form->submit) !!}
                            </div>
                        </div>
                    {!! form_end($form) !!}
                </div>
            </div>
        </div>
    </div>

@endsection