@extends('front.commons.base.main')
@section('content')
    <div class="row col-12 justify-content-center">
        <div class="col-md-3 col-sm-4 col-6">
            <div class="card home_card card-shadow">
                <a href="{{ route('front.relations.create') }}">
                    <div class="card-body">
                        Dodaj Relacje
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-6">
            @if(!$visitCodeItem->expired)
                <div class="card home_card card-shadow">
                    <a href="{{ route('front.ratings.create') }}">
                        <div class="card-body">
                            Dodaj opinie
                        </div>
                    </a>
                </div>
            @else
                <div class="card home_card card-shadow">
                    <div class="card-body">
                        Nie możesz dodać opinii, bo ten kod został już użyty w tym celu.
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection