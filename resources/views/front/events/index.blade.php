@extends('front.commons.base.main')

@section('content')

    <div class="card filters">
        <a id="headingOne" class="filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters">
            <div class="card-header">
               <b>
                   Filtry wyszukiwania wydarzeń
               </b>
            </div>
        </a>
        <div id="collapseFilters" class="collapse card-body">
            <div>
                {!! form_start($formSearch) !!}
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->local_types) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->event_types) !!}
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->start_from) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->start_to) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->rating_from) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->rating_to) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->count_rating_from) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->count_rating_to) !!}
                    </div>
                </div>
                <div class="row">
                    @if(Auth::user())
                        {!! form_row($formSearch->hide_rated) !!}
                        {!! form_row($formSearch->hide_not_rated) !!}
                        {!! form_row($formSearch->want_see_local) !!}
                        {!! form_row($formSearch->want_see_event) !!}
                    @endif
                    {!! form_row($formSearch->free_entrance) !!}
                </div>
                <div class="row">
                    <div class="col-3">
                        {!! form_row($formSearch->submit) !!}
                    </div>
                </div>
                {!! form_end($formSearch, false) !!}
            </div>
        </div>
    </div>

    <div class="locals_list row">
        @foreach($events as $item)
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="card_local">
                        <a title="{{$item->title}}" href="{{route('front.events.show', ['local_id' => $item->id])}}">
                        <div class="card_local_image">
                                <img src="images/pub.jpg"/>
                            </div>
                        </a>
                        <div class="card_local_content">
                            <div class="card_local_content_type_name">
                                {{$item->local->local_type->name}}: {{ $item->local->name }}
                            </div>
                            <a title="{{$item->title}}" href="{{route('front.events.show', ['local_id' => $item->id])}}">
                                <div class="card_local_content_name">
                                    {{$item->title}}
                                </div>
                            </a>
                            <div class="card_local_content_opening_hours">
                                <div class="row">
                                    <div class="col-6 border-right text-center">
                                        {{ parseCarbonDateToFormat($item->start_at, 'd F Y') }}
                                    </div>
                                    <div class="col-6 border-left text-center">
                                        {{ parseCarbonDateToFormat($item->start_at, 'H:i') }} - {{ parseCarbonDateToFormat($item->finish_at, 'H:i') }}
                                    </div>
                                </div>
                            </div>
                            <div class="card_local_content_stats row">
                                <div class="col-6 card_local_content_stats_rating">
                                <span>
                                    <i class="fas fa-star"></i>
                                    <strong>{{ number_format($item->local->local_stats->rating, 1) }}</strong>
                                    <small>({{  $item->local->local_stats->count_rating }})</small>
                                </span>
                                </div>
                                <div class="col-6 card_local_content_stats_comment">
                                <span>
                                    <i class="fas fa-comment"></i>
                                    {{ $item->local->local_stats->count_comments }}
                                </span>
                                </div>
                            </div>
                        </div>

                    </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>

    {{----}}
    {{----}}
        {{--<div class="list-items">--}}
            {{--<ul class="items list-unstyled">--}}
                {{--@foreach($events as $item)--}}
                    {{--<li class="event">--}}
                        {{--<div class="row">--}}
                            {{--<div class="image col-3">--}}
                                {{--<img class="col-12" src="images/pub.jpg"/>--}}
                            {{--</div>--}}
                            {{--<div class="body col-9">--}}
                                {{--<div class="col-12">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-8">--}}
                                            {{--<div class="name">{{$item->title}}</div>--}}
                                            {{--<div class="local-name">--}}
                                                {{--{{ $item->local->name }} ({{ $item->local->local_type->name }})--}}
                                            {{--</div>--}}
                                            {{--<div class="opening-hours">--}}
                                                {{--{{ parseCarbonDateToFormat($item->start_at, $item->start_at->diffInHours($item->finish_at) < 47 ? 'H:i' : 'H:i d-m-Y') }} - {{ parseCarbonDateToFormat($item->finish_at, $item->start_at->diffInHours($item->finish_at) < 47 ? 'H:i' : 'H:i d-m-Y') }}--}}
                                            {{--</div>--}}
                                            {{--<div class="buttons">--}}
                                                {{--<a href="{{route('front.events.show', ['local_id' => $item->id])}}" class="btn btn-more">Zobacz więcej</a>--}}
                                                {{--<a class="btn btn-more"><i class="fas fa-video"></i> 12 </a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-4">--}}
                                            {{--<ul class="info list-unstyled">--}}
                                                {{--<li class="rating">--}}
                                                    {{--<span>--}}
                                                        {{--<i class="fas fa-star"></i>--}}
                                                        {{--<strong>{{ $item->local->rating }}</strong>--}}
                                                        {{--<small>({{ $item->local->countRating }})</small>--}}
                                                    {{--</span>--}}
                                                {{--</li>--}}
                                                {{--<li class="see">--}}
                                                    {{--<span>--}}
                                                    {{--<i class="fas fa-eye"></i>--}}
                                                    {{--45678--}}
                                                {{--</span>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--</div>--}}


@endsection
