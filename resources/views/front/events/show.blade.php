@extends('front.commons.base.main')
@section('content')
    <div class="row">
        <div class="col-7">
            <div class="card">
                <div class="card-body">
                    <div class="images">
                        <img src="/images/pub.jpg"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-body">
                    <div class="basic-informations">
                        <div class="info">
                            <div class="name">
                                <h2><strong>{{ $event->title }}</strong></h2>
                                <h6>{{ $event->local->local_type->name }}: {{ $event->local->name }}</h6>
                            </div>
                            <div class="opening_hours">
                                <b>Termin: </b>
                                <div>
                                    {{ parseCarbonDateToFormat($event->start_at, 'd F Y') }}
                                    <strong>[ {{ parseCarbonDateToFormat($event->start_at, 'H:i') }} - {{ parseCarbonDateToFormat($event->finish_at, 'H:i') }} ]</strong>
                                </div>
                            </div>
                            <div class="address">
                                <b>Adres:</b>
                                <div>
                                    {{ $event->local->address }}
                                </div>
                            </div>

                        </div>
                        <div class="stats_info">
                            <div class="rating">
                                <i class="fas fa-star"></i> {{ $event->local->local_stats->rating }} ({{ $event->local->local_stats->count_rating }})
                            </div>
                            <div class="comments">
                                <i class="fas fa-comment"></i> {{ $event->local->local_stats->count_comments }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="information-content card">
        <div class="card-body">
            <ul class="nav mb-3 justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="description nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-selected="true">
                        <i class="fas fa-info"></i>
                        Opis
                    </a>
                </li>
                <li class="nav-item">
                    <a class="program nav-link" id="pills-program-tab" data-toggle="pill" href="#pills-program" role="tab" aria-controls="pills-program" aria-selected="false">
                        <i class="fas fa-stream"></i>
                        Program
                    </a>
                </li>
                <li class="nav-item">
                    <a class="menu nav-link" id="pills-menu-tab" data-toggle="pill" href="#pills-menu" role="tab" aria-controls="pills-menu" aria-selected="false">
                        <i class="fas fa-utensils"></i>
                        Menu
                    </a>
                </li>
            </ul>
            <div class="col-12 tab-content" id="pills-tabContent">
                <div class="description tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                    {{ $event->description }}
                </div>
                <div class="program tab-pane fade" id="pills-program" role="tabpanel" aria-labelledby="pills-program-tab">
                    {{ $event->program }}
                </div>
                <div class="menu tab-pane fade" id="pills-menu" role="tabpanel" aria-labelledby="pills-menu-tab">
                    <ul>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                        <li> Praesent in leo - 234 dolor.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="card comments_content">
        <div class="comments_header col-12">
            <div class="row">
                <div class="col-3 comments_title">
                    Opinie
                </div>
                <div class="col-9 filter_comments text-right">
                    <a id="headingOne" class="btn filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters"><i class="fas fa-filter"></i> Filtry</a>
                </div>
                <div id="collapseFilters" class="collapse">
                    <div class="card-body">
                        <div>
                            <input type="checkbox"> Nie pokazuj negatywnych
                            <input type="checkbox"> Sortowanie od najnowszego
                            <input type="checkbox"> Sortowanie od najstarszego
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="comments">
                @foreach($ratings as $rating)
                    <div class="comment">
                        <div class="row">
                            <div class="col-1 comment_author_image">
                                <img class="image" src="/images/pub.jpg"/>
                            </div>
                            <div class="col-11">
                                <div class="comment_author">
                                    {{ $rating->user->name }}
                                    <small>
                                        {{ $rating->created_at }}
                                    </small>
                                </div>
                                <div class="comment_rating">
                                    @for($i = 0; $i < $rating->rate; $i++)
                                        <i class="fas fa-star"></i>
                                    @endfor
                                </div>
                                <div class="comment_content">
                                    {{ $rating->comment }}
                                </div>
                                <div class="comment_buttons">
                                    <a class="btn" href="#"><i class="far fa-thumbs-up"></i> Lubię <strong>(342)</strong></a>
                                    <a class="btn" href="#"><i class="far fa-thumbs-down"></i> Nie lubię <strong>(57)</strong></a>
                                    <a class="btn" href="#"><i class="fas fa-quote-right"></i> Cytuj</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection