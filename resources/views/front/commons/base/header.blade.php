<header>
	<div class="col-xl-9 col-lg-10 col-md-11 col-12 wrap">
			<div class="row">
				<div class="col-3">
					<a href="{{ route('front.home') }}">
						<div class="logo">
							<img src="/logo/lornetka.png" alt="Logo">
						</div>
					</a>
				</div>
				<div class="col-9">
					<div class="buttnos">
						<ul class="nav justify-content-end">
							@if(Auth::user())
								<li class="nav-item ml-2">
									<a class="text-white nav-link btn btn-dark text-white" href="{{ route('front.visit_codes.showFormVisitCode') }}"> <i class="fas fa-plus"></i> Dodaj</a>
								</li>
								<li>
									<div class="navbar-loged-user nav-item ml-2">
										<div class="nav-item dropdown">
											<div class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<a href=""><img class="avatar-mini" src="/images/pub.jpg"/></a>
											</div>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a class="dropdown-item" href="{{route('front.accountChange.edit')}}">{{trans('labels.change_account')}}</a>

												<a class="dropdown-item" href="{{route('front.passwordChange.showChangePasswordForm')}}">{{trans('labels.change_password')}}</a>
												<a class="dropdown-item" href="{{route('front.logout')}}" >{{trans('labels.logout')}}</a>
											</div>
										</div>
									</div>
								</li>
							@else
								<li class="nav-item ml-2">
									<a class="text-white nav-link btn btn-sm btn-dark text-white" href="{{route('front.registration.showRegistrationForm')}}">{{ trans('labels.register') }}</a>
								</li>
								<li class="nav-item ml-2">
									<a class="text-white nav-link btn btn-sm btn-dark text-white" href="{{route('front.login.showLoginForm')}}">{{ trans('labels.sing_in') }}</a>
								</li>
							@endif
						</ul>
					</div>
				</div>
			</div>
	</div>
</header>