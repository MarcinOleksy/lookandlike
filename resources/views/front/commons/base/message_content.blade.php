<div data-lazy-hook="container-message">

    @if(Session::has('success'))
        <div class="message p-2">
            <div class="alert alert-success m-0">
                {!!  Session::get('success') !!}
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="message p-2">
            <div class="alert alert-danger m-0">
                {!!  Session::get('error') !!}
            </div>
        </div>
    @endif
    @if(Session::has('info'))
        <div class="message p-2">
            <div class="alert alert-info m-0">
                {!!  Session::get('info') !!}
            </div>
        </div>
    @endif
</div>