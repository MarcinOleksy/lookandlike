@extends('front.commons.base.layout')

@section('body')
    @include('front.commons.base.header')
    @include('front.commons.base.menu')
    @include('front.commons.base.content')
    @include('front.commons.base.footer')
@endsection