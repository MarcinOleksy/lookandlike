<main>
	<div class="col-xl-9 col-lg-10 col-md-11 col-12 wrap">
		@include('front.commons.base.message_content')
		@yield('content')
	</div>
</main>