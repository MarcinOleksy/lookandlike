<nav>
    <?php
       $currentRouteNameExplode = explode('.', Route::currentRouteName());
    ?>
    <div class="col-xl-9 col-lg-10 col-md-11 col-12 wrap">
        <div class="row col-12">
            <ul class="nav justify-content-end">
                @foreach(\App\Navigation\ConvertNavigation::getInstance()->menu('front-main') as $row)
                    @if(!$row->getAuth() || $row->getPermission())
                        <li class="nav-item {{ $currentRouteNameExplode[1] == explode('.',$row->getRoute())[1] ? "active" : ""}}">
                            <a class="text-white nav-link btn btn-gold text-white" href="{{$row->getUrl()}}">{{$row->getLabel()}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div id="content_header">
        <div class="col-xl-9 col-lg-10 col-md-11 col-12 wrap">
            @yield('content_header')
        </div>
    </div>
</nav>
