@extends('front.commons.base.main')
@section('content')
    <div class="row col-12">
            <div class="col-md-4 col-12">

                    <div class="card home_card card-shadow">
                        <a href="{{ route('front.visit_codes.showFormVisitCode') }}">
                        <div class="card-body">
                            <i class="fas fa-photo-video"></i>
                            <br/>
                            Dodaj opinie / Dodaj relacje
                        </div>
                        </a>
                    </div>

            </div>
            <div class="col-md-4 col-12">

                    <div class="card home_card card-shadow">
                        <a href="{{ route('front.locals.index') }}">
                        <div class="card-body">
                            <i class="fas fa-map-marker-alt"></i>
                            <br/>
                            Lokale
                        </div>
                        </a>
                    </div>

            </div>
            <div class="col-md-4 col-12">

                    <div class="card home_card card-shadow">
                        <a href="{{ route('front.events.index') }}">
                        <div class="card-body">
                            <i class="fas fa-glass-cheers"></i>
                            <br/>
                            Wydarzenia
                        </div>
                        </a>
                    </div>

            </div>
    </div>
@endsection