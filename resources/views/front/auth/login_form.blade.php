@extends('front.commons.base.main')
@section('content')
{!! form_start($form) !!}
    <div class="row justify-content-center">
        <div class="col-md-7 col-sm-9 col-12">
            <div class="card card-shadow">
                <div class="card-header">
                    {{ trans('labels.login') }}
                </div>
                <div class="card-body">
                    {!! form_row($form->email) !!}
                    {!! form_row($form->password) !!}
                    <div class="row">
                        <div class="col-sm-8 col-6">
                            <a href="{{ route('front.passwordForget.email') }}">{{ trans('labels.password_forget') }}</a>
                        </div>
                        <div class="col-sm-4 col-6">
                            {!! form_row($form->submit) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! form_end($form) !!}
@endsection