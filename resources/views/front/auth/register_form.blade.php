@extends('front.commons.base.main')
@section('content')
{!! form_start($form) !!}
    <div class="row justify-content-center">
        <div class="col-md-7 col-sm-9 col-12">
            <div class="card card-shadow">
                <div class="card-header">
                    {{ trans('labels.registration') }}
                </div>
                <div class="card-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->password) !!}
                    {!! form_row($form->repeat_password) !!}
                    <div class="row">
                        <div class="col-8">
                        </div>
                        <div class="col-4">
                            {!! form_row($form->submit) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! form_end($form) !!}
@endsection