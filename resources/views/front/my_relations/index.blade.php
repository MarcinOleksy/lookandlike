@extends('front.commons.base.main')

@section('content')

    <div class="card filters">
        <a id="headingOne" class="filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters">
            <div class="card-header">
                <b>
                    Filtry wyszukiwania relacji
                </b>
            </div>
        </a>
        <div id="collapseFilters" class="collapse card-body">
            <div>
                {!! form_start($formSearch) !!}
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12">
                        {!! form_row($formSearch->local_id) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->created_from) !!}
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        {!! form_row($formSearch->created_to) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        {!! form_row($formSearch->submit) !!}
                    </div>
                </div>
                {!! form_end($formSearch, false) !!}
            </div>
        </div>
    </div>

    <div class="relations_list">
        <div class="row">
            @foreach($relations as $relation)
                <div class="col-lg-4 col-sm-6">
                    <div class="relation">
                        <div class="card">
                            <div class="relation_local">
                                <a href="{{ route('front.locals.show', ['local_id' => $relation->local_id]) }}">
                                    {{ $relation->local->local_type->name }}
                                    {{ $relation->local->name }}
                                </a>
                            </div>
                            <div class="relation_image">
                                <img class="image" src="{{  $relation->image ? '/storage/'.$relation->image->thumbnail_image_path : '' }}"/>
                            </div>
                            <div class="relation_comment">
                                {{ $relation->comment }}
                            </div>
                            <div class="relation_created_at">
                                {{ $relation->createdAt }}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


@endsection


