@extends('front.commons.base.main')

@section('content')
    <div class="card filters">
        <a id="headingOne" class="filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters">
            <div class="card-header">
                <b>
                    Filtry wyszukiwania lokali
                </b>
            </div>
        </a>
        <div id="collapseFilters" class="collapse card-body">
            <div>
                {!! form_start($formSearch) !!}
                <div class="row">
                    <div class="col-md-6 col-12">
                        {!! form_row($formSearch->local_types) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->rating_from) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->rating_to) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->count_rating_from) !!}
                    </div>
                    <div class="col-md-3 col-6">
                        {!! form_row($formSearch->count_rating_to) !!}
                    </div>
                </div>
                <div class="row">
                    @if(Auth::user())
                        {!! form_row($formSearch->hide_rated) !!}
                        {!! form_row($formSearch->hide_not_rated) !!}
                        {!! form_row($formSearch->want_see) !!}
                    @endif
                    {!! form_row($formSearch->free_entrance) !!}
                </div>
                <div class="row">
                    <div class="col-3">
                        {!! form_row($formSearch->submit) !!}
                    </div>
                </div>
                {!! form_end($formSearch, false) !!}
            </div>
        </div>
    </div>

    <div class="locals_list">
        <div class="row">
            @foreach($locals as $item)
                <div class="col-lg-4 col-sm-6">
                    <div class="card">
                        <div class="card_local">
                            <a title="{{$item->name}}" href="{{route('front.locals.show', ['local_id' => $item->id])}}">
                                <div class="card_local_image">
                                    <img src="{{  $item->image_main ? '/storage/'.$item->image_main->thumbnail_image_path : '' }}"/>
                                </div>
                            </a>
                            <div class="card_local_content">
                                <div class="card_local_content_type_name">
                                    {{ $item->local_type->name }}
                                </div>
                                <a title="{{$item->name}}" href="{{route('front.locals.show', ['local_id' => $item->id])}}">
                                    <div class="card_local_content_name">
                                        {{$item->name}}
                                    </div>
                                </a>
                                {{--<div class="card_local_content_opening_hours">--}}
                                {{--<div class="nav-item dropdown">--}}
                                {{--<div class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                {{--{{ (trans('days_of_week.days.'.date('N').'.shortPL')) .' | '. (isset($item->local_opening_hours) ? $item->local_opening_hours->getOpeningHoursForNumberDay(date('N')) : 'Godziny nieustawione') }}--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
                                {{--<table>--}}
                                {{--<tbody>--}}
                                {{--@foreach(array_keys(trans('days_of_week.days')) as $numberDay)--}}
                                {{--<tr class="dropdown-item">--}}
                                {{--<td style="width: 50px">--}}
                                {{--{{ (trans('days_of_week.days.'.$numberDay.'.shortPL')) }}--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--{{ ' | '.(isset($item->local_opening_hours) ? $item->local_opening_hours->getOpeningHoursForNumberDay($numberDay) : 'Godziny nieustawione') }}--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="card_local_content_stats row">
                                    <div class="col-6 card_local_content_stats_rating">
                                <span>
                                    <i class="fas fa-star"></i>
                                    <strong>{{ number_format($item->local_stats->rating, 1) }}</strong>
                                    <small>({{ $item->local_stats->count_rating }})</small>
                                </span>
                                    </div>
                                    <div class="col-6 card_local_content_stats_comment">
                                <span>
                                    <i class="fas fa-comment"></i>
                                    {{ $item->local_stats->count_comments }}
                                </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="text-center">
            {{ $locals->links()  }}
        </div>
    </div>


@endsection
