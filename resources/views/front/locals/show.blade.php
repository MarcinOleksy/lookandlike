@extends('front.commons.base.main')
@section('content')
    <div class="row">
        <div class="col-7">
            <div class="card">
                <div class="card-body">
                    <div class="images">
                        <img src="{{  $local->image_main ? '/storage/'.$local->image_main->thumbnail_image_path : '' }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-body">
                    <div class="basic-informations">
                        <div class="info">
                            <div class="name">
                                <h4>{{ $local->local_type->name }}</h4>
                                <h2><strong>{{ $local->name }}</strong></h2>
                            </div>
                            <div class="address">
                                <b>Adres:</b>
                                <div>
                                    {{ $local->address }}
                                </div>
                            </div>
                            <div class="local_opening_hours">
                                <b>Godziny otwarcia:</b>
                                <div class="nav-item dropdown">
                                    <div class="btn p-0 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ (trans('days_of_week.days.'.date('N').'.shortPL')) .' | '. (isset($local->local_opening_hours) ? $local->local_opening_hours->getOpeningHoursForNumberDay(date('N')) : 'Godziny nieustawione') }}
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <table>
                                            <tbody>
                                            @foreach(array_keys(trans('days_of_week.days')) as $numberDay)
                                                <tr class="dropdown-item">
                                                    <td style="width: 50px">
                                                        {{ (trans('days_of_week.days.'.$numberDay.'.shortPL')) }}
                                                    </td>
                                                    <td>
                                                        {{ ' | '.(isset($local->local_opening_hours) ? $local->local_opening_hours->getOpeningHoursForNumberDay($numberDay) : 'Godziny nieustawione') }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stats_info">
                            <div class="rating">
                                <i class="fas fa-star"></i> {{ $local->local_stats->rating }} ({{ $local->local_stats->count_rating }})
                            </div>
                            <div class="comments">
                                <i class="fas fa-comment"></i> {{ $local->local_stats->count_comments }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="information-content card">
            <div class="card-body">
                <ul class="nav mb-3 justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="description nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-selected="true">
                            <i class="fas fa-info"></i>
                            Opis
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="events nav-link" id="pills-events-tab" data-toggle="pill" href="#pills-events" role="tab" aria-controls="pills-events" aria-selected="true">
                            <i class="fas fa-glass-cheers"></i>
                            Wydarzenia
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="map nav-link" id="pills-map-tab" data-toggle="pill" href="#pills-map" role="tab" aria-controls="pills-map" aria-selected="false">
                            <i class="fas fa-map-marker-alt"></i>
                            Mapa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="menu nav-link" id="pills-menu-tab" data-toggle="pill" href="#pills-menu" role="tab" aria-controls="pills-menu" aria-selected="false">
                            <i class="fas fa-utensils"></i>
                            Menu
                        </a>
                    </li>
                </ul>
                <div class="col-12 tab-content" id="pills-tabContent">
                    <div class="description tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                        Opis:
                        {{ $local->description }}
                    </div>
                    <div class="events tab-pane fade" id="pills-events" role="tabpanel" aria-labelledby="pills-events-tab">
                        Wydarzenia:
                    </div>
                    <div class="map tab-pane fade" id="pills-map" role="tabpanel" aria-labelledby="pills-map-tab">
                        <div id="myMap" data-longitude="{{ $local->longitude }}" data-latitude="{{ $local->latitude }}"></div>
                    </div>
                    <div class="menu tab-pane fade" id="pills-menu" role="tabpanel" aria-labelledby="pills-menu-tab">
                        <ul>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                            <li> Praesent in leo - 234 dolor.</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    <div class="card comments_content">
        <div class="comments_header col-12">
            <div class="row">
                <div class="col-3 comments_title">
                    Opinie
                </div>
                <div class="col-9 filter_comments text-right">
                    <a id="headingOne" class="btn filters_btn" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters"><i class="fas fa-filter"></i> Filtry</a>
                </div>
                <div id="collapseFilters" class="collapse">
                    <div class="card-body">
                        {!! form_start($formSearchRating) !!}
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12">
                                {!! form_row($formSearchRating->rating_from) !!}
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                {!! form_row($formSearchRating->rating_to) !!}
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                {!! form_row($formSearchRating->created_from) !!}
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                {!! form_row($formSearchRating->created_to) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12">
                                {!! form_row($formSearchRating->submit) !!}
                            </div>
                        </div>
                        {!! form_start($formSearchRating) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="comments">
                @foreach($ratings as $rating)
                    <div class="comment">
                        <div class="row">
                            <div class="col-1 comment_author_image">
                                <img class="image" src="/images/pub.jpg"/>
                            </div>
                            <div class="col-11">
                                <div class="comment_author">
                                    {{ $rating->user->name }}
                                    <small>
                                        {{ $rating->created_at }}
                                    </small>
                                </div>
                                <div class="comment_rating">
                                    @for($i = 0; $i < $rating->rating; $i++)
                                        <i class="fas fa-star"></i>
                                    @endfor
                                </div>
                                <div class="comment_content">
                                    {{ $rating->comment }}
                                </div>
                                <div class="comment_buttons">
                                    <a class="btn" href="#"><i class="far fa-thumbs-up"></i> Lubię <strong>(342)</strong></a>
                                    <a class="btn" href="#"><i class="far fa-thumbs-down"></i> Nie lubię <strong>(57)</strong></a>
                                    <a class="btn" href="#"><i class="fas fa-quote-right"></i> Cytuj</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection