<?php

return [
    'name' => 'Nazwa',
    'type' => 'Typ',
    'created_at' => 'Data utworzenia',
    'latitude' => 'Szerokość geograficzna',
    'longitude' => 'Wysokość geograficzna',
    'address' => 'Adres',
    'logo' => 'Logo',
    'color' => 'Kolor',
    'email' => 'Email',
    'password' => 'Hasło',
    'logout' => 'Wyloguj',
    'categories' => 'Kategorie',
    'description' => 'Opis',
    'components' => 'Składniki',
    'price' => 'Cena',
    'image' => 'Zdjęcie',
    'logo' => 'Logo',
    'code' => 'Kod',
    'rate' => 'Ocena',
    'comment' => 'Komentarz',
    'change_password' => 'Zmień hasło',
    'current_password' => 'Aktualne hasło',
    'repeat_password' => 'Powtórz hasło',
    'open' => 'Otwarcie',
    'close' => 'Zamknięcie',
    'from' => 'Od',
    'to' => 'Do',
    'hide_rated' => 'Ukryj ocenione',
    'hide_not_rated' => 'Ukryj nieocenione',
    'want_see' => 'Tylko te, które chcę zobaczyć',
    'want_see_local' => 'Tylko lokale, które chce zobaczyć',
    'want_see_event' => 'Tylko wydarzenia, które chce zobaczyć',
    'free_entrance' => 'Wstęp bezpłatny',
    'search' => 'Wyszukaj',
    'send' => 'Wyślij',

    //auth
    'sing_in' => 'Zaloguj się',
    'login' => 'Logowanie',
    'sing_out' => 'Wyloguj się',
    'register' => 'Zarejestruj się',
    'registration' => 'Rejestracja',
    'create_account' => 'Utwórz konto',
    'edit_account' => 'Edytuj konto',
    'password_forget' => 'Nie pamiętam hasła',
    'send_password_reminder' => 'Wyślij przypomnienie hasła',

    //buttons
    'create' => 'Utwórz',
    'edit' => 'Edytuj',
    'delete' => 'Usuń',
    'save' => 'Zapisz',
    'preview' => 'Szczegóły',

    //locals
    'locals' => 'Lokale',
    'local' => 'Lokal',
    'locals_list' => 'Lista lokali',
    'create_local' => 'Utwórz lokal',
    'edit_local' => 'Edytuj lokal',

    //local types
    'locals_types' => 'Typy lokali',
    'local_typ' => 'Typ lokalu',
    'local_types' => 'Typy lokalu',
    'locals_types_list' => 'Lista typów lokali',
    'create_local_type' => 'Utwórz typ lokalu',
    'edit_local_type' => 'Edytuj typ lokalu',

    //users
    'users' => 'Użytkownicy',
    'user' => 'Użytkownik',
    'users_list' => 'Lista użytkowników',
    'create_user' => 'Utwórz użytkownika',
    'edit_user' => 'Edytuj użytkownika',

    //events
    'events' => 'Wydarzenia',
    'event' => 'Wydarzenie',
    'events_list' => 'Lista wydarzeń',
    'create_event' => 'Utwórz wydarzenie',
    'edit_event' => 'Edytuj wydarzenie',

    //events_periodic
    'events_periodic' => 'Wydarzenia cykliczme',
    'event_periodic' => 'Wydarzenie cukliczne',
    'events_periodic_list' => 'Lista wydarzeń cyklicznych',
    'create_event_periodic' => 'Utwórz wydarzenie cykliczne',
    'edit_event_periodic' => 'Edytuj wydarzenie cykliczne',

    //events_types
    'events_types' => 'Typy wydarzeń',
    'event_typ' => 'Typ wydarzenia',
    'events_types_list' => 'Lista typów wydarzeń',
    'create_event_type' => 'Utwórz typ wydarzenia',
    'edit_event_type' => 'Edytuj typ wydarzenia',
    'event_types' => 'Typy eventu',

    //menu
    'menu' => 'Menu',
    'menu_list' => 'Lista menu',

    //menu_items
    'menu_items' => 'Pozycje w menu',
    'menu_items_list' => 'Lista pozycji w menu',
    'create_menu_item' => 'Utwórz pozycje w menu',
    'edit_menu_item' => 'Edytuj pozycje w menu',

    //menu_item_categories
    'menu_item_categories_list' => 'Lista kategorii pozycji w menu',
    'create_menu_item_category' => 'Utwórz kategorie pozycji w menu',

    //locals_ratings
    'locals_ratings' => 'Opinie o lokalach',
    'local_ratings' => 'Opinie o lokalu',
    'ratings' => 'Opinie',
    'rating' => 'Opinia',
    'locals_ratings_list' => 'Lista opini o lokalach',
    'local_ratings_list' => 'Lista opini o lokalu',
    'add_local_rating' => 'Wystaw ocene lokalu',

    //local_relations_list
    'relations' => 'Relacje',
    'local_relations_list' => 'Lista relacji lokalu',

    //locals_visit_codes
    'locals_visit_codes' => 'Kody wizyt lokali',
    'local_visit_codes' => 'Kody wizyt lokalu',
    'locals_visit_codes_list' => 'Lista kodów wizyt lokali',
    'local_visit_codes_list' => 'Lista kodów wizyt lokalu',
    'local_visit_code' => 'Kod wizyty lokalu',

    'opening_hours' => 'Godziny otwarcia',
    'edit_local_opening_hours' => 'Edycja godziń pracy',

    'days_of_week' => [
        'monday' => [
            'pl' => 'Poniedziałek',
        ],
        'tuesday' => [
            'pl' => 'Wtorek',
        ],
        'wednesday' => [
            'pl' => 'Środa',
        ],
        'thursday' => [
            'pl' => 'Czwartek',
        ],
        'friday' => [
            'pl' => 'Piątek',
        ],
        'saturday' => [
            'pl' => 'Sobota',
        ],
        'sunday' => [
            'pl' => 'Niedziela',
        ],
    ],

    //search
    'rating_from' => 'Ocena od',
    'rating_to' => 'Ocena do',
    'count_rating_from' => 'Liczba ocen od',
    'count_rating_to' => 'Liczba ocen do',
    'start_from' => 'Termin od',
    'start_to' => 'Termin do'
];