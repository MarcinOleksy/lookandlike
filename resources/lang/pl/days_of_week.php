<?php

return [

    'days' => [
        '1' => [
           'shortPL' => 'Pn',
           'shortEN' => 'Mon',
           'longPL' => 'Poniedziałek',
           'longEN' => 'Monday'
        ],
        '2' => [
            'shortPL' => 'Wt',
            'shortEN' => 'Tue',
            'longPL' => 'Wtorek',
            'longEN' => 'Tuesday'
        ],
        '3' => [
            'shortPL' => 'Śr',
            'shortEN' => 'Wed',
            'longPL' => 'Środa',
            'longEN' => 'Wednesday'
        ],
        '4' => [
            'shortPL' => 'Czw',
            'shortEN' => 'Thu',
            'longPL' => 'Czwartek',
            'longEN' => 'Thursday'
        ],
        '5' => [
            'shortPL' => 'Pt',
            'shortEN' => 'Fri',
            'longPL' => 'Piątek',
            'longEN' => 'Friday'
        ],
        '6' => [
            'shortPL' => 'Sob',
            'shortEN' => 'Sat',
            'longPL' => 'Sobota',
            'longEN' => 'Saturday'
        ],
        '7' => [
            'shortPL' => 'Niedz',
            'shortEN' => 'Sun',
            'longPL' => 'Niedziela',
            'longEN' => 'Sunday'
        ],
    ]

];