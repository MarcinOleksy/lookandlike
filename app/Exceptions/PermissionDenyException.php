<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.04.2020
 * Time: 00:47
 */

namespace App\Exceptions;


use Throwable;

class PermissionDenyException extends \Exception
{
    public function render()
    {
        return response(view($this->getMessage().'.commons.permissionDeny'));
    }
}