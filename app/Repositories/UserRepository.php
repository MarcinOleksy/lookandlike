<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 25.08.2019
 * Time: 20:52
 */

namespace App\Repositories;


use App\Models\AclUserGroup;
use App\Models\User;
use App\Repositories\Commons\Repository;

class UserRepository extends Repository
{
    public function managerPluck()
    {
        $query = $this->query();

        $query->whereHas('acl_group', function ($subquery){
            $subquery->where('slug', AclUserGroup::MANAGE_SLUG);
        });

        return $query->pluck('name','id');
    }

    protected function model(): string
    {
        return User::class;
    }
}