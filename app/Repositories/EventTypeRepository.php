<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 29.10.2019
 * Time: 22:38
 */

namespace App\Repositories;


use App\Models\EventType;
use App\Repositories\Commons\Repository;

class EventTypeRepository extends Repository
{
    protected function model(): string
    {
        return EventType::class;
    }
}