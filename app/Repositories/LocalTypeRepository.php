<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 25.08.2019
 * Time: 20:53
 */

namespace App\Repositories;


use App\Models\LocalType;
use App\Repositories\Commons\Repository;

class LocalTypeRepository extends Repository
{
    protected function model(): string
    {
       return LocalType::class;
    }
}