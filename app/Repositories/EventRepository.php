<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 29.10.2019
 * Time: 22:38
 */

namespace App\Repositories;


use App\Models\Event;
use App\Repositories\Commons\Repository;
use Illuminate\Database\Eloquent\Model;

class EventRepository extends Repository
{
    public function findOrFail(int $id = null):Model
    {
        return $this->query()->with('local.local_type','local.menu.menu_items.category')->findOrFail($id);
    }

    protected function model(): string
    {
        return Event::class;
    }
}