<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 26.04.2020
 * Time: 23:53
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\LocalRelation;
use App\Models\User;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;

class LocalRelationRepository extends Repository
{
    public function paginateByLocal(Local $local, Filter $filter = null)
    {
        $query = $this->query();

        if($filter)
            $filter->filter($query);

        $query->where('local_id', $local->id)
            ->where('expired', 0);

        $query->with('user');

        return $query->paginate();
    }

    public function paginateByUser(User $user, Filter $filter = null)
    {
        $query = $this->query();

        if($filter)
            $filter->filter($query);

        $query->where('user_id', $user->id)
            ->where('expired', 0);

        return $query->paginate();
    }

    protected function model(): string
    {
        return LocalRelation::class;
    }
}