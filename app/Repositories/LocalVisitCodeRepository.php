<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:28
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\LocalVisitCode;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;

class LocalVisitCodeRepository extends Repository
{
    public function getNotPrintedForLocal(Local $local)
    {
        $query = $this->query();

        $query->whereNull('printed_at')
            ->where('local_id', $local->id);

        return $query->get();
    }

    public function findCode(string $code)
    {
        return $this->query()->where('code', $code)->first();
    }

    public function paginateForLocal(Local $local, Filter $filter = null)
    {
        $query = $this->query();

        $query->where('local_id', $local->id);

        if($filter)
            $filter->filter($query);

        return $query->paginate();
    }

    protected function model(): string
    {
        return LocalVisitCode::class;
    }
}