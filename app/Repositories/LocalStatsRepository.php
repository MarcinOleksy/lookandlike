<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 31.03.2020
 * Time: 01:04
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\LocalStats;
use App\Repositories\Commons\Repository;
use Illuminate\Database\Eloquent\Model;

class LocalStatsRepository extends Repository
{
    protected function model(): string
    {
        return LocalStats::class;
    }

    public function findForLocal(Local $local): Model
    {
        $query = $this->query();

        $query->where('local_id', $local->id);

        return $query->first();
    }
}