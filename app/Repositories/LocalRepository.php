<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 25.08.2019
 * Time: 20:52
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\User;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class LocalRepository extends Repository
{
    public function pluckRatingByUser(User $user)
    {
        $query = $this->query();

        $query->whereHas('local_ratings', function ($subquery) use ($user){
            $subquery->where('user_id', $user->id);
        });

        return $query->pluck('name','id');
    }


    public function findOrFail(int $id = null): Model
    {
        $query = $this->query();

        $query->with('local_type','local_opening_hours');

        return $query->findOrFail($id);
    }

    public function paginateForOwner(User $owner, Filter $filter)
    {
        $query = $this->query();

        $query->whereHas('local_owners', function ($subquery) use ($owner){
            $subquery->where('user_id', $owner->id);
        });

        if($filter)
            $filter->filter($query);

        return $query->paginate();
    }

    public function paginateFrontIndex(Filter $filter = null): LengthAwarePaginator
    {
        $query = $this->query();

        if($filter)
            $filter->filter($query);

        $query->with('local_type','local_opening_hours');

        return $query->paginate();
    }


    public function paginate(Filter $filter = null): LengthAwarePaginator
    {
        $query = $this->query();

        if($filter)
            $filter->filter($query);

        $query->with('local_type');

        return $query->paginate();
    }

    protected function model(): string
    {
        return Local::class;
    }
}