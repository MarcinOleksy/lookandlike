<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:29
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\Menu;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;

class MenuRepository extends Repository
{
    public function paginateForLocal(Local $local, Filter $filter = null)
    {
        $query = $this->query();

        $query->whereHas('local', function ($query) use ($local){
            $query->where('locals.id', $local->id);
        });

        if($filter) $filter->filter($query);

        return $query->paginate();
    }

    protected function model(): string
    {
        return Menu::class;
    }
}