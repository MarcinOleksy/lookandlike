<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.12.2019
 * Time: 21:26
 */

namespace App\Repositories;


use App\Models\LocalOpeningHours;
use App\Repositories\Commons\Repository;

class LocalOpeningHoursRepository extends Repository
{
    public function findOrNewByLocalId(int $local_id)
    {
        $model = $this->query()->where('local_id', $local_id)->first() ?: $this->makeModel();

        $model->local_id = $local_id;

        return $model;
    }

    protected function model(): string
    {
        return LocalOpeningHours::class;
    }
}