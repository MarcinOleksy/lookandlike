<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 25.08.2019
 * Time: 20:37
 */

namespace App\Repositories\Commons;




use App\Repositories\Filters\Commons\Filter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    abstract protected function model():string;

    protected function query():Builder
    {
        return $this->makeModel()->newQuery();
    }

    protected function makeModel()
    {
        $class = $this->model();
        return new $class;
    }

    public function paginate(Filter $filter = null): LengthAwarePaginator
    {
        $query = $this->query();

        if($filter)
            $filter->filter($query);

        return $query->paginate();
    }

    public function get(Filter $filter = null): Collection
    {
        $query = $this->query();

        if($filter) $filter->filter($query);

        return $query->get();
    }

    public function findOrFail(int $id = null): Model
    {
        return $this->query()->findOrFail($id);
    }

    public function findOrNew(int $id = null): Model
    {
        $model = $this->query()->find($id) ?: $this->makeModel();

        return $model;
    }

    public function create(array $data): Model
    {
        return $this->query()->create($data);
    }

    public function update(int $id, array  $data): Model
    {
        $model = $this->findOrFail($id);
        $model->fill($data);
        $this->store($model);
        return $model;
    }

    public function updateByData(Model $model, array $data)
    {
        $model->fill($data);

        return $model;
    }

    public function store(Model $model): bool
    {
        return $model->save();
    }

    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    public function pluck()
    {
        return $this->query()->pluck('name','id');
    }
};