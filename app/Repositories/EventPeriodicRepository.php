<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 15:48
 */

namespace App\Repositories;


use App\Models\EventPeriod;
use App\Repositories\Commons\Repository;

class EventPeriodicRepository extends Repository
{
    protected function model(): string
    {
        return EventPeriod::class;
    }
}