<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:29
 */

namespace App\Repositories;


use App\Models\Menu;
use App\Models\MenuItem;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;

class MenuItemRepository extends Repository
{
    public function paginateForMenu(Menu $menu, Filter $filter = null)
    {
        $query = $this->query();

//        $query->whereHas('menu', function ($query) use ($menu){
//            $query->where('menu.id', $menu->id);
//        });

        if($filter) $filter->filter($query);

        return $query->paginate();
    }

    protected function model(): string
    {
        return MenuItem::class;
    }
}