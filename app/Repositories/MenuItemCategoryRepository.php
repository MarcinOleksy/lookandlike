<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:31
 */

namespace App\Repositories;


use App\Models\MenuItemCategory;
use App\Repositories\Commons\Repository;

class MenuItemCategoryRepository extends Repository
{
    protected function model(): string
    {
        return MenuItemCategory::class;
    }
}