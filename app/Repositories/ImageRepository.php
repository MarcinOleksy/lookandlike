<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 29.10.2019
 * Time: 22:49
 */

namespace App\Repositories;


use App\Models\Image;
use App\Repositories\Commons\Repository;

class ImageRepository extends Repository
{

    protected function model(): string
    {
        return Image::class;
    }
}