<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 07.05.2020
 * Time: 22:05
 */

namespace App\Repositories\Filters\Dashboard;


use App\Repositories\Filters\Commons\Filter;
use Carbon\Carbon;

class LocalVisitCodeIndexFilter extends Filter
{
    const DATETIME_FORMAT = 'Y-m-d H:i';

    protected $filterKeys = [
        'printed_at',
    ];

    public function printed_at_filter(int $printed_at)
    {
        if($printed_at)
            $this->builder->whereNotNull('printed_at');
        else
            $this->builder->whereNull('printed_at');
    }
}