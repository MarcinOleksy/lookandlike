<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 07.05.2020
 * Time: 22:05
 */

namespace App\Repositories\Filters\Dashboard;


use App\Repositories\Filters\Commons\Filter;
use Carbon\Carbon;

class UserIndexFilter extends Filter
{
    const DATETIME_FORMAT = 'Y-m-d H:i';

    protected $filterKeys = [
        'search_engine',
        'acl_group',
        'created_from',
        'created_to'
    ];

    public function search_engine_filter(string $search_engine)
    {
        $this->builder->where(function ($subquery) use ($search_engine){
            $subquery->where('name', 'LIKE', '%'.$search_engine.'%')
                ->orWhere('email', 'LIKE', '%'.$search_engine.'%');
        });
    }

    public function acl_group_filter(int $acl_group)
    {
        $this->builder->whereHas('acl_group', function ($subquery) use ($acl_group){
            $subquery->where('id',$acl_group);
        });
    }

    public function created_from_filter(string $created_from)
    {
        $created_from = Carbon::parse($created_from)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at >= "'.$created_from.'"');
    }

    public function created_to_filter(string $created_to)
    {
        $created_to = Carbon::parse($created_to)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at <= "'.$created_to.'"');
    }
}