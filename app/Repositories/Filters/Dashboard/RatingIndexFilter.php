<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 24.03.2020
 * Time: 21:41
 */

namespace App\Repositories\Filters\Dashboard;


use App\Repositories\Filters\Commons\Filter;
use Carbon\Carbon;


class RatingIndexFilter extends Filter
{
    const DATETIME_FORMAT = 'Y-m-d H:i';

    protected $filterKeys = [
        'rating_from',
        'rating_to',
        'created_from',
        'created_to',
    ];

    public function rating_from_filter(int $rating)
    {
        $this->builder->where('rating', '>=', $rating);
    }

    public function rating_to_filter(int $rating)
    {
        $this->builder->where('rating', '<=', $rating);
    }

    public function created_from_filter(string $created_from)
    {
        $created_from = Carbon::parse($created_from)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at >= "'.$created_from.'"');
    }

    public function created_to_filter(string $created_to)
    {
        $created_to = Carbon::parse($created_to)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at <= "'.$created_to.'"');
    }
}