<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:44
 */

namespace App\Repositories\Filters\Dashboard;


use App\Repositories\Filters\Commons\Filter;
use Illuminate\Support\Facades\Auth;

class LocalIndexFilter extends Filter
{
    protected $filterKeys = [
        'local_types',
        'rating_from',
        'rating_to',
        'count_rating_from',
        'count_rating_to',
    ];

    public function local_types_filter(array $types)
    {
        $this->builder->whereHas('local_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function rating_from_filter(int $rating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($rating){
            $query->where('rating', '>=', $rating);
        });
    }

    public function rating_to_filter(int $rating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($rating){
            $query->where('rating', '<=', $rating);
        });
    }

    public function count_rating_from_filter(int $countRating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($countRating){
            $query->where('count_rating', '>=', $countRating);
        });
    }

    public function count_rating_to_filter(int $countRating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($countRating){
            $query->where('count_rating', '<=', $countRating);
        });
    }
}