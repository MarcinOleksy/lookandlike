<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:44
 */

namespace App\Repositories\Filters\Dashboard;


use App\Repositories\Filters\Commons\Filter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EventIndexFilter extends Filter
{
    const DATETIME_FORMAT = 'Y-m-d H:i';

    protected $filterKeys = [
        'local_types',
        'event_types',
        'start_from',
        'start_to',
        'rating_from',
        'rating_to',
        'count_rating_from',
        'count_rating_to',
    ];

    public function local_types_filter(array $types)
    {
        $this->builder->whereHas('local.local_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function event_types_filter(array $types)
    {
        $this->builder->whereHas('event_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function start_from_filter(string $start_from)
    {
        $start_from = Carbon::parse($start_from)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('finish_at >= DATE_SUB("'.$start_from.'", INTERVAL 2 HOUR)');
    }

    public function start_to_filter(string $start_to)
    {
        $start_to = Carbon::parse($start_to)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('start_at <= DATE_SUB("'.$start_to.'", INTERVAL 2 HOUR)');
    }

    public function rating_from_filter(int $rating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($rating) {
            $query->where('rating', '>=', $rating);
        });
    }

    public function rating_to_filter(int $rating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($rating) {
            $query->where('rating', '<=', $rating);
        });
    }

    public function count_rating_from_filter(int $countRating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($countRating) {
            $query->where('count_rating', '>=', $countRating);
        });
    }

    public function count_rating_to_filter(int $countRating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($countRating) {
            $query->where('count_rating', '<=', $countRating);
        });
    }
}