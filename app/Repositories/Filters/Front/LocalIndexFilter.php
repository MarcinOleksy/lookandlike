<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:44
 */

namespace App\Repositories\Filters\Front;


use App\Repositories\Filters\Commons\Filter;
use Illuminate\Support\Facades\Auth;

class LocalIndexFilter extends Filter
{
    protected $filterKeys = [
        'local_types',
        'rating_from',
        'rating_to',
        'count_rating_from',
        'count_rating_to',
        'hide_rated',
        'hide_not_rated',
        'want_see',
        'free_entrance',
    ];

    public function local_types_filter(array $types)
    {
        $this->builder->whereHas('local_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function rating_from_filter(int $rating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($rating){
            $query->where('rating', '>=', $rating);
        });
    }

    public function rating_to_filter(int $rating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($rating){
            $query->where('rating', '<=', $rating);
        });
    }

    public function count_rating_from_filter(int $countRating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($countRating){
            $query->where('count_rating', '>=', $countRating);
        });
    }

    public function count_rating_to_filter(int $countRating)
    {
        $this->builder->whereHas('local_stats', function ($query) use ($countRating){
            $query->where('count_rating', '<=', $countRating);
        });
    }

    public function hide_rated_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;

        $this->builder->whereDoesntHave('local_ratings', function ($query) use ($user){
            $query->where('user_id', $user->id);
        });
    }

    public function hide_not_rated_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;

        $this->builder->whereHas('local_ratings', function ($query) use ($user){
            $query->where('user_id', $user->id);
        });
    }

    public function want_see_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;
        $this->builder->whereHas('local_followers', function ($query) use ($user){
            $query->where('user_id', $user->id);
        });
    }

    public function free_entrance_filter(int $value)
    {

    }
}