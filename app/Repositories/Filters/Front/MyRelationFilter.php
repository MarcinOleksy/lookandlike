<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 24.03.2020
 * Time: 21:41
 */

namespace App\Repositories\Filters\Front;


use App\Repositories\Filters\Commons\Filter;
use Carbon\Carbon;


class MyRelationFilter extends Filter
{
    const DATETIME_FORMAT = 'Y-m-d H:i';

    protected $filterKeys = [
        'local_id',
        'created_from',
        'created_to',
    ];

    public function local_id_filter(array $local_id)
    {
        $this->builder->whereIn('local_id', $local_id);
    }

    public function created_from_filter(string $created_from)
    {
        $created_from = Carbon::parse($created_from)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at >= "'.$created_from.'"');
    }

    public function created_to_filter(string $created_to)
    {
        $created_to = Carbon::parse($created_to)->format(self::DATETIME_FORMAT);
        $this->builder->whereRaw('created_at <= "'.$created_to.'"');
    }
}