<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:44
 */

namespace App\Repositories\Filters\Front;


use App\Repositories\Filters\Commons\Filter;
use Illuminate\Support\Facades\Auth;

class EventIndexFilter extends Filter
{
    protected $filterKeys = [
        'local_types',
        'event_types',
        'start_from',
        'start_to',
        'rating_from',
        'rating_to',
        'count_rating_from',
        'count_rating_to',
        'hide_rated',
        'hide_not_rated',
        'want_see_local',
        'want_see_event',
        'free_entrance',
    ];

    public function local_types_filter(array $types)
    {
        $this->builder->whereHas('local.local_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function event_types_filter(array $types)
    {
        $this->builder->whereHas('event_type', function ($query) use ($types){
            $query->whereIn('id', $types);
        });
    }

    public function start_from_filter(string $start_from)
    {
        $this->builder->whereRaw('finish_at >= DATE_SUB("'.$start_from.'", INTERVAL 2 HOUR)');
    }

    public function start_to_filter(string $start_to)
    {
        $this->builder->whereRaw('start_at <= DATE_SUB("'.$start_to.'", INTERVAL 2 HOUR)');
    }

    public function rating_from_filter(int $rating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($rating) {
            $query->where('rating', '>=', $rating);
        });
    }

    public function rating_to_filter(int $rating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($rating) {
            $query->where('rating', '<=', $rating);
        });
    }

    public function count_rating_from_filter(int $countRating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($countRating) {
            $query->where('count_rating', '>=', $countRating);
        });
    }

    public function count_rating_to_filter(int $countRating)
    {
        $this->builder->whereHas('local.local_stats', function($query) use ($countRating) {
            $query->where('count_rating', '<=', $countRating);
        });
    }

    public function hide_rated_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;

        $this->builder->whereHas('local', function($query) use ($user) {
            $query->whereDoesntHave('local_ratings', function ($query) use ($user){
                $query->where('user_id', $user->id);
            });
        });
    }

    public function hide_not_rated_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;

        $this->builder->whereHas('local', function($query) use ($user) {
            $query->whereHas('local_ratings', function ($query) use ($user){
                $query->where('user_id', $user->id);
            });
        });
    }

    public function want_see_filter(int $value)
    {
        $user = Auth::user();
        if(!$user) return;

        $this->builder->whereHas('local', function($query) use ($user) {
            $query->whereHas('local_followers', function ($query) use ($user){
                $query->where('user_id', $user->id);
            });
        });
    }

    public function free_entrance_filter(int $value)
    {

    }
}