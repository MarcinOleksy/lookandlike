<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 25.08.2019
 * Time: 20:46
 */

namespace App\Repositories\Filters\Commons;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class Filter
{
    protected $builder;

    protected $filterKeys = [];

    protected $filtersValues = [];

    public function __construct(Request $request)
    {
        $this->setValues($request->only($this->filterKeys));
    }

    public function filter(Builder $builder):Builder
    {
        $this->builder = $builder;
        $this->executeFilters();
        return $this->builder;
    }

    protected function executeFilters()
    {
        foreach ($this->filterKeys as $filter)
        {
            $filterMethod = $filter.'_filter';
            if(method_exists($this, $filterMethod) && ($value = $this->getFilterParameter($filter)) != null)
                $this->{$filterMethod}($value);
        }
    }

    protected function getFilterParameter(string $filter)
    {
        return Arr::get($this->filtersValues, $filter);
    }

    public function setValues(array $filtersValues): void
    {
        $this->filtersValues = $filtersValues;
    }

    public function getFilterValues()
    {
        return $this->filtersValues;
    }

    public function getFilterValue(string $name)
    {
        if(isset($this->filtersValues[$name]))
            return $this->filtersValues[$name];

        return false;
    }
}