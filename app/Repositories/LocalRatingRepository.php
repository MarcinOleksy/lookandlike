<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:29
 */

namespace App\Repositories;


use App\Models\Local;
use App\Models\LocalRating;
use App\Models\User;
use App\Repositories\Commons\Repository;
use App\Repositories\Filters\Commons\Filter;

class LocalRatingRepository extends Repository
{
    public function itemAllStatsForLocal(Local $local)
    {
        $query = $this->query();

        $query->where('local_id', $local->id);

        $query->selectRaw("round(AVG(rating),1) as rating, COUNT(id) as 'count_rating', sum(if(comment is null, 0, 1)) as 'count_comments'");

        return $query->first();
    }

    public function getForLocal(Local $local)
    {
        $query = $this->query();

        $query->where('local_id', $local->id);

        return $query->get();
    }

    public function paginateForUser(User $user, Filter $filter = null)
    {
        $query = $this->query();

        $query->where('user_id', $user->id);

        if($filter) $filter->filter($query);

        $query->with('local.local_type');
        $query->orderBy('created_at', 'DESC');

        return $query->paginate();
    }

    public function paginateForLocal(Local $local, Filter $filter = null)
    {
        $query = $this->query();

        $query->where('local_id', $local->id);

        if($filter)
            $filter->filter($query);

        $query->with('user');
        $query->orderBy('created_at', 'DESC');

        return $query->paginate();
    }

    protected function model(): string
    {
        return LocalRating::class;
    }
}