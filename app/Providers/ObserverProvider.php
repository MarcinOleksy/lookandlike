<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 31.03.2020
 * Time: 01:10
 */

namespace App\Providers;


use App\Models\Local;
use App\Models\LocalRating;
use App\Observers\LocalObserver;
use App\Observers\LocalRatingObserver;
use Illuminate\Support\ServiceProvider;

class ObserverProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        Local::observe(LocalObserver::class);
        LocalRating::observe(LocalRatingObserver::class);
    }
}