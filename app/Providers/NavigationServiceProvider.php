<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 03.10.2019
 * Time: 22:44
 */

namespace App\Providers;


use App\Navigation\ConvertNavigation;
use Illuminate\Support\ServiceProvider;

class NavigationServiceProvider extends ServiceProvider
{
    public $na = [];

    public function boot()
    {
        $this->na = ConvertNavigation::getInstance()->convert();
    }

    public function register()
    {
        $this->app->instance('na', $this);
        $this->app->bind(self::class, $this);
    }
}