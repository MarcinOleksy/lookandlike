<?php
/**
 * Created by PhpStorm.
 * User: mikol
 * Date: 24.06.2019
 * Time: 15:31
 */

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('isCurrentPassword', function ($attribute, $value, $parameters, $validator){

            return Hash::check($value, Auth::user()->password);
        });

    }
}