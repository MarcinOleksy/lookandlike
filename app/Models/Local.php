<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Local
 * @package App\Models
 * @property int id
 * @property int type_id
 * @property LocalType local_type
 * @property int local_stats_id
 * @property LocalStats local_stats
 * @property string name
 * @property double latitude
 * @property double longitude
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 * @property Collection locals_owners
 */
class Local extends Model
{
    use SoftDeletes;

    protected $table = 'locals';

    protected $fillable = [
        'type_id',
        'local_stats_id',
        'image_id',
        'name',
        'address',
        'description',
        'latitude',
        'longitude'
    ];

    protected $with = [
        'local_type',
        'local_owners',
        'local_stats',
        'image_main'
    ];

    public function local_owners()
    {
        return $this->belongsToMany(User::class, 'locals_owners','local_id', 'user_id');
    }

    public function local_type()
    {
        return $this->belongsTo(LocalType::class, 'type_id');
    }

    public function local_opening_hours()
    {
        return $this->hasOne(LocalOpeningHours::class, 'local_id');
    }

    public function local_stats()
    {
        return $this->belongsTo(LocalStats::class, 'local_stats_id');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'local_id');
    }

    public function local_ratings()
    {
        return $this->hasMany(LocalRating::class, 'local_id');
    }

    public function local_followers()
    {
        return $this->belongsToMany(User::class, 'locals_followers', 'local_id', 'user_id');
    }

    public function menu()
    {
        return $this->hasMany(Menu::class,'local_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }




    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}
