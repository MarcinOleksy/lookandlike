<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.12.2019
 * Time: 21:08
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LocalOpeningHours extends Model
{
    protected $table = "locals_opening_hours";

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'local_id',
        'monday_open',
        'monday_close',
        'tuesday_open',
        'tuesday_close',
        'wednesday_open',
        'wednesday_close',
        'thursday_open',
        'thursday_close',
        'friday_open',
        'friday_close',
        'saturday_open',
        'saturday_close',
        'sunday_open',
        'sunday_close',
    ];

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }
    
    public function getOpeningHoursForNumberDay(int $numberDay):string
    {
        if($numberDay < 0 || $numberDay > 7)
            return '';

        $open = $this->{strtolower(trans('days_of_week.days.'.$numberDay.'.longEN')).'_open'};
        $close = $this->{strtolower(trans('days_of_week.days.'.$numberDay.'.longEN')).'_close'};

        return $open.' - '.$close;
    }
}