<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:03
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuItems
 * @package App\Models
 * @property int menu_id
 * @property Menu menu
 * @property int category_id
 * @property MenuItemCategory category
 * @property int image_id
 * @property Image image
 * @property string name
 * @property string description
 * @property string components
 * @property double price
 */
class MenuItem extends Model
{
    protected $table = 'menu_items';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    protected $fillable = array(
        'menu_id',
        'category_id',
        'image_id',
        'name',
        'description',
        'components',
        'price'
    );

    protected $with = [
        'image_main'
    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function category()
    {
        return $this->belongsTo(MenuItemCategory::class, 'category_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }
}