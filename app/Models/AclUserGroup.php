<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 20:21
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AclUserGroup extends Model
{
    const ADMIN_ID = 1;
    const MANAGER_ID = 2;
    const USER_ID = 3;

    const ADMIN_SLUG = 'admin';
    const MANAGE_SLUG = 'manager';
    const USER_SLUG = 'user';

    protected $table = 'acl_user_has_groups';
}