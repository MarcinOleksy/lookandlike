<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:03
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * @package App\Models
 * @property int id
 * @property string name
 * @property Carbon created_at
 */
class MenuItemCategory extends Model
{
    protected $table = 'menu_item_categories';

    const UPDATED_AT = NULL;

    protected $fillable = array(
        'name'
    );

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }
}