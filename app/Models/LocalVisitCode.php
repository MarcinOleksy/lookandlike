<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:11
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LocalVisitCode
 * @package App\Models
 * @property int local_id
 * @property Local local
 * @property string code
 * @property string path
 * @property string printed_at
 * @property boolean expired
 */
class LocalVisitCode extends Model
{
    protected $table = 'locals_visit_codes';

    const UPDATED_AT = null;

    protected $fillable = [
        'local_id',
        'code',
        'path',
        'printed_at',
        'expired',
    ];

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function getPrintedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }
}