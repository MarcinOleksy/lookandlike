<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 29.10.2019
 * Time: 19:38
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 * @property int id
 * @property string original_image_path
 * @property string thumbnail_image_path
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 */
class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'original_image_path',
        'thumbnail_image_path'
    ];

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}