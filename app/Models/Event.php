<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Null_;

/**
 * Class Event
 * @package App\Models
 * @property int event_periodic_id
 * @property EventPeriod $event_periodic
 * @property int type_id
 * @property EventType event_type
 * @property int image_id
 * @property Image image
 * @property int local_id
 * @property Local local
 * @property string title
 * @property string description
 * @property string status
 * @property Carbon start_at
 * @property Carbon finish_at
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 */
class Event extends Model
{
    use SoftDeletes;

    protected $table = 'events';

    protected $fillable = [
        'event_periodic_id',
        'type_id',
        'local_id',
        'image_id',
        'title',
        'description',
        'status',
        'start_at',
        'finish_at',
    ];

    protected $with = [
        'local',
        'event_type',
        'image_main'
    ];

    CONST STATUS_PREPARED = 'prepared';
    CONST STATUS_STARTED = 'started';
    CONST STATUS_FINISHED = 'finished';

    CONST STATUSES = [
        self::STATUS_PREPARED,
        self::STATUS_STARTED,
        self::STATUS_FINISHED,
    ];

    public function event_periodic()
    {
        return $this->belongsTo(EventPeriod::class, 'event_periodic_id');
    }

    public function event_type()
    {
        return $this->belongsTo(EventType::class, 'type_id');
    }

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getStartAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getFinishAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}
