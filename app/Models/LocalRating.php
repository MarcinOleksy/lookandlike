<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:14
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalRating extends Model
{
    use SoftDeletes;

    protected $table = 'locals_ratings';

    protected $fillable = [
        'local_id',
        'code_id',
        'user_id',
        'image_id',
        'rating',
        'comment',
    ];

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function code()
    {
        return $this->belongsTo(LocalVisitCode::class, 'code_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}