<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 26.04.2020
 * Time: 13:20
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LocalRelation extends Model
{
    protected $table = 'locals_relations';

    protected $fillable = [
        'local_id',
        'code_id',
        'user_id',
        'image_id',
        'comment',
        'expired',
    ];

    protected $with = [
        'image_main'
    ];

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function code()
    {
        return $this->belongsTo(LocalVisitCode::class, 'code_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }

}