<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LocalType
 * @package App\Models
 * @property int id
 * @property string name
 * @property string logo_path
 * @property string color
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 */
class LocalType extends Model
{
    use SoftDeletes;

    protected $table = 'locals_types';

    public $fillable = [
        'name',
        'image_id',
    ];

    protected $with = [
        'image_main'
    ];

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}
