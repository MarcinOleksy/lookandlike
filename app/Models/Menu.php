<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 16:03
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * @package App\Models
 * @property int id
 * @property int local_id
 * @property Local local
 * @property int event_id
 * @property Event event
 * @property Carbon created_at
 */
class Menu extends Model
{
    protected $table = 'menu';

    const UPDATED_AT = null;

    protected $fillable = array(
        'local_id',
        'event_id'
    );

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function menu_items()
    {
        return $this->hasMany(MenuItem::class, 'menu_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }
    
}