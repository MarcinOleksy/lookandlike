<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 31.03.2020
 * Time: 00:23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalStats extends Model
{
    use SoftDeletes;

    protected $table = 'locals_stats';

    protected $fillable = [
        'local_id',
        'rating',
        'count_rating',
        'count_comments',
        'count_followers',
        'count_images'
    ];

    public function local()
    {
        return $this->hasOne(Local::class, 'local_stats_id');
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}