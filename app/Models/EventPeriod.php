<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 15:36
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EventPeriod
 * @package App\Models
 * @property int type_id
 * @property EventType event_type
 * @property int image_id
 * @property Image image
 * @property int local_id
 * @property Local local
 * @property string title
 * @property string description
 * @property string status
 * @property string frequency
 * @property int|null day_of_week
 * @property Carbon time_start
 * @property Carbon duration
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 */
class EventPeriod extends Model
{
    use SoftDeletes;

    protected $table = 'events_periodic';

    protected $fillable = [
        'type_id',
        'local_id',
        'image_id',
        'title',
        'description',
        'status',
        'day_of_week',
        'time_start',
        'duration',
    ];

    protected $with = [
        'image_main'
    ];

    public function event_type()
    {
        return $this->belongsTo(EventType::class, 'type_id');
    }

    public function local()
    {
        return $this->belongsTo(Local::class, 'local_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getTimeStartAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDurationAtnAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}