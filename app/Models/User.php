<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Junges\ACL\Http\Models\Group as Acl_Group;
use Junges\ACL\Traits\UsersTrait;

/**
 * Class User
 * @package App\Models
 * @property int id
 * @property string name
 * @property string email
 * @property string password
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon|null deleted_at
 */
class User extends Authenticatable
{
    use UsersTrait, Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'image_id'
    ];

    protected $guarded = [
        'id'
    ];

    protected $with = [
        'acl_group', 'image_main'
    ];

    public function locals()
    {
        return $this->belongsToMany(Local::class, 'locals_owners', 'user_id', 'local_id');
    }

    public function acl_group()
    {
        return $this->hasOneThrough(Acl_Group::class, AclUserGroup::class, 'user_id', 'id', 'id', 'group_id');
    }

    public function image_main()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getCreatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getUpdatedAtAttribute($value):Carbon
    {
        return Carbon::parse($value);
    }

    public function getDeletedAtAttribute($value):?Carbon
    {
        IF($value)
            return Carbon::parse($value);

        return NULL;
    }
}
