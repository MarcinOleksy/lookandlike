<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 03.09.2019
 * Time: 19:42
 */

namespace App\Navigation;

use App\Policies\Policy;
use phpDocumentor\Reflection\Types\Boolean;

class Url
{
    protected $url = null;
    protected $controller = null;
    protected $action = null;
    protected $label = null;
    protected $icon = null;
    protected $method = null;
    protected $children = null;
    protected $route = null;
    protected $breadcrumb = null;
    protected $positionInMenu = null;
    protected $auth = null;
    protected $middleware = [];
    protected $type = null;
    protected $permissionClass = null;
    protected $permissionAction = null;

    public function setUrl($url):Url
    {
        $this->url = $url;
        return $this;
    }

    public function setController($controller):Url
    {
        $this->controller = $controller;
        return $this;
    }


    public function setAction($action):Url
    {
        $this->action = $action;
        return $this;
    }


    public function setLabel($label):Url
    {
        $this->label = $label;
        return $this;
    }

    public function setIcon(string $icon):Url
    {
        $this->icon = $icon;
        return $this;
    }


    public function setMethod($method):Url
    {
        $this->method = $method;
        return $this;
    }

    public function setChildren($children): Url
    {
        $this->children = $children;
        return $this;
    }

    public function setRoute($route): Url
    {
        $this->route = $route;
        return $this;
    }

    public function setPositionInMenu(string $positionInMenu): Url
    {
        $this->positionInMenu = $positionInMenu;
        return $this;
    }

    public function setAuth(bool $auth): Url
    {
        $this->auth = $auth;
        return $this;
    }

    public function setMiddleware(array $middleware): Url
    {
        $this->middleware = $middleware;
        return $this;
    }

    public function setType($type): Url
    {
        $this->type = $type;
        return $this;
    }

    public function setBreadcrumb(string $breadcrumb)
    {
        $this->breadcrumb = $breadcrumb;
        return $this;
    }

    public function setPermissionClass(?string $permissionClass)
    {
        $this->permissionClass = $permissionClass;
        return $this;
    }

    public function setPermissionAction(?string $permissionAction)
    {
        $this->permissionAction = $permissionAction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }


    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }


    /**
     * @return string|null
     */
    public function getPositionInMenu(): ?string
    {
        return $this->positionInMenu;
    }

    /**
     * @return null|bool
     */
    public function getAuth(): ?bool
    {
        return $this->auth;
    }

    /**
     * @return array
     */
    public function getMiddleware(): array
    {
        return $this->middleware;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getBreadcrumb():?string
    {
        return $this->breadcrumb;
    }

    public function getPermissionClass():?string
    {
        return $this->permissionClass;
    }

    public function getPermissionAction():?string
    {
        return $this->permissionAction;
    }

    public function getPermission(array $routeParameters = []):?bool
    {
        if(is_null($this->permissionClass) || is_null($this->permissionAction))
            return null;

        return app($this->permissionClass)->setRouteParameters($routeParameters)->{$this->permissionAction}();
    }
}