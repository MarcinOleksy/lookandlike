<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 03.09.2019
 * Time: 19:36
 */

namespace App\Navigation;


//DASHBOARD
use App\Http\Controllers\Addons\ImageController;
use App\Http\Controllers\Dashboard\Auth\ChangeAccountController;
use App\Http\Controllers\Dashboard\Auth\ChangePasswordController;
use App\Http\Controllers\Dashboard\Auth\ForgotPasswordController;
use App\Http\Controllers\Dashboard\Auth\LoginController;
use App\Http\Controllers\Dashboard\Auth\ResetPasswordController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\EventController;
use App\Http\Controllers\Dashboard\EventPeriodicController;
use App\Http\Controllers\Dashboard\EventTypeController;
use App\Http\Controllers\Dashboard\LocalController;
use App\Http\Controllers\Dashboard\LocalOpeningHoursController;
use App\Http\Controllers\Dashboard\LocalRatingController;
use App\Http\Controllers\Dashboard\LocalRelationController;
use App\Http\Controllers\Dashboard\LocalTypeController;
use App\Http\Controllers\Dashboard\LocalVisitCodeController;
use App\Http\Controllers\Dashboard\MenuController;
use App\Http\Controllers\Dashboard\MenuItemCategoryController;
use App\Http\Controllers\Dashboard\MenuItemController;
use App\Http\Controllers\Dashboard\UserController;

//DASHBOARD POLICY
use App\Policies\Controllers\Dashboard\DashboardControllerPolicy;
use App\Policies\Controllers\Dashboard\EventTypeControllerPolicy;
use App\Policies\Controllers\Dashboard\LocalControllerPolicy;
use App\Policies\Controllers\Dashboard\LocalTypeControllerPolicy;
use App\Policies\Controllers\Dashboard\MenuItemCategoryControllerPolicy;
use App\Policies\Controllers\Dashboard\UserControllerPolicy;

//FRONT
use App\Http\Controllers\Front\Auth\ChangeAccountController as FrontChangeAccountController;
use App\Http\Controllers\Front\Auth\ChangePasswordController as FrontChangePasswordController;
use App\Http\Controllers\Front\Auth\ForgotPasswordController as FrontForgotPasswordController;
use App\Http\Controllers\Front\Auth\LoginController as FrontLoginController;
use App\Http\Controllers\Front\Auth\RegisterController as FrontRegisterController;
use App\Http\Controllers\Front\Auth\ResetPasswordController as FrontResetPasswordController;
use App\Http\Controllers\Front\LocalController as FrontLocalController;
use App\Http\Controllers\Front\EventController as FrontEventController;
use App\Http\Controllers\Front\HomeController as FrontHomeController;
use App\Http\Controllers\Front\LocalVisitCodeController as FrontLocalVisitCodeController;
use App\Http\Controllers\Front\LocalRatingController as FrontLocalRatingController;
use App\Http\Controllers\Front\MyRatingController as FrontMyRatingController;
use App\Http\Controllers\Front\MyRelationController as FrontMyRelationController;
use App\Http\Controllers\Front\LocalRelationController as FrontLocalRelationController;
//FRONT POLICY
use App\Policies\Controllers\Front\FrontControllerPolicy;


class Navigation
{
    public function navigation()
    {
        $navigation = [
            (new Url())
                ->setUrl('/panel')
                ->setRoute('dashboard')
                ->setAuth(true)
                ->setMiddleware(['web','permission'])
                ->setType('web')
                ->setPermissionClass(DashboardControllerPolicy::class)
                ->setPermissionAction('main')
                ->setChildren([
                    (new Url())
                        ->setController(HomeController::class)
                        ->setUrl('')
                        ->setRoute('home')
                        ->setAction('index')
                        ->setMethod('GET')
                    ,
                    (new Url())
                        ->setController(LoginController::class)
                        ->setUrl('/zaloguj')
                        ->setRoute('login')
                        ->setAuth(false)
                        ->setChildren([
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showLoginForm')
                                ->setAction('showLoginForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zweryfikuj')
                                ->setRoute('verification')
                                ->setAction('login')
                                ->setMethod('POST')
                    ]),
                    (new Url())
                        ->setController(LoginController::class)
                        ->setUrl('/wyloguj')
                        ->setRoute('logout')
                        ->setAction('logout')
                        ->setAuth(false)
                        ->setMethod('GET'),
                    (new Url())
                        ->setController(ForgotPasswordController::class)
                        ->setUrl('/zapomniałem-hasla')
                        ->setRoute('passwordForget')
                        ->setAuth(false)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('email')
                                ->setAction('showLinkRequestForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/send')
                                ->setRoute('send')
                                ->setAction('reset')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setController(ResetPasswordController::class)
                        ->setUrl('/zresetuj-haslo')
                        ->setRoute('passwordReset')
                        ->setAuth(false)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showResetForm')
                                ->setAction('showResetForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('store')
                                ->setAction('reset')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setController(ChangePasswordController::class)
                        ->setUrl('/zmiana-hasla')
                        ->setRoute('passwordChange')
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showChangePasswordForm')
                                ->setAction('showChangePasswordForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('store')
                                ->setAction('store')
                                ->setMethod('POST'),
                            (new Url())
                                ->setUrl('/udana')
                                ->setRoute('success')
                                ->setAction('successStore')
                                ->setMethod('GET')
                        )),
                    (new Url())
                        ->setController(ChangeAccountController::class)
                        ->setUrl('/edytuj-profil')
                        ->setRoute('accountChange')
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('edit')
                                ->setAction('edit')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('update')
                                ->setAction('update')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setController(LocalController::class)
                        ->setUrl('/lokale')
                        ->setRoute('locals')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setBreadcrumb('Lokale')
                                ->setMethod('GET')
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setLabel('Lokale')
                                ->setIcon('fas fa-building')
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('show')
                                ->setMethod('GET')
                                ->setUrl('/{local_id}')
                                ->setRoute('show'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{local_id}')
                                ->setRoute('edit')
                                ->setPermissionClass(LocalControllerPolicy::class)
                                ->setPermissionAction('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{local_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{local_id}')
                                ->setRoute('delete'),
                            (new Url())
                                ->setController(MenuController::class)
                                ->setUrl('/{local_id}/menu')
                                ->setRoute('menu')
                                ->setChildren([
                                    (new Url())
                                        ->setAction('index')
                                        ->setBreadcrumb('Menu')
                                        ->setMethod('GET')
                                        ->setUrl('/')
                                        ->setRoute('index'),
                                    (new Url())
                                        ->setAction('store')
                                        ->setMethod('GET')
                                        ->setUrl('/store')
                                        ->setRoute('store'),
                                    (new Url())
                                        ->setAction('delete')
                                        ->setMethod('DELETE')
                                        ->setUrl('/{menu_id}')
                                        ->setRoute('delete'),
                                    (new Url())
                                        ->setController(MenuItemController::class)
                                        ->setUrl('/{menu_id}/pozycje')
                                        ->setRoute('items')
                                        ->setChildren([
                                            (new Url())
                                                ->setAction('index')
                                                ->setBreadcrumb('Pozycje')
                                                ->setMethod('GET')
                                                ->setUrl('/')
                                                ->setRoute('index'),
                                            (new Url())
                                                ->setAction('create')
                                                ->setMethod('GET')
                                                ->setUrl('/dodaj')
                                                ->setRoute('create'),
                                            (new Url())
                                                ->setAction('store')
                                                ->setMethod('POST')
                                                ->setUrl('/')
                                                ->setRoute('store'),
                                            (new Url())
                                                ->setAction('edit')
                                                ->setMethod('GET')
                                                ->setUrl('/{menu_item_id}')
                                                ->setRoute('edit'),
                                            (new Url())
                                                ->setAction('update')
                                                ->setMethod('POST')
                                                ->setUrl('/{menu_item_id}')
                                                ->setRoute('update'),
                                            (new Url())
                                                ->setAction('delete')
                                                ->setMethod('DELETE')
                                                ->setUrl('/{menu_item_id}')
                                                ->setRoute('delete'),
                                        ]),
                                ]),
                            (new Url())
                                ->setController(LocalVisitCodeController::class)
                                ->setUrl('/{local_id}/codes')
                                ->setRoute('visitCodes')
                                ->setChildren(array(
                                    (new Url())
                                        ->setRoute('index')
                                        ->setUrl('/')
                                        ->setAction('index')
                                        ->setBreadcrumb('Kody wizyt')
                                        ->setMethod('GET'),
                                    (new Url())
                                        ->setRoute('create')
                                        ->setUrl('/store')
                                        ->setAction('store')
                                        ->setMethod('GET'),
                                    (new Url())
                                        ->setRoute('print')
                                        ->setUrl('/drukuj')
                                        ->setMethod('GET')
                                        ->setAction('print'),
                                    (new Url())
                                        ->setRoute('show')
                                        ->setUrl('/{code_id}')
                                        ->setMethod('GET')
                                        ->setAction('show'),
                                )),
                            (new Url())
                                ->setController(LocalRatingController::class)
                                ->setUrl('/{local_id}/opinie')
                                ->setRoute('ratings')
                                ->setChildren(array(
                                    (new Url())
                                        ->setRoute('index')
                                        ->setUrl('/')
                                        ->setAction('index')
                                        ->setMethod('GET'),
                                )),
                            (new Url())
                                ->setController(LocalRelationController::class)
                                ->setUrl('/{local_id}/relacje')
                                ->setRoute('relations')
                                ->setChildren(array(
                                    (new Url())
                                        ->setRoute('index')
                                        ->setUrl('/')
                                        ->setAction('index')
                                        ->setMethod('GET')
                                        ->setBreadcrumb('Relacje'),
                                    (new Url())
                                        ->setRoute('delete')
                                        ->setUrl('/{local_id}')
                                        ->setAction('destroy')
                                        ->setMethod('DELETE'),
                                )),
                            (new Url())
                                ->setController(LocalOpeningHoursController::class)
                                ->setUrl('/{local_id}/godziny-otwarcia')
                                ->setRoute('opening_hours')
                                ->setChildren(array(
                                    (new Url())
                                        ->setRoute('edit')
                                        ->setUrl('/')
                                        ->setAction('edit')
                                        ->setMethod('GET'),
                                    (new Url())
                                        ->setRoute('update')
                                        ->setUrl('/update')
                                        ->setAction('update')
                                        ->setMethod('POST'),
                                )),
                        ]),
                    (new Url())
                        ->setController(LocalTypeController::class)
                        ->setUrl('/typy-lokali')
                        ->setRoute('localsTypes')
                        ->setPermissionClass(LocalTypeControllerPolicy::class)
                        ->setPermissionAction('main')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setLabel('Typy lokali')
                                ->setBreadcrumb('Typy lokali')
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setIcon('fas fa-city')
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{local_type_id}')
                                ->setRoute('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{local_type_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{local_type_id}')
                                ->setRoute('delete'),
                        ]),
                    (new Url())
                        ->setController(MenuItemCategoryController::class)
                        ->setUrl('/menu/kategorie')
                        ->setRoute('menuItemsCategories')
                        ->setPermissionClass(MenuItemCategoryControllerPolicy::class)
                        ->setPermissionAction('main')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setBreadcrumb('Kategorie menu')
                                ->setLabel('Kategorie menu')
                                ->setIcon("fas fa-utensils")
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/create')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{menu_item_category_id}')
                                ->setRoute('delete'),
                    ]),
                    (new Url())
                        ->setController(EventController::class)
                        ->setUrl('/wydarzenia')
                        ->setRoute('events')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setBreadcrumb('Wydarzenia')
                                ->setLabel('Wydarzenia')
                                ->setIcon("fas fa-newspaper")
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{event_id}')
                                ->setRoute('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{event_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{event_id}')
                                ->setRoute('delete'),
                        ]),
                    (new Url())
                        ->setController(EventPeriodicController::class)
                        ->setUrl('/wydarzenia-cykliczne')
                        ->setRoute('eventsPeriodic')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setUrl('/')
                                ->setBreadcrumb('Wydarzenia cykliczne')
                                ->setRoute('index'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{event_periodic_id}')
                                ->setRoute('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{event_periodic_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{event_periodic_id}')
                                ->setRoute('delete'),
                        ]),
                    (new Url())
                        ->setController(EventTypeController::class)
                        ->setUrl('/typy-wydarzeń')
                        ->setRoute('eventsTypes')
                        ->setPermissionClass(EventTypeControllerPolicy::class)
                        ->setPermissionAction('main')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setLabel('Typy wydarzeń')
                                ->setIcon("fas fa-shapes")
                                ->setUrl('/')
                                ->setBreadcrumb('Typy wydarzeń')
                                ->setRoute('index')
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{event_type_id}')
                                ->setRoute('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{event_type_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{event_type_id}')
                                ->setRoute('delete'),
                        ]),
                    (new Url())
                        ->setController(UserController::class)
                        ->setUrl('/uzytkownicy')
                        ->setRoute('users')
                        ->setPermissionClass(UserControllerPolicy::class)
                        ->setPermissionAction('main')
                        ->setChildren([
                            (new Url())
                                ->setAction('index')
                                ->setMethod('GET')
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setBreadcrumb('Użytkownicy')
                                ->setLabel('Użytkownicy')
                                ->setIcon('fas fa-users')
                                ->setPositionInMenu('dashboard-main'),
                            (new Url())
                                ->setAction('create')
                                ->setMethod('GET')
                                ->setUrl('/dodaj')
                                ->setRoute('create'),
                            (new Url())
                                ->setAction('store')
                                ->setMethod('POST')
                                ->setUrl('/')
                                ->setRoute('store'),
                            (new Url())
                                ->setAction('edit')
                                ->setMethod('GET')
                                ->setUrl('/{user_id}')
                                ->setRoute('edit'),
                            (new Url())
                                ->setAction('update')
                                ->setMethod('POST')
                                ->setUrl('/{user_id}')
                                ->setRoute('update'),
                            (new Url())
                                ->setAction('delete')
                                ->setMethod('DELETE')
                                ->setUrl('/{user_id}')
                                ->setRoute('delete'),
                        ]),
                ]),
            (new Url())
                ->setUrl('')
                ->setRoute('front')
                ->setMiddleware(['web','permission'])
                ->setType('web')
                ->setPermissionClass(FrontControllerPolicy::class)
                ->setPermissionAction('main')
                ->setChildren(array(
                    (new Url())
                        ->setUrl('/')
                        ->setRoute('home')
                        ->setController(FrontHomeController::class)
                        ->setAction('index')
                        ->setMethod('GET'),
                    (new Url())
                        ->setController(FrontRegisterController::class)
                        ->setUrl('/rejestracja')
                        ->setRoute('registration')
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showRegistrationForm')
                                ->setAction('showRegistrationForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('register')
                                ->setAction('register')
                                ->setMethod('POST')
                        )),
                    (new Url())
                        ->setController(FrontLoginController::class)
                        ->setUrl('/zaloguj')
                        ->setRoute('login')
                        ->setAuth(false)
                        ->setChildren([
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showLoginForm')
                                ->setAction('showLoginForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zweryfikuj')
                                ->setRoute('verification')
                                ->setAction('login')
                                ->setMethod('POST')
                        ]),
                    (new Url())
                        ->setController(FrontLoginController::class)
                        ->setUrl('/wyloguj')
                        ->setRoute('logout')
                        ->setAction('logout')
                        ->setAuth(false)
                        ->setMethod('GET'),
                    (new Url())
                        ->setController(FrontForgotPasswordController::class)
                        ->setUrl('/zapomniałem-hasla')
                        ->setRoute('passwordForget')
                        ->setAuth(false)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('email')
                                ->setAction('showLinkRequestForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/send')
                                ->setRoute('send')
                                ->setAction('reset')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setController(FrontResetPasswordController::class)
                        ->setUrl('/zresetuj-haslo')
                        ->setRoute('passwordReset')
                        ->setAuth(false)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showResetForm')
                                ->setAction('showResetForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('store')
                                ->setAction('reset')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setController(FrontChangePasswordController::class)
                        ->setUrl('/zmiana-hasla')
                        ->setRoute('passwordChange')
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showChangePasswordForm')
                                ->setAction('showChangePasswordForm')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('store')
                                ->setAction('store')
                                ->setMethod('POST'),
                            (new Url())
                                ->setUrl('/udana')
                                ->setRoute('success')
                                ->setAction('successStore')
                                ->setMethod('GET')
                        )),
                    (new Url())
                        ->setController(FrontChangeAccountController::class)
                        ->setUrl('/edytuj-profil')
                        ->setRoute('accountChange')
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('edit')
                                ->setAction('edit')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('update')
                                ->setAction('update')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setUrl('/lokale')
                        ->setRoute('locals')
                        ->setController(FrontLocalController::class)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setMethod('GET')
                                ->setAction('index')
                                ->setLabel('Lokale')
                                ->setPositionInMenu('front-main'),
                            (new Url())
                                ->setUrl('/{local_id}')
                                ->setRoute('show')
                                ->setMethod('GET')
                                ->setAction('show'),
                            (new Url())
                                ->setUrl('/{local_id}/opinie')
                                ->setRoute('ratings')
                                ->setController(FrontLocalRatingController::class)
                                ->setChildren(array(
                                    (new Url())
                                        ->setUrl('/')
                                        ->setRoute('index')
                                        ->setAction('index')
                                        ->setMethod('GET'),
                                )),
                        )),
                    (new Url())
                        ->setController(FrontLocalVisitCodeController::class)
                        ->setUrl('/kod-wizyty')
                        ->setRoute('visit_codes')
                        ->setAuth(true)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('showFormVisitCode')
                                ->setAction('showFormVisitCode')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/dostepne-opcje')
                                ->setRoute('choiceForm')
                                ->setAction('choiceForm')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setUrl('/opinia')
                        ->setRoute('ratings')
                        ->setController(FrontLocalRatingController::class)
                        ->setAuth(true)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/dodaj')
                                ->setRoute('create')
                                ->setAction('create')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz/')
                                ->setRoute('store')
                                ->setAction('store')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setUrl('/relacja')
                        ->setRoute('relations')
                        ->setController(FrontLocalRelationController::class)
                        ->setAuth(true)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/dodaj')
                                ->setRoute('create')
                                ->setAction('create')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/zapisz')
                                ->setRoute('store')
                                ->setAction('store')
                                ->setMethod('POST'),
                            (new Url())
                                ->setUrl('/edytuj')
                                ->setRoute('edit')
                                ->setAction('edit')
                                ->setMethod('GET'),
                            (new Url())
                                ->setUrl('/uaktualnij')
                                ->setRoute('update')
                                ->setAction('update')
                                ->setMethod('POST'),
                        )),
                    (new Url())
                        ->setUrl('/wydarzenia')
                        ->setRoute('events')
                        ->setController(FrontEventController::class)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setMethod('GET')
                                ->setAction('index')
                                ->setLabel('Wydarzenia')
                                ->setPositionInMenu('front-main'),
                            (new Url())
                                ->setUrl('/{event_id}')
                                ->setRoute('show')
                                ->setMethod('GET')
                                ->setAction('show'),
                        )),
                    (new Url())
                        ->setUrl('/moje-opinie')
                        ->setRoute('my_ratings')
                        ->setController(FrontMyRatingController::class)
                        ->setAuth(true)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setMethod('GET')
                                ->setAction('index')
                                ->setLabel('Moje opinie')
                                ->setPositionInMenu('front-main'),
                        )),
                    (new Url())
                        ->setUrl('/moje-relacje')
                        ->setRoute('my_relations')
                        ->setAuth(true)
                        ->setController(FrontMyRelationController::class)
                        ->setChildren(array(
                            (new Url())
                                ->setUrl('/')
                                ->setRoute('index')
                                ->setMethod('GET')
                                ->setAction('index')
                                ->setLabel('Moje relacje')
                                ->setPositionInMenu('front-main'),
                        )),
                )),
            (new Url())
                ->setUrl('/add-ons')
                ->setRoute('addons')
                ->setChildren(array(
                    (new Url())
                        ->setRoute('images')
                        ->setUrl('/images')
                        ->setController(ImageController::class)
                        ->setChildren(array(
                            (new Url())
                                ->setAction('create')
                                ->setRoute('create')
                                ->setUrl('/create')
                                ->setMethod('POST'),
                        )),
                ))
        ];

        return $navigation;
    }
}