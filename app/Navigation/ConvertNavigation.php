<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 03.10.2019
 * Time: 22:03
 */

namespace App\Navigation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class ConvertNavigation
{
    private static $instance = null;

    public $routes = [];
    public $navigations = [];
    public $menu = [];
    public $breadcrumb = [];

    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new ConvertNavigation();
        }
        return self::$instance;
    }

    public function convert()
    {
        $navigation = app(Navigation::class)->navigation();

        $this->con($navigation);

        return $this->routes;
    }

    protected function con($navigation, Url $inherit = null)
    {
        /** @var Url $row */
        foreach ($navigation as $row) {
            $new = $row;

            $new->setUrl(($inherit && $inherit->getUrl() ? $inherit->getUrl() : '') . $new->getUrl());
            $new->setRoute(($inherit && $inherit->getRoute() ? $inherit->getRoute() . '.' : '') . $new->getRoute());
            $new->setController(($inherit && !$new->getController() ? $inherit->getController() : $new->getController()));

            $new->setPermissionClass(($inherit && !$new->getPermissionClass() ? $inherit->getPermissionClass() : $new->getPermissionClass()));
            $new->setPermissionAction(($inherit && !$new->getPermissionAction() ? $inherit->getPermissionAction() : $new->getPermissionAction()));

            $new->setAuth($inherit ? $this->isAuth($inherit,$new) : (boolean)$new->getAuth());
            $new->setMiddleware($inherit ? $inherit->getMiddleware() : $new->getMiddleware());
            $new->setType($inherit ? $inherit->getType() : $new->getType());

            $middleware = count($new->getMiddleware()) ? $new->getMiddleware() : ['web'];

            if($new->getAuth())
                $middleware[] = 'auth:'.$new->getType();


            if ($row->getChildren())
                $this->con($row->getChildren(), $new);


            if($new->getMethod())
            {
                $this->navigations[] = $new;
                $this->routes[] = Route::{$new->getMethod()}($new->getUrl(), ['uses' => $new->getController() . '@' . $new->getAction(), 'middleware' => $middleware])->name($new->getRoute());
            }
        }
    }

    public function menu(string $menuName)
    {
        $this->me(app(Navigation::class)->navigation(), $menuName);

        return $this->menu;
    }

    public function me($navigation, string $menuName, $inherit = null)
    {
        foreach ($navigation as $row)
        {
            $new = $row;
            $new->setUrl(($inherit && $inherit->getUrl() ? $inherit->getUrl() : '') . $new->getUrl());
            $new->setRoute(($inherit && $inherit->getRoute() ? $inherit->getRoute() . '.' : '') . $new->getRoute());
            $new->setController(($inherit && !$new->getController() ? $inherit->getController() : $new->getController()));
            $new->setAuth($inherit ? $this->isAuth($inherit,$new) : (boolean)$new->getAuth());

            $new->setPermissionClass(($inherit && !$new->getPermissionClass() ? $inherit->getPermissionClass() : $new->getPermissionClass()));
            $new->setPermissionAction(($inherit && !$new->getPermissionAction() ? $inherit->getPermissionAction() : $new->getPermissionAction()));

            if($new->getPositionInMenu() == $menuName)
            {
                $this->menu[] = $new;
                continue;
            }

            if($new->getChildren())
                $this->me($new->getChildren(), $menuName, $new);

        }
    }

    protected function isAuth(Url $inherit, Url $new):bool
    {
        if(!is_null($new->getAuth()))
            return $new->getAuth();

        return (boolean)$inherit->getAuth();
    }

    public function breadcrumb()
    {
        $routes = Route::getRoutes();
        $route = $routes->match(request());

        $this->bread(app(Navigation::class)->navigation(), '.'.$route->action['as'], $route->parameters);

        return $this->breadcrumb;
    }

    protected function bread(array $navigation, $uri, array $params = [] ,Url $inherit = null)
    {
        $uri = substr($uri,1);

        $uriExp = explode('.',$uri);

        for($k = 0; $k < count($navigation); $k++)
        {
            $str = $uriExp[0];

            if($navigation[$k]->getRoute() != $str && !$navigation[$k]->getBreadcrumb())
                continue;

            $new = $navigation[$k];

            $new->setUrl(($inherit && $inherit->getUrl() ? $inherit->getUrl() : '') . $new->getUrl());
            $new->setRoute(($inherit && $inherit->getRoute() ? $inherit->getRoute() . '.' : '') . $new->getRoute());
            $new->setController(($inherit && !$new->getController() ? $inherit->getController() : $new->getController()));
            $new->setAuth($inherit ? $this->isAuth($inherit, $new) : (boolean)$new->getAuth());
            $new->setMiddleware($inherit ? $inherit->getMiddleware() : $new->getMiddleware());
            $new->setType($inherit ? $inherit->getType() : $new->getType());

            $strUri = substr($uri,strlen($str));

            if(!$navigation[$k]->getAction())
                $this->bread($navigation[$k]->getChildren(),$strUri, $params ,$new);


            if($navigation[$k]->getAction() && $navigation[$k]->getBreadcrumb())
            {
                $parameters = [];

                foreach ($params as $key => $value)
                    if(preg_match('/\{'.$key.'\}/',$navigation[$k]->getUrl()))
                        $parameters[$key] = $value;


                $this->breadcrumb[] = [
                    'urlObj' => $navigation[$k],
                    'routeParams' => $parameters
                ];
            }


        }

        return $this->breadcrumb;
    }

    public function checkRoutePermission(Request $request)
    {
        dd($this->routes);
    }
}