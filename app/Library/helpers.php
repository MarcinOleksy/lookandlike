<?php

use Carbon\Carbon;

if (! function_exists('parseCarbonDateToFormat')) {

    function parseCarbonDateToFormat(?Carbon $data = null, string $format = "Y-m-d H-i"):?string
    {
        if(!$data)
            return null;

        return Carbon::parse($data)->format($format);
    }
}