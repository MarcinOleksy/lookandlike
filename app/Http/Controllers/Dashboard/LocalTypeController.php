<?php

namespace App\Http\Controllers\Dashboard;

use App\Forms\Dashboard\LocalTypeCreateForm;
use App\Repositories\LocalTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalTypeController extends Controller
{
    use FormBuilderTrait;

    protected $localTypeRepository;

    public function __construct(
        LocalTypeRepository $localTypeRepository
    )
    {
        $this->localTypeRepository = $localTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locals_types = $this->localTypeRepository->paginate();

        return view('dashboard.locals_types.index', compact('locals_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $local_type = $this->localTypeRepository->findOrNew();

        $form = $this->form(LocalTypeCreateForm::class, [
            'model' => $local_type,
            'method' => 'POST',
            'url' => route('dashboard.localsTypes.store')
        ]);

        return view('dashboard.locals_types.create', compact('local_type','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $local_type = $this->localTypeRepository->findOrNew();

        $form = $this->form(LocalTypeCreateForm::class, [
            'model' => $local_type,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals.store.error'));

        $this->localTypeRepository->updateByData($local_type,$form->getFieldValues());

        $this->localTypeRepository->store($local_type);

        return redirect()->route('dashboard.localsTypes.index')->with('success', trans('messages.locals.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $local_type = $this->localTypeRepository->findOrFail($id);

        return view('dashboard.locals_types.show', compact('local_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $local_type = $this->localTypeRepository->findOrFail($id);

        $form = $this->form(LocalTypeCreateForm::class, [
            'model' => $local_type,
            'method' => 'POST',
            'url' => route('dashboard.localsTypes.edit', ['id' => $local_type->id])
        ]);

        return view('dashboard.locals_types.edit', compact('local_type', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $local_type = $this->localTypeRepository->findOrFail($id);

        $form = $this->form(LocalTypeCreateForm::class, [
            'model' => $local_type,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals.store.error'));

        $this->localTypeRepository->updateByData($local_type,$form->getFieldValues());

        $this->localTypeRepository->store($local_type);

        return redirect()->route('dashboard.localsTypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $local_type = $this->localTypeRepository->findOrFail($id);

        return redirect()->route('dashboard.localsTypes.index');
    }
}
