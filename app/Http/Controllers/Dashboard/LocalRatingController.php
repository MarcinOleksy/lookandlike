<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:32
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\RatingIndexSearchForm;
use App\Http\Controllers\Controller;
use App\Repositories\Filters\Dashboard\RatingIndexFilter;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalRatingController extends Controller
{
    use FormBuilderTrait;

    protected $localRatingRepository;
    protected $localVisitCodeRepository;
    protected $localRepository;

    public function __construct(
        LocalRatingRepository $localRatingRepository,
        LocalVisitCodeRepository $localVisitCodeRepository,
        LocalRepository $localRepository
    )
    {
        $this->localVisitCodeRepository = $localVisitCodeRepository;
        $this->localRatingRepository = $localRatingRepository;
        $this->localRepository = $localRepository;
    }

    public function index(RatingIndexFilter $filter, int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $ratings = $this->localRatingRepository->paginateForLocal($local, $filter);

        $formSearch = $this->form(RatingIndexSearchForm::class, [
            'model' => $filter->getFilterValues()
        ]);

        return view('dashboard.locals_ratings.index',compact('local','ratings', 'formSearch'));
    }
}