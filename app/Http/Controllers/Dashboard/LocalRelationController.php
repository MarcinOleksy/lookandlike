<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 27.04.2020
 * Time: 00:04
 */

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\LocalRelationRepository;
use App\Repositories\LocalRepository;

class LocalRelationController extends Controller
{
    protected $localRelationRepository;
    protected $localRepository;

    public function __construct(
        LocalRelationRepository $localRelationRepository,
        LocalRepository $localRepository
    )
    {
        $this->localRelationRepository = $localRelationRepository;
        $this->localRepository = $localRepository;
    }

    public function index(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $relations = $this->localRelationRepository->paginateByLocal($local);

        return view('dashboard.locals_relations.index', compact('local', 'relations'));
    }

    public function delete(int $local_id, int $relation_id)
    {
        $relation = $this->localRelationRepository->findOrFail($relation_id);

        return view('dashboard.locals_relations.index');
    }
}