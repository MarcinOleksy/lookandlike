<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.12.2019
 * Time: 21:05
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\LocalOpeningHoursForm;
use App\Http\Controllers\Controller;
use App\Repositories\LocalOpeningHoursRepository;
use App\Repositories\LocalRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalOpeningHoursController extends Controller
{
    use FormBuilderTrait;

    protected $localOpeningHoursRepository;
    protected $localRepository;

    public function __construct(
        LocalOpeningHoursRepository $localOpeningHoursRepository,
        LocalRepository $localRepository
    )
    {
        $this->localOpeningHoursRepository = $localOpeningHoursRepository;
        $this->localRepository = $localRepository;
    }

    public function edit(int $id)
    {
        $model = $this->localRepository->findOrFail($id);
        $opening_hours = $this->localOpeningHoursRepository->findOrNewByLocalId($id);

        $form = $this->form(LocalOpeningHoursForm::class, [
            'model' => $opening_hours,
            'url' => route('dashboard.locals.opening_hours.update', ['local_id' => $id]),
            'method' => 'POST'
        ]);

        return view('dashboard.locals_opening_hours.edit', compact('form','$opening_hours', 'model'));
    }

    public function update(int $id)
    {
        $model = $this->localRepository->findOrFail($id);
        $opening_hours = $this->localOpeningHoursRepository->findOrNewByLocalId($id);

        $form = $this->form(LocalOpeningHoursForm::class, [
            'model' => $opening_hours
        ]);

        if(!$form->isValid())
            return redirect()->back()->withErrors($form->getErrors())->withInput()->with('error', trans('messages.locals_opening_close.store.error'));;

        $this->localRepository->updateByData($opening_hours,$form->getFieldValues());

        $this->localOpeningHoursRepository->store($opening_hours);

        return redirect()->route('dashboard.locals.show',['local_id' => $id])->with('success', trans('messages.locals_opening_close.store.success'));
    }
}