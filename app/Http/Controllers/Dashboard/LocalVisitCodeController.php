<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 17.11.2019
 * Time: 15:30
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\LocalVisitCodeIndexSearchForm;
use App\Http\Controllers\Controller;
use App\Repositories\Filters\Dashboard\LocalVisitCodeIndexFilter;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use App\Services\QrCodeFileGenerate;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use PDF;
use Carbon\Carbon;

class LocalVisitCodeController extends Controller
{
    use FormBuilderTrait;

    protected $localVisitCodeRepository;
    protected $localRepository;
    protected $qrCodeFileGenerate;

    public function __construct(
        LocalVisitCodeRepository $localVisitCodeRepository,
        LocalRepository $localRepository,
        QrCodeFileGenerate $qrCodeFileGenerate
    )
    {
        $this->localVisitCodeRepository = $localVisitCodeRepository;
        $this->localRepository = $localRepository;
        $this->qrCodeFileGenerate = $qrCodeFileGenerate;
    }

    public function index(LocalVisitCodeIndexFilter $filter, int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $codes = $this->localVisitCodeRepository->paginateForLocal($local, $filter);

        $formSearch = $this->form(LocalVisitCodeIndexSearchForm::class, [
            'model' => $filter->getFilterValues()
        ]);

        return view('dashboard.locals_visit_codes.index',compact('local', 'codes', 'formSearch'));
    }

    public function store(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);

        $code =  $local->id.'_'.time();

        $data = [
            'local_id' => $local->id,
            'code' => $code,
            'path' => $this->qrCodeFileGenerate->generateFile($code),
        ];

        $code = $this->localVisitCodeRepository->create($data);

        return redirect()->route('dashboard.locals.visitCodes.show',['local_id' => $local->id, 'code_id' => $code->id])->with('success', trans('messages.locals_visit_codes.create.success'));
    }

    public function show(int $local_id, int $code_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $code = $this->localVisitCodeRepository->findOrFail($code_id);

        return view('dashboard.locals_visit_codes.show', compact('local','code'));
    }

    public function print(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $visitCodes = $this->localVisitCodeRepository->getNotPrintedForLocal($local);

        $data = [
            'visitCodes' => $visitCodes
        ];

        $currentDate = Carbon::now()->format('Y-m-d H:i:s');
        $currentDate = preg_replace('/\s|\-|\:|\./','_',$currentDate);

        foreach ($visitCodes as $visitCode)
        {
            $visitCode->printed_at = $currentDate;
            $this->localVisitCodeRepository->store($visitCode);
        }

        $pdf = PDF::loadView('dashboard/locals_visit_codes/pdf', $data);

        return $pdf->download(str_replace('_',' ',$local->name).'_'.$currentDate.'.pdf');
    }
}