<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 15:49
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\EventPeriodicCreateForm;
use App\Http\Controllers\Controller;
use App\Repositories\EventPeriodicRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventPeriodicController extends Controller
{
    use FormBuilderTrait;

    protected $eventPeriodicRepository;

    public function __construct(
        EventPeriodicRepository $eventPeriodicRepository
    )
    {
        $this->eventPeriodicRepository = $eventPeriodicRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = $this->eventPeriodicRepository->paginate();

        return view('dashboard.events_periodic.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = $this->eventPeriodicRepository->findOrNew();

        $form = $this->form(EventPeriodicCreateForm::class, [
            'model' => $event,
            'method' => 'POST',
            'url' => route('dashboard.eventsPeriodic.store')
        ]);

        return view('dashboard.events_periodic.create', compact('event','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $event = $this->eventPeriodicRepository->findOrNew();

        $form = $this->form(EventPeriodicCreateForm::class, [
            'model' => $event,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events.store.error'));

        $this->eventPeriodicRepository->updateByData($event,$form->getFieldValues());

        $this->eventPeriodicRepository->store($event);

        return redirect()->route('dashboard.eventsPeriodic.index')->with('success', trans('messages.events.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $event = $this->eventPeriodicRepository->findOrFail($id);

        return view('dashboard.events_periodic.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $event = $this->eventPeriodicRepository->findOrFail($id);

        $form = $this->form(EventPeriodicCreateForm::class, [
            'model' => $event,
            'method' => 'POST',
            'url' => route('dashboard.eventsPeriodic.edit', ['id' => $event->id])
        ]);

        return view('dashboard.events_periodic.edit', compact('event', 'form'));
    }

    /**
     * Update the specified resource in storage.

     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(int $id)
    {
        $event = $this->eventPeriodicRepository->findOrFail($id);

        $form = $this->form(EventPeriodicCreateForm::class, [
            'model' => $event,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events.store.error'));

        $this->eventPeriodicRepository->updateByData($event,$form->getFieldValues());

        $this->eventPeriodicRepository->store($event);

        return redirect()->route('dashboard.eventsPeriodic.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $event = $this->eventPeriodicRepository->findOrFail($id);

        return redirect()->route('dashboard.eventsPeriodic.index');
    }
}
