<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 20.11.2019
 * Time: 20:08
 */

namespace App\Http\Controllers\Dashboard\Auth;


use App\Forms\Dashboard\Auth\PasswordChangeForm;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ChangePasswordController extends Controller
{
    use FormBuilderTrait;

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function showChangePasswordForm()
    {
        $user = Auth::user();

        $form = $this->form(PasswordChangeForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.passwordChange.store')
        ]);

        return view('dashboard.auth.passwords.change_form', compact('form', 'user'));
    }

    public function store()
    {
        $user = Auth::user();

        $form = $this->form(PasswordChangeForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.passwordChange.store')
        ]);

        if(!$form->isValid())
            return redirect()->back()->withErrors($form->getErrors())->withInput();

        $user->password = Hash::make($form->getFieldValues()['password']);

        $this->userRepository->store($user);

        return redirect()->route('dashboard.passwordChange.success');
    }

    public function successStore()
    {
        return view('dashboard.auth.passwords.change_success');
    }
}