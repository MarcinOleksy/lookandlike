<?php

namespace App\Http\Controllers\Dashboard;

use App\czas;
use App\Forms\Dashboard\UserCreateForm;
use App\Forms\Dashboard\UserEditForm;
use App\Forms\Dashboard\UserIndexSearchForm;
use App\Repositories\Filters\Dashboard\UserIndexFilter;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class UserController extends Controller
{
    use FormBuilderTrait;

    protected $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserIndexFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function index(UserIndexFilter $filter)
    {
        $users = $this->userRepository->paginate($filter);

        $formSearch = $this->form(UserIndexSearchForm::class, [
           'model' => $filter->getFilterValues()
        ]);

        return view('dashboard.users.index', compact('users', 'formSearch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->userRepository->findOrNew();

        $form = $this->form(UserCreateForm::class, [
            'model' => $user,
            'method' => 'POST',
            'url' => route('dashboard.users.store')
        ]);

        return view('dashboard.users.create', compact('user','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->userRepository->findOrNew();

        $form = $this->form(UserCreateForm::class, [
            'model' => $user,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.users.store.error'));

        $data = $form->getFieldValues();

        $this->userRepository->updateByData($user,$data);

        $this->userRepository->store($user);

        $user->assignGroup([ $data['acl_group'] ]);

        return redirect()->route('dashboard.users.index')->with('success', trans('messages.users.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findOrFail($id);

        return view('dashboard.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findOrFail($id);

        $form = $this->form(UserEditForm::class, [
            'model' => $user,
            'method' => 'POST',
            'url' => route('dashboard.users.edit', ['id' => $user->id])
        ],['user' => $user]);

        return view('dashboard.users.edit', compact('user', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->userRepository->findOrFail($id);

        $form = $this->form(UserEditForm::class, [
            'model' => $user,
        ],['user' => $user]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.users.store.error'));

        $this->userRepository->updateByData($user,$form->getFieldValues());

        $this->userRepository->store($user);

        return redirect()->route('dashboard.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findOrFail($id);

        return redirect()->route('dashboard.$users.index');
    }


    public function asd()
    {
        if(!isset($_POST['login']) || !isset($_POST['password']))
        {
            echo "Nie podałeś loginu bądź hasła";
            die();
        }

        $login = $_POST['login'];
        $password = $_POST['password'];

        if(empty($login) || empty($password))
        {
            echo "Musisz podać login i hasło";
            die();
        }

        if($login != "damian" || $password != "abc")
        {

            echo "dane logowania nie prawidłowe";
            return false;
        }

        echo "udalo sie zalogować";
        return true;
    }
}























































