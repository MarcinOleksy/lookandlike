<?php

namespace App\Http\Controllers\Dashboard;

use App\Forms\Dashboard\LocalCreateForm;
use App\Forms\Dashboard\LocalIndexSearchForm;
use App\Models\AclUserGroup;
use App\Repositories\Filters\Dashboard\LocalIndexFilter;
use App\Repositories\LocalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalController extends Controller
{
    use FormBuilderTrait;

    protected $localRepository;

    public function __construct(
        LocalRepository $localRepository
    )
    {
        $this->localRepository = $localRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param LocalIndexFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function index(LocalIndexFilter $filter)
    {
        if(Auth::user()->acl_group->slug == AclUserGroup::ADMIN_SLUG)
            $locals = $this->localRepository->paginate($filter);
        else
            $locals = $this->localRepository->paginateForOwner(Auth::user(), $filter);

        $formSearch = $this->form(LocalIndexSearchForm::class,[
            'model' => $filter->getFilterValues()
        ]);

        return view('dashboard.locals.index', compact('locals', 'formSearch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $local = $this->localRepository->findOrNew();

        $form = $this->form(LocalCreateForm::class, [
            'model' => $local,
            'method' => 'POST',
            'url' => route('dashboard.locals.store')
        ]);

        return view('dashboard.locals.create', compact('local','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $local = $this->localRepository->findOrNew();

        $form = $this->form(LocalCreateForm::class, [
            'model' => $local,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals.store.error'));

        $this->localRepository->updateByData($local,$form->getFieldValues());

        $this->localRepository->store($local);

        if(Auth::user()->acl_group->slug == AclUserGroup::ADMIN_SLUG)
            $local->local_owners()->attach($form->getFieldValues()['owners_id']);
        else
            $local->local_owners()->attach(Auth::user()->id);

        return redirect()->route('dashboard.locals.index')->with('success', trans('messages.locals.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $local = $this->localRepository->findOrFail($id);

        return view('dashboard.locals.show', compact('local'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $local = $this->localRepository->findOrFail($id);

        $form = $this->form(LocalCreateForm::class, [
            'model' => $local,
            'method' => 'POST',
            'url' => route('dashboard.locals.edit', ['id' => $local->id])
        ]);

        return view('dashboard.locals.edit', compact('local', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $local = $this->localRepository->findOrFail($id);

        $form = $this->form(LocalCreateForm::class, [
            'model' => $local,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals.store.error'));

        $formData = $form->getFieldValues();

        $this->localRepository->updateByData($local,$formData);

        $this->localRepository->store($local);

        if(Auth::user()->acl_group->slug == AclUserGroup::ADMIN_SLUG)
            $local->local_owners()->sync($formData['owners_id']);

        return redirect()->route('dashboard.locals.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $local = $this->localRepository->findOrFail($id);

        return redirect()->route('dashboard.locals.index');
    }
}
