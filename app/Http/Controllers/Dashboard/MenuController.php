<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 17:11
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;
use App\Repositories\LocalRepository;
use App\Repositories\MenuRepository;

class MenuController extends Controller
{
    protected $menuRepository;
    protected $localRepository;
    protected $eventRepository;

    public function __construct(
        MenuRepository $menuRepository,
        LocalRepository $localRepository,
        EventRepository $eventRepository
    )
    {
        $this->menuRepository = $menuRepository;
        $this->localRepository = $localRepository;
        $this->eventRepository = $eventRepository;
    }

    public function index(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);

        $menu = $this->menuRepository->paginateForLocal($local);

        return view('dashboard.menu.index' , compact('menu','local'));
    }

    public function store(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);

        $data = [
            'local_id' => $local_id,
        ];

        $menu = $this->menuRepository->create($data);

        return redirect()->route('dashboard.locals.menu.index', ['local_id' => $local->id])->with('success', trans('messages.menu.store.success'));
    }

    public function delete(int $local_id, int $id)
    {
        $local = $this->localRepository->findOrFail($local_id);

        $menu = $this->menuRepository->findOrFail($id);

        $this->menuRepository->delete($menu);

        return redirect()->route('dashboard.locals.menu.index')->with('success', trans('messages.menu.delete.success'));
    }

}