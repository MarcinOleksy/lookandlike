<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 26.03.2020
 * Time: 20:36
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('dashboard.home.index');
    }
}