<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 17:23
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\MenuItemCategoryCreateForm;
use App\Http\Controllers\Controller;
use App\Repositories\MenuItemCategoryRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MenuItemCategoryController extends Controller
{
    use FormBuilderTrait;

    protected $menuItemCategoryRepository;

    public function __construct(
        MenuItemCategoryRepository $menuItemCategoryRepository
    )
    {
        $this->menuItemCategoryRepository = $menuItemCategoryRepository;
    }

    public function index()
    {
        $categories = $this->menuItemCategoryRepository->paginate();

        return view('dashboard.menu_item_categories.index' ,compact('categories'));
    }

    public function create()
    {
        $form = $this->form(MenuItemCategoryCreateForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.menuItemsCategories.store')
        ]);

        return view('dashboard.menu_item_categories.create', compact('form') );
    }

    public function store()
    {
        $category = $this->menuItemCategoryRepository->findOrNew();

        $form = $this->form(MenuItemCategoryCreateForm::class, [
            'model' => $category
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.menu.items.categories.store.error'));

        $this->menuItemCategoryRepository->updateByData($category,$form->getFieldValues());

        $this->menuItemCategoryRepository->store($category);

        return redirect()->route('dashboard.menuItemsCategories.index')->with('success', trans('messages.events.store.success'));
    }

    public function delete(int $id)
    {
        $category = $this->menuItemCategoryRepository->findOrFail($id);

        $this->menuItemCategoryRepository->delete($category);

        return redirect()->route('dashboard.menuItemsCategories.index')->with('success', trans('messages.events.delete.success'));
    }

}