<?php

namespace App\Http\Controllers\Dashboard;

use App\Forms\Dashboard\EventCreateForm;
use App\Forms\Dashboard\EventIndexSearchForm;
use App\Repositories\EventRepository;
use App\Repositories\Filters\Dashboard\EventIndexFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventController extends Controller
{
    use FormBuilderTrait;

    protected $eventRepository;

    public function __construct(
        EventRepository $eventRepository
    )
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param EventIndexFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function index(EventIndexFilter $filter)
    {
        $events = $this->eventRepository->paginate($filter);

        $formSearch = $this->form(EventIndexSearchForm::class, [
            'model' => $filter->getFilterValues()
        ]);

        return view('dashboard.events.index', compact('events', 'formSearch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = $this->eventRepository->findOrNew();

        $form = $this->form(EventCreateForm::class, [
            'model' => $event,
            'method' => 'POST',
            'url' => route('dashboard.events.store')
        ]);

        return view('dashboard.events.create', compact('event','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $this->eventRepository->findOrNew();

        $form = $this->form(EventCreateForm::class, [
            'model' => $event,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events.store.error'));

        $this->eventRepository->updateByData($event,$form->getFieldValues());

        $this->eventRepository->store($event);

        return redirect()->route('dashboard.events.index')->with('success', trans('messages.events.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $event = $this->eventRepository->findOrFail($id);

        return view('dashboard.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = $this->eventRepository->findOrFail($id);

        $form = $this->form(EventCreateForm::class, [
            'model' => $event,
            'method' => 'PUT',
            'url' => route('dashboard.events.edit', ['id' => $event->id])
        ]);

        return view('dashboard.events.edit', compact('event', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = $this->eventRepository->findOrFail($id);

        $form = $this->form(EventCreateForm::class, [
            'model' => $event,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events.store.error'));

        $this->eventRepository->updateByData($event,$form->getFieldValues());

        $this->eventRepository->store($event);

        return redirect()->route('dashboard.events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = $this->eventRepository->findOrFail($id);

        return redirect()->route('dashboard.events.index');
    }
}
