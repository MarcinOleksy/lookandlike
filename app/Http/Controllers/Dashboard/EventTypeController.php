<?php

namespace App\Http\Controllers\Dashboard;

use App\Forms\Dashboard\EventTypeCreateForm;
use App\Repositories\EventTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventTypeController extends Controller
{
    use FormBuilderTrait;

    protected $eventTypeRepository;

    public function __construct(
        EventTypeRepository $eventTypeRepository
    )
    {
        $this->eventTypeRepository = $eventTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events_types = $this->eventTypeRepository->paginate();

        return view('dashboard.events_types.index', compact('events_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventType = $this->eventTypeRepository->findOrNew();

        $form = $this->form(EventTypeCreateForm::class, [
            'model' => $eventType,
            'method' => 'POST',
            'url' => route('dashboard.eventsTypes.store')
        ]);

        return view('dashboard.events_types.create', compact('eventType','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventType = $this->eventTypeRepository->findOrNew();

        $form = $this->form(EventTypeCreateForm::class, [
            'model' => $eventType,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events_types.store.error'));

        $this->eventTypeRepository->updateByData($eventType,$form->getFieldValues());

        $this->eventTypeRepository->store($eventType);

        return redirect()->route('dashboard.eventsTypes.index')->with('success', trans('messages.events_types.store.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $eventType = $this->eventTypeRepository->findOrFail($id);

        return view('dashboard.events_types.show', compact('eventType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventType = $this->eventTypeRepository->findOrFail($id);

        $form = $this->form(EventTypeCreateForm::class, [
            'model' => $eventType,
            'method' => 'POST',
            'url' => route('dashboard.eventsTypes.edit', ['id' => $eventType->id])
        ]);

        return view('dashboard.events_types.edit', compact('eventType', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventType = $this->eventTypeRepository->findOrFail($id);

        $form = $this->form(EventTypeCreateForm::class, [
            'model' => $eventType,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.events_types.store.error'));

        $this->eventTypeRepository->updateByData($eventType,$form->getFieldValues());

        $this->eventTypeRepository->store($eventType);

        return redirect()->route('dashboard.eventsTypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventType = $this->eventTypeRepository->findOrFail($id);

        return redirect()->route('dashboard.eventsTypes.index');
    }
}
