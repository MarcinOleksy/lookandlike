<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 18:47
 */

namespace App\Http\Controllers\Dashboard;


use App\Forms\Dashboard\MenuItemCreateForm;
use App\Http\Controllers\Controller;
use App\Repositories\LocalRepository;
use App\Repositories\MenuItemRepository;
use App\Repositories\MenuRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MenuItemController extends Controller
{
    use FormBuilderTrait;

    protected $menuItemRepository;
    protected $menuRepository;
    protected $localRepository;

    public function __construct(
        MenuItemRepository $menuItemRepository,
        MenuRepository $menuRepository,
        LocalRepository $localRepository
    )
    {
        $this->menuItemRepository = $menuItemRepository;
        $this->menuRepository = $menuRepository;
        $this->localRepository = $localRepository;
    }

    public function index(int $local_id, int $menu_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $menu = $this->menuRepository->findOrFail($menu_id);

        $items = $this->menuItemRepository->paginateForMenu($menu);

        return view('dashboard.menu_items.index',compact('items', 'local','menu'));
    }

    public function create(int $local_id, int $menu_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $menu = $this->menuRepository->findOrFail($menu_id);

        $form = $this->form(MenuItemCreateForm::class, [
            'method' => 'POST',
            'model' => $menu,
            'url' => route('dashboard.locals.menu.items.store', ['local_id' => $local->id, 'menu_id' => $menu->id])
        ]);

        return view('dashboard.menu_items.create', compact('menu','form', 'local'));
    }


    public function store(int $local_id, int $menu_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $menu = $this->menuRepository->findOrFail($menu_id);

        $item = $this->menuItemRepository->findOrNew();

        $form = $this->form(MenuItemCreateForm::class, [
            'model' => $item
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.menu_items.store.error'));

        $data = $form->getFieldValues();
        $data['menu_id'] = $menu_id;

        $this->menuItemRepository->updateByData($item,$data);

        $this->menuItemRepository->store($item);

        return redirect()->route('dashboard.locals.menu.items.index', ['local_id' => $local->id, 'menu_id' => $menu->id])->with('success', trans('messages.menu_items.store.success'));
    }

    public function edit(int $local_id, int $menu_id, int $menu_item_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $menu = $this->menuRepository->findOrFail($menu_id);
        $item = $this->menuItemRepository->findOrFail($menu_item_id);

        $form = $this->form(MenuItemCreateForm::class, [
            'model' => $item,
            'method' => 'POST',
            'url' => route('dashboard.locals.menu.items.update', ['local_id' => $local->id, 'menu_id' => $menu->id, 'menu_type_id' => $item->id])
        ]);

        return view('dashboard.menu_items.edit', compact('item', 'form', 'local'));
    }

    public function update(int $local_id, int $menu_id, int $menu_item_id)
    {
        $local = $this->localRepository->findOrFail($local_id);
        $menu = $this->menuRepository->findOrFail($menu_id);
        $item = $this->menuItemRepository->findOrFail($menu_item_id);

        $form = $this->form(MenuItemCreateForm::class, [
            'model' => $item,
        ]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.menu_items.store.error'));

        $data = $form->getFieldValues();
        $data['menu_id'] = $menu_id;

        $this->menuItemRepository->updateByData($item,$data);

        $this->menuItemRepository->store($item);

        return redirect()->route('dashboard.locals.menu.items.index', ['local_id' => $local->id, 'menu_id' => $menu->id])->with('success', trans('messages.menu_items.store.success'));
    }

    public function destroy(int $menu_item_id)
    {
        $event = $this->menuItemRepository->findOrFail($menu_item_id);

        return redirect()->route('dashboard.items.events.index');
    }
}