<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 21.03.2020
 * Time: 20:36
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\MyRelationIndexSearchForm;
use App\Http\Controllers\Controller;
use App\Repositories\Filters\Front\MyRelationFilter;
use App\Repositories\LocalRelationRepository;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MyRelationController extends Controller
{
    use FormBuilderTrait;

    protected $localRelationRepository;

    public function __construct(
        LocalRelationRepository $localRelationRepository
    )
    {
        $this->localRelationRepository = $localRelationRepository;
    }

    public function index(MyRelationFilter $filter)
    {
        $formSearch = $this->form(MyRelationIndexSearchForm::class, [
            'model' => $filter->getFilterValues()
        ]);

        $relations = $this->localRelationRepository->paginateByUser(Auth::user(), $filter);

        return view('front.my_relations.index', compact('relations','formSearch'));
    }
}