<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 28.11.2019
 * Time: 18:55
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\EventIndexSearchForm;
use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;
use App\Repositories\Filters\Front\EventIndexFilter;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventController extends Controller
{
    use FormBuilderTrait;

    protected $eventRepository;
    protected $localRepository;
    protected $localRatingRepository;

    public function __construct(
        EventRepository $eventRepository,
        LocalRepository $localRepository,
        LocalRatingRepository $localRatingRepository
    )
    {
        $this->eventRepository = $eventRepository;
        $this->localRepository = $localRepository;
        $this->localRatingRepository = $localRatingRepository;

    }

    public function index(EventIndexFilter $eventIndexFilter)
    {
        $events = $this->eventRepository->paginate($eventIndexFilter);

        $formSearch = $this->form(EventIndexSearchForm::class, ['model' => $eventIndexFilter->getFilterValues()]);

        return view('front.events.index', compact('events', 'formSearch'));
    }

    public function show(int $id)
    {
        $event = $this->eventRepository->findOrFail($id);

        $ratings = $this->localRatingRepository->paginateForLocal($event->local);

        return view('front.events.show', compact('event','ratings'));
    }
}