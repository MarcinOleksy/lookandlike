<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 14.12.2019
 * Time: 21:46
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\LocalVisitCodeForm;
use App\Http\Controllers\Controller;
use App\Models\LocalVisitCode;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Auth;

class LocalVisitCodeController extends Controller
{
    use FormBuilderTrait;

    protected $localVisitCodeRepository;
    protected $localRepository;

    public function __construct(
        LocalVisitCodeRepository $localVisitCodeRepository,
        LocalRepository $localRepository
    )
    {
        $this->localVisitCodeRepository = $localVisitCodeRepository;
        $this->localRepository = $localRepository;
    }

    public function showFormVisitCode()
    {
        if(!Auth::check())
            return redirect()->route('front.login.showLoginForm');

        $form = $this->form(LocalVisitCodeForm::class, [
            'method' => 'POST',
            'url' => route('front.visit_codes.choiceForm')
        ]);

        return view('front.locals_visit_codes.showFormVisitCode' ,compact('form'));
    }

    public function choiceForm(Request $request)
    {
        $form = $this->form(LocalVisitCodeForm::class);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors());

        $code = $form->getFieldValues()['code'];

        $visitCodeItem = $this->localVisitCodeRepository->findCode($code);

        if(!$visitCodeItem)
            return redirect()->back()->with('error', trans('messages.locals_visit_codes.find_code.error'));

        $local = $this->localRepository->findOrFail($visitCodeItem->local_id);

        $request->session()->put('local_id', $local->id);
        $request->session()->put('code_id', $visitCodeItem->id);

        return view('front.locals_visit_codes.choiceForm', compact('local', 'visitCodeItem'));
    }
}