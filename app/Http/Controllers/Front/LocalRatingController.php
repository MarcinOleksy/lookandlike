<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 14.12.2019
 * Time: 21:18
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\LocalRatingCreateForm;
use App\Http\Controllers\Controller;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalRatingController extends Controller
{
    use FormBuilderTrait;

    protected $localRatingRepository;
    protected $localRepository;
    protected $localVisitCodeRepository;

    public function __construct(
        LocalRatingRepository $localRatingRepository,
        LocalRepository $localRepository,
        LocalVisitCodeRepository $localVisitCodeRepository
    )
    {
        $this->localRatingRepository = $localRatingRepository;
        $this->localRepository = $localRepository;
        $this->localVisitCodeRepository = $localVisitCodeRepository;
    }

    public function index(int $local_id)
    {
        $local = $this->localRepository->findOrFail($local_id);

        $items = $this->localRatingRepository->paginateForLocal($local);

        return view('front.locals_ratings.index',compare('local','items'));
    }

    public function create(Request $request)
    {
        $local_id = $request->session()->get('local_id');
        $code_id = $request->session()->get('code_id');

        $local = $this->localRepository->findOrFail($local_id);
        $visitCode = $this->localVisitCodeRepository->findOrFail($code_id);

        if($visitCode->local_id != $local->id)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.find_for_local.error'));

        if($visitCode->expired)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.expired.error'));

        $form = $this->form(LocalRatingCreateForm::class, [
            'method' => 'POST',
            'url' => route('front.ratings.store')
        ]);

        return view('front.locals_ratings.create',compact('local', 'form'));
    }

    public function store(Request $request)
    {
        $local_id = $request->session()->get('local_id');
        $code_id = $request->session()->get('code_id');

        $local = $this->localRepository->findOrFail($local_id);
        $visitCode = $this->localVisitCodeRepository->findOrFail($code_id);

        if($visitCode->local_id != $local->id)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.find_for_local.error'));

        if($visitCode->expired)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.expired.error'));


        $form = $this->form(LocalRatingCreateForm::class);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals_ratings.store.error'));;

        $rating = $this->localRatingRepository->findOrNew();

        $rating->user_id = Auth::user()->id;
        $rating->local_id = $local_id;
        $rating->code_id = $code_id;

        $this->localRatingRepository->updateByData($rating,$form->getFieldValues());

        $this->localRatingRepository->store($rating);

        $visitCode->expired = 1;
        $this->localVisitCodeRepository->store($visitCode);

        return redirect()->route('front.locals.show', ['local_id' => $local->id])->with('success', trans('messages.locals_ratings.store.success'));
    }
}