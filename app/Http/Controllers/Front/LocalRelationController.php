<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 27.04.2020
 * Time: 00:26
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\LocalRelationCreateForm;
use App\Http\Controllers\Controller;

use App\Repositories\LocalRelationRepository;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Auth;

class LocalRelationController extends Controller
{
    use FormBuilderTrait;

    protected $localRelationRepository;
    protected $localRepository;
    protected $localVisitCodeRepository;

    public function __construct(
        LocalRelationRepository $localRelationRepository,
        LocalRepository $localRepository,
        LocalVisitCodeRepository $localVisitCodeRepository
    )
    {
        $this->localRelationRepository = $localRelationRepository;
        $this->localRepository = $localRepository;
        $this->localVisitCodeRepository = $localVisitCodeRepository;
    }

    public function create(Request $request)
    {
        $local_id = $request->session()->get('local_id');
        $code_id = $request->session()->get('code_id');

        $local = $this->localRepository->findOrFail($local_id);
        $visitCode = $this->localVisitCodeRepository->findOrFail($code_id);

        if($visitCode->local_id != $local->id)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.find_for_local.error'));


        $form = $this->form(LocalRelationCreateForm::class, [
            'url' => route('front.relations.store'),
            'method' => 'POST',
        ]);

        return view('front.locals_relations.create', compact('form', 'local'));
    }

    public function store(Request $request)
    {
        $local_id = $request->session()->get('local_id');
        $code_id = $request->session()->get('code_id');

        $local = $this->localRepository->findOrFail($local_id);
        $visitCode = $this->localVisitCodeRepository->findOrFail($code_id);

        if($visitCode->local_id != $local->id)
            return redirect()->route('front.locals.visit_codes.showFormVisitCode')->with('error', trans('messages.locals_visit_codes.find_for_local.error'));

        $form = $this->form(LocalRelationCreateForm::class);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.locals_ratings.store.error'));;

        $relation = $this->localRelationRepository->findOrNew();

        $relation->user_id = Auth::user()->id;
        $relation->local_id = $local_id;
        $relation->code_id = $code_id;

        $this->localRelationRepository->updateByData($relation,$form->getFieldValues());

        $this->localRelationRepository->store($relation);

        return redirect()->route('front.locals.show', ['local_id' => $local->id])->with('success', trans('messages.locals_ratings.store.success'));
    }

    public function edit(int $relation_id)
    {
        return view('front.locals_relations.edit', compact());
    }

    public function update(int $relation_id)
    {
        return view('front.locals_relations.edit', compact());
    }
}