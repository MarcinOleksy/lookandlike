<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 21.03.2020
 * Time: 20:37
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\LocalRatingIndexSearchForm;
use App\Http\Controllers\Controller;
use App\Forms\Front\MyRatingIndexSearchForm;
use App\Repositories\Filters\Front\MyRatingFilter;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use App\Repositories\LocalVisitCodeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;


class MyRatingController extends Controller
{
    use FormBuilderTrait;

    protected $localRatingRepository;
    protected $localRepository;
    protected $localVisitCodeRepository;

    public function __construct(
        LocalRatingRepository $localRatingRepository,
        LocalRepository $localRepository,
        LocalVisitCodeRepository $localVisitCodeRepository
    )
    {
        $this->localRatingRepository = $localRatingRepository;
        $this->localRepository = $localRepository;
        $this->localVisitCodeRepository = $localVisitCodeRepository;
    }

    public function index(MyRatingFilter $myRatingFilter)
    {
        $ratings = $this->localRatingRepository->paginateForUser(Auth::user(), $myRatingFilter);

        $formSearch = $this->form(MyRatingIndexSearchForm::class, ["model" => $myRatingFilter->getFilterValues()]);

        return view('front.my_ratings.index', compact('ratings', 'formSearch'));
    }
}

