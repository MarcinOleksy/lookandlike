<?php

namespace App\Http\Controllers\Front\Auth;

use App\Forms\Front\Auth\PasswordForgetForm;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    use FormBuilderTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $form = $this->form(PasswordForgetForm::class, [
            'method' => 'POST',
            'url' => route('front.passwordForget.send')
        ]);

        return view('front.auth.passwords.email_form', compact('form'));
    }
}
