<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 20.11.2019
 * Time: 21:22
 */

namespace App\Http\Controllers\Front\Auth;


use App\Forms\Front\Auth\AccountEditForm;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Authenticate;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ChangeAccountController extends Controller
{
    use FormBuilderTrait;

    protected $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function edit()
    {
        /** @var User $user */
        $user = Auth::user();

        $form = $this->form(AccountEditForm::class, [
            'model' => $user,
            'method' => 'POST',
            'url' => route('front.accountChange.update')
        ],['user' => $user]);

        return view('front.auth.account_edit_form', compact('user', 'form'));
    }

    public function update()
    {
        /** @var User $user */
        $user = Auth::user();

        $form = $this->form(AccountEditForm::class, [
            'model' => $user,
        ],['user' => $user]);

        if(!$form->isValid())
            return redirect()->back()->withInput()->withErrors($form->getErrors())->with('error', trans('messages.auth_account.store.error'));

        $this->userRepository->updateByData($user,$form->getFieldValues());

        $this->userRepository->store($user);

        return redirect()->route('front.accountChange.edit')->with('success','messages.auth_account.store.success');
    }
}