<?php

namespace App\Http\Controllers\Front\Auth;

use App\Forms\Front\Auth\LoginForm;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, FormBuilderTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectToDashboard = '/panel/lokale';
    protected $redirectToFront = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $form = $this->form(LoginForm::class, [
            'method' => 'POST',
            'url' => route('front.login.verification')
        ]);

        return view('front.auth.login_form', compact('form'));
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route('front.login.showLoginForm');
    }

    public function redirectTo()
    {
        return in_array(Auth::user()->acl_group->slug, ['user']) ? $this->redirectToFront : $this->redirectToDashboard;
    }
}
