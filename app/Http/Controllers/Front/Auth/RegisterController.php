<?php

namespace App\Http\Controllers\Front\Auth;

use App\Forms\Front\Auth\RegisterForm;
use App\Models\AclUserGroup;
use App\Repositories\UserRepository;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use League\Flysystem\Config;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, FormBuilderTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $userRepository;


    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->middleware('guest');
        $this->userRepository = $userRepository;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        $form = $this->form(RegisterForm::class,[
            'url' => route('front.registration.register'),
            'method' => 'POST',
        ]);

        return view('front.auth.register_form', compact('form'));
    }

    public function register(Request $request)
    {
        $user = $this->userRepository->findOrNew();

        $form = $this->form(RegisterForm::class);

        if(!$form->isValid())
            return redirect()->back()->withErrors($form->getErrors())->withInput();

        $this->userRepository->updateByData($user,$form->getFieldValues());

        $this->userRepository->store($user);

        $user->assignGroup([ AclUserGroup::USER_ID ]);

        Auth::loginUsingId($user->id);

        return redirect()->route('front.locals.index')->with('success', trans('messages.users.store.success'));
    }
}
