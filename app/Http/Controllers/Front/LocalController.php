<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 28.11.2019
 * Time: 18:55
 */

namespace App\Http\Controllers\Front;


use App\Forms\Front\LocalIndexSearchForm;
use App\Forms\Front\LocalRatingShowSearchForm;
use App\Http\Controllers\Controller;
use App\Repositories\Filters\Front\LocalIndexFilter;
use App\Repositories\Filters\Front\LocalRatingShowFilter;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LocalController extends Controller
{
    use FormBuilderTrait;

    protected $localRepository;
    protected $localRatingRepository;

    public function __construct(
        LocalRepository $localRepository,
        LocalRatingRepository $localRatingRepository
    )
    {
        $this->localRepository = $localRepository;
        $this->localRatingRepository = $localRatingRepository;
    }

    public function index(LocalIndexFilter $localIndexFilter)
    {
        $locals = $this->localRepository->paginateFrontIndex($localIndexFilter);

        $formSearch = $this->form(LocalIndexSearchForm::class, ["model" => $localIndexFilter->getFilterValues()]);

        return view('front.locals.index', compact('locals', 'formSearch'));
    }

    public function show(LocalRatingShowFilter $localRatingShowFilter, int $id)
    {
        $local = $this->localRepository->findOrFail($id);

        $ratings = $this->localRatingRepository->paginateForLocal($local, $localRatingShowFilter);

        $formSearchRating = $this->form(LocalRatingShowSearchForm::class,["model" => $localRatingShowFilter->getFilterValues()]);

        return view('front.locals.show', compact('local','ratings','formSearchRating'));
    }
}