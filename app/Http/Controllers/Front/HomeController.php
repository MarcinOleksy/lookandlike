<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 28.11.2019
 * Time: 19:19
 */

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('front.home.index');
    }
}