<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 16.11.2019
 * Time: 13:09
 */

namespace App\Http\Controllers\Addons;


use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use App\Services\ImageSaveFile;
use Illuminate\Http\Request;
use App\Models\Image as ImageModel;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    public $imageRepository;
    public $imageSaveFile;

    public function __construct(
        ImageRepository $imageRepository,
        ImageSaveFile $imageSaveFile
    )
    {
        $this->imageRepository = $imageRepository;
        $this->imageSaveFile = $imageSaveFile;
    }

    public function create(Request $request)
    {
        $image = $request->file('image_id');

        dd('test_image',$this->imageSaveFile->create($image));

        return ;
    }
}