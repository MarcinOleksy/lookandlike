<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 07.04.2020
 * Time: 21:53
 */

namespace App\Http\Middleware;

use App\Exceptions\PermissionDenyException;
use App\Navigation\ConvertNavigation;
use App\Navigation\Navigation;
use App\Policies\Controllers\Dashboard\LocalControllerPolicy;
use App\Policies\Policy;
use Closure;
use Illuminate\Http\Request;

class Permission
{

    public function handle(Request $request, Closure $next)
    {
        $requestRouteExplode = explode('.',$request->route()->getName());
        $navigation = (new Navigation())->navigation();
        $routeParameters = $request->route()->parameters;

        $urlObj = null;
        $permission = null;
        $auth = null;

        foreach($requestRouteExplode as $key => $requestRouteLevel)
        {
            foreach ($navigation as $navigationLevel)
            {
                if($navigationLevel->getRoute() == $requestRouteLevel)
                {
                    $navigation = $navigationLevel->getChildren();
                    $urlObj = $navigationLevel;

                    if(!is_null($urlObj->getPermission($routeParameters)))
                        $permission = $urlObj->getPermission($routeParameters);

                    if(!is_null($urlObj->getAuth()))
                        $auth = $urlObj->getAuth();
                }
            }

            if(count($requestRouteExplode)-1 == $key)
                break;
        }

        if($permission || !$auth)
            return $next($request);

        throw new PermissionDenyException($requestRouteExplode[0]);
    }
}