<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 14.12.2019
 * Time: 21:45
 */

namespace App\Forms\Front;


use Kris\LaravelFormBuilder\Form;

class LocalVisitCodeForm extends Form
{
    public function buildForm()
    {
        $this->add('code', 'text', [
            'label' => trans('labels.local_visit_code'),
            'rules' => 'required'
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}