<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 20.11.2019
 * Time: 21:23
 */

namespace App\Forms\Front\Auth;

use Kris\LaravelFormBuilder\Form;

class AccountEditForm extends Form
{
    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('email', 'text', [
            'label' => trans('labels.email'),
            'rules' => 'required|email|unique:users,email,'.$this->getUserId(),
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'nullable|image'
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }

    public function getUser()
    {
        return $this->getData('user');
    }

    public function getUserId():int
    {
        return $this->getUser()->id;
    }
}