<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 17:01
 */

namespace App\Forms\Front\Auth;


use Kris\LaravelFormBuilder\Form;

class PasswordForgetForm extends Form
{
    public function buildForm()
    {
        $this->add('email', 'text', [
            'rules' => 'required',
            'label' => trans('labels.email')
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.send'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}