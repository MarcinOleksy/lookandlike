<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 20.11.2019
 * Time: 20:17
 */

namespace App\Forms\Front\Auth;


use Kris\LaravelFormBuilder\Form;

class PasswordChangeForm extends Form
{
    public function buildForm()
    {
        $this->add('current_password', 'password', [
            'rules' => 'required|isCurrentPassword',
            'label' => trans('labels.current_password')
        ]);

        $this->add('password', 'password', [
            'rules' => 'required|min:8',
            'label' => trans('labels.password')
        ]);

        $this->add('password_confirmation', 'password', [
            'rules' => 'required|same:password',
            'label' => trans('labels.repeat_password')
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}