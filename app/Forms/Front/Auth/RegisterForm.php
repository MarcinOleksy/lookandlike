<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 12.12.2019
 * Time: 19:36
 */

namespace App\Forms\Front\Auth;


use Kris\LaravelFormBuilder\Form;

class RegisterForm extends Form
{
    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('email', 'text', [
            'label' => trans('labels.email'),
            'rules' => 'required|email|unique:users,email',
        ]);

        $this->add('password', 'password', [
            'label' => trans('labels.password'),
            'rules' => 'required',
        ]);

        $this->add('repeat_password', 'password', [
            'label' => trans('labels.repeat_password'),
            'rules' => 'required|same:password',
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.create_account'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}