<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 14.12.2019
 * Time: 21:24
 */

namespace App\Forms\Front;


use Kris\LaravelFormBuilder\Form;

class LocalRelationCreateForm extends Form
{
    public function __construct()
    {
        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('comment', 'textarea', [
            'label' => trans('labels.comment'),
            'attr' => [
                'rows' => 5
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}