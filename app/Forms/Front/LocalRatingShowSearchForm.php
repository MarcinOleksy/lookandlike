<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.06.2020
 * Time: 00:33
 */

namespace App\Forms\Front;


use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class LocalRatingShowSearchForm extends Form
{
    const DATETIME_FORMAT = 'd-m-Y H:i';

    protected $localRepository;

    public function buildForm()
    {
        $this->add('rating_from', 'text', [
            'label' => trans('labels.rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('rating_to', 'text', [
            'label' => trans('labels.rating_to'),
            'attr' => [
                'placeholder' => '10'
            ]
        ]);

        $this->add('created_from', 'text', [
            'label' => trans('labels.start_from'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('created_to', 'text', [
            'label' => trans('labels.start_to'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}