<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:34
 */

namespace App\Forms\Front;


use App\Repositories\EventTypeRepository;
use App\Repositories\LocalTypeRepository;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class EventIndexSearchForm extends Form
{
    const DATETIME_FORMAT = 'd-m-Y H:i';

    protected $localTypeRepository;
    protected $eventTypeRepository;

    public function __construct(
        LocalTypeRepository $localTypeRepository,
        EventTypeRepository $eventTypeRepository
    )
    {
        $this->localTypeRepository = $localTypeRepository;
        $this->eventTypeRepository = $eventTypeRepository;
    }

    public function buildForm()
    {
        $this->add('local_types', 'select',[
            'label' => trans('labels.local_types'),
            'choices' => $this->localTypeRepository->pluck()->toArray(),
            'attr' => [
                'multiple' => true,
                'title' => 'Wszystkie'
            ]
        ]);

        $this->add('event_types', 'select',[
            'label' => trans('labels.event_types'),
            'choices' => $this->eventTypeRepository->pluck()->toArray(),
            'attr' => [
                'multiple' => true,
                'title' => 'Wszystkie'
            ]
        ]);

        $this->add('start_from', 'text', [
            'label' => trans('labels.start_from'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('start_to', 'text', [
            'label' => trans('labels.start_to'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->addMonths(1)->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('rating_from', 'text', [
            'label' => trans('labels.rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('rating_to', 'text', [
            'label' => trans('labels.rating_to'),
            'attr' => [
                'placeholder' => '10'
            ]
        ]);

        $this->add('count_rating_from', 'text', [
            'label' => trans('labels.count_rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('count_rating_to', 'text', [
            'label' => trans('labels.count_rating_to'),
            'attr' => [
                'placeholder' => '9999+'
            ]
        ]);

        $this->add('hide_rated', 'checkbox', [
            'label' => trans('labels.hide_rated'),
        ]);

        $this->add('hide_not_rated', 'checkbox', [
            'label' => trans('labels.hide_not_rated')
        ]);

        $this->add('want_see_local', 'checkbox', [
            'label' => trans('labels.want_see_local')
        ]);

        $this->add('want_see_event', 'checkbox', [
            'label' => trans('labels.want_see_event')
        ]);

        $this->add('free_entrance', 'checkbox', [
            'label' => trans('labels.free_entrance'),
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}