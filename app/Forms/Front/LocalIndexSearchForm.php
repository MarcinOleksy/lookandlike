<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 15.12.2019
 * Time: 10:34
 */

namespace App\Forms\Front;


use App\Repositories\LocalTypeRepository;
use Kris\LaravelFormBuilder\Form;

class LocalIndexSearchForm extends Form
{
    protected $localTypeRepository;

    public function __construct(
        LocalTypeRepository $localTypeRepository
    )
    {
        $this->localTypeRepository = $localTypeRepository;
    }

    public function buildForm()
    {
        $this->add('local_types', 'select',[
            'label' => trans('labels.local_types'),
            'choices' => $this->localTypeRepository->pluck()->toArray(),
            'attr' => [
                'multiple' => true,
                'title' => 'Wszystkie'
            ]
        ]);

        $this->add('rating_from', 'text', [
            'label' => trans('labels.rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('rating_to', 'text', [
            'label' => trans('labels.rating_to'),
            'attr' => [
                'placeholder' => '10'
            ]
        ]);

        $this->add('count_rating_from', 'text', [
            'label' => trans('labels.count_rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('count_rating_to', 'text', [
            'label' => trans('labels.count_rating_to'),
            'attr' => [
                'placeholder' => '9999+'
            ]
        ]);

        $this->add('hide_rated', 'checkbox', [
            'label' => trans('labels.hide_rated'),
        ]);

        $this->add('hide_not_rated', 'checkbox', [
            'label' => trans('labels.hide_not_rated')
        ]);

        $this->add('want_see', 'checkbox', [
            'label' => trans('labels.want_see')
        ]);


        $this->add('free_entrance', 'checkbox', [
            'label' => trans('labels.free_entrance'),
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}