<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 14.12.2019
 * Time: 21:24
 */

namespace App\Forms\Front;


use Kris\LaravelFormBuilder\Form;

class LocalRatingCreateForm extends Form
{
    public function buildForm()
    {
        $this->add('rating', 'number', [
            'label' => trans('labels.rate'),
            'rules' => 'required',
            'attr' => [
                'step' => '1',
                'max' => '10',
                'min' => 1
            ]
        ]);

        $this->add('comment', 'textarea', [
            'label' => trans('labels.comment'),
            'attr' => [
                'rows' => 5
            ]
        ]);


        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}