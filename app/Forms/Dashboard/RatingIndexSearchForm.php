<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 24.03.2020
 * Time: 20:56
 */

namespace App\Forms\Dashboard;

use App\Repositories\LocalRepository;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class RatingIndexSearchForm  extends Form
{
    const DATETIME_FORMAT = 'd-m-Y H:i';

    public function buildForm()
    {
        $this->add('rating_from', 'text', [
            'label' => trans('labels.rating_from'),
            'attr' => [
                'placeholder' => '0'
            ]
        ]);

        $this->add('rating_to', 'text', [
            'label' => trans('labels.rating_to'),
            'attr' => [
                'placeholder' => '10'
            ]
        ]);

        $this->add('created_from', 'text', [
            'label' => trans('labels.start_from'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('created_to', 'text', [
            'label' => trans('labels.start_to'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}