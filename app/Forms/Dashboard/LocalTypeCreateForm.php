<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 01.09.2019
 * Time: 20:10
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class LocalTypeCreateForm extends Form
{
    public function __construct()
    {
        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}