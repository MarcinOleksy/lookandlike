<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.05.2020
 * Time: 20:32
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class LocalVisitCodeIndexSearchForm extends Form
{
    public function buildForm()
    {
        $this->add('printed_at', 'select', [
            'choices' => [
                '0' => 'Nie pobrane',
                '1' => 'Pobrane'
            ],
            'attr' => [

            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}