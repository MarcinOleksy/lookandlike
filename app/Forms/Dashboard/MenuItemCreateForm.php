<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 18:44
 */

namespace App\Forms\Dashboard;


use App\Repositories\MenuItemCategoryRepository;
use Kris\LaravelFormBuilder\Form;

class MenuItemCreateForm extends Form
{
    protected $menuItemCategoryRepository;

    public function __construct(
        MenuItemCategoryRepository $menuItemCategoryRepository
    )
    {
        $this->menuItemCategoryRepository = $menuItemCategoryRepository;

        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('category_id', 'select', [
            'label' => trans('labels.categories'),
            'rules' => 'required',
            'choices' => $this->menuItemCategoryRepository->pluck()->toArray()
        ]);

        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('price', 'number', [
            'label' => trans('labels.price'),
            'attr' => array(
                'min' => '1',
                'step' => '0.5'
            )
        ]);

        $this->add('description', 'textarea', [
            'label' => trans('labels.description'),
        ]);

        $this->add('components', 'textarea', [
            'label' => trans('labels.components'),
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }

}