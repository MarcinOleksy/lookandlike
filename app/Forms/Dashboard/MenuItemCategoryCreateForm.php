<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.11.2019
 * Time: 18:36
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class MenuItemCategoryCreateForm extends Form
{
    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required'
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}