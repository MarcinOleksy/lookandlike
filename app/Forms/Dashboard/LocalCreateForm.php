<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 01.09.2019
 * Time: 18:22
 */

namespace App\Forms\Dashboard;


use App\Models\AclUserGroup;
use App\Repositories\LocalTypeRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class LocalCreateForm extends Form
{
    protected $localTypeRepository;
    protected $usersRepository;

    public function __construct(LocalTypeRepository $localTypeRepository, UserRepository $usersRepository)
    {
        $this->localTypeRepository = $localTypeRepository;
        $this->usersRepository = $usersRepository;

        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        if(Auth::user()->acl_group->slug == AclUserGroup::ADMIN_SLUG)
        {
            $this->add('owners_id', 'select', [
                'label' => trans('labels.owners'),
                'rules' => 'required',
                'choices' => $this->usersRepository->managerPluck()->toArray(),
                'selected' => $this->getModel()->local_owners->pluck('id')->toArray(),
                'attr' => [
//                    'placeholder' => trans('labels.select_default'),
                    'multiple' => true,
                ]
            ]);
        }


        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('type_id', 'select', [
            'label' => trans('labels.type'),
            'rules' => 'required',
            'choices' => $this->localTypeRepository->pluck()->toArray(),
            'attr' => [
                'placeholder' => trans('labels.select_default')
            ]
        ]);

        $this->add('description', 'textarea', [
            'label' => trans('labels.description'),
            'rules' => 'required',
        ]);

        $this->add('address', 'text', [
            'label' => trans('labels.address'),
            'rules' => 'required',
        ]);

        $this->add('longitude', 'number',[
            'label' => trans('labels.longitude'),
            'rules' => 'required',
            'attr' => [
                'step' => '0.000000000000000001',
                'max' => '100',
                'min' => '-100',
                'readonly' => "readonly"
            ]
        ]);

        $this->add('latitude', 'number',[
            'label' => trans('labels.latitude'),
            'rules' => 'required',
            'attr' => [
                'step' => '0.000000000000000001',
                'max' => '100',
                'min' => '-100',
                'readonly' => "readonly"
            ]
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}