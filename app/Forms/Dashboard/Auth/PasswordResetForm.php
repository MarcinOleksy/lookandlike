<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 17:02
 */

namespace App\Forms\Dashboard\Auth;

use Kris\LaravelFormBuilder\Form;

class PasswordResetForm extends Form
{
    public function buildForm()
    {
        $this->add('email', 'text', [
            'rules' => 'required|exists:users,email',
            'label' => trans('labels.email')
        ]);

        $this->add('password', 'password', [
            'rules' => 'required|min:8',
            'label' => trans('labels.password')
        ]);

        $this->add('password_confirmation', 'password', [
            'rules' => 'required|same:password',
            'label' => trans('labels.password')
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}