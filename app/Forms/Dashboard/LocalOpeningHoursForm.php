<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 11.12.2019
 * Time: 20:57
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class LocalOpeningHoursForm extends Form
{
    public function buildForm()
    {
        $this->add('monday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:monday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('monday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:monday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('tuesday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:tuesday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('tuesday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:tuesday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('wednesday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:wednesday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('wednesday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:wednesday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('thursday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:thursday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('thursday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:thursday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('friday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:friday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('friday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:friday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('saturday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:saturday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('saturday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:saturday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('sunday_open', 'text', [
            'label' => trans('labels.open'),
            'rules' => 'required_with:sunday_close',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('sunday_close', 'text', [
            'label' => trans('labels.close'),
            'rules' => 'required_with:sunday_open',
            'attr' => [
                'class' => 'form-control timepicker',
                'data-time-task' => 'true'
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}