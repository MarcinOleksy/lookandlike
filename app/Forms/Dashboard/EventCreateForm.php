<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 14:34
 */

namespace App\Forms\Dashboard;


use App\Repositories\EventTypeRepository;
use App\Repositories\LocalRepository;
use Kris\LaravelFormBuilder\Form;

class EventCreateForm extends Form
{
    protected $localRepository;
    protected $eventTypeRepository;

    public function __construct(
        LocalRepository $localRepository,
        EventTypeRepository $eventTypeRepository
    )
    {
        $this->localRepository = $localRepository;
        $this->eventTypeRepository = $eventTypeRepository;

        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('local_id', 'select', [
            'label' => trans('labels.local'),
            'rules' => 'required',
            'choices' => $this->localRepository->pluck()->toArray(),
        ]);

        $this->add('title', 'text', [
            'label' => trans('labels.title'),
            'rules' => 'required'
        ]);

        $this->add('description','textarea', [
            'label' => trans('labels.description'),
        ]);

        $this->add('type_id', 'select', [
            'label' => trans('labels.types'),
            'rules' => 'required',
            'choices' => $this->eventTypeRepository->pluck()->toArray()
        ]);

        $this->add('start_at', 'text', [
            'label' => trans('labels.start_at'),
            'rules' => 'required'
        ]);

        $this->add('finish_at', 'text', [
            'label' => trans('labels.finish_at'),
            'rules' => 'required'
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}