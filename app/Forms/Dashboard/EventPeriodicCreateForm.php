<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 15:29
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class EventPeriodicCreateForm extends Form
{
    public function __construct()
    {
        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('title', 'text', [
            'label' => trans('labels.title'),
            'rules' => 'required'
        ]);

        $this->add('description','textarea', [
            'label' => trans('labels.description'),
        ]);

        $this->add('type_id', 'select', [
            'label' => trans('labels.types'),
            'rules' => 'required'
        ]);

        $this->add('status' , 'select', [
            'label' => trans('labels.status'),
            'rules' => 'required',
            'choices' => array(
                'implemented' => trans('label.implemented'),
                'suspended' => trans('label.suspended')
            )
        ]);

        $this->add('day_of_week', 'text', [
            'label' => trans('labels.start_at'),
            'rules' => 'required',
            'choices' => array(
                1=> trans('labels.monday'),
                2=> trans('labels.tuesday'),
                3=> trans('labels.wednesday'),
                4=> trans('labels.thursday'),
                5=> trans('labels.friday'),
                6=> trans('labels.saturday'),
                0=> trans('labels.sunday')
            )
        ]);

        $this->add('time_start', 'text', [
            'label' => trans('labels.time_start'),
            'rules' => 'required'
        ]);

        $this->add('duration', 'text', [
            'label' => trans('labels.duration'),
            'rules' => 'required'
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}