<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.11.2019
 * Time: 14:34
 */

namespace App\Forms\Dashboard;


use Kris\LaravelFormBuilder\Form;

class EventTypeCreateForm extends Form
{
    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required'
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}