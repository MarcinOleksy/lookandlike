<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 07.05.2020
 * Time: 22:01
 */

namespace App\Forms\Dashboard;


use Carbon\Carbon;
use Junges\ACL\Http\Models\Group;
use Kris\LaravelFormBuilder\Form;

class UserIndexSearchForm extends Form
{
    const DATETIME_FORMAT = 'd-m-Y H:i';

    protected $aclGroup;

    public function __construct()
    {
        $this->aclGroup = new Group();
    }

    public function buildForm()
    {
        $this->add('search_engine', 'text', [
            'label' => trans('labels.search_engine'),
            'attr' => [
                'placeholder' => trans('labels.search_engine'),
            ]
        ]);

        $this->add('acl_group', 'select', [
            'label' => trans('labels.role'),
            'choices' => $this->aclGroup->pluck('name','id')->toArray(),
            'attr' => [
                'placeholder' => trans('labels.select_default'),
            ]
        ]);

        $this->add('created_from', 'text', [
            'label' => trans('labels.created_from'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('created_to', 'text', [
            'label' => trans('labels.created_to'),
            'attr' => [
                'class' => 'form-control datetimepicker',
                'data-time-task' => 'true',
                'placeholder' => Carbon::now()->addMonths(1)->format(self::DATETIME_FORMAT),
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.search'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }
}