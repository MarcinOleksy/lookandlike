<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 01.09.2019
 * Time: 18:22
 */

namespace App\Forms\Dashboard;


use App\Models\User;
use App\Repositories\LocalTypeRepository;
use Kris\LaravelFormBuilder\Form;
use Junges\ACL\Http\Models\Group;


class UserCreateForm extends Form
{
    protected $aclGroup;

    public function __construct()
    {
        $this->aclGroup = new Group();

        $this->setFormOption('enctype', 'multipart/form-data');
    }

    public function buildForm()
    {
        $this->add('name', 'text', [
            'label' => trans('labels.name'),
            'rules' => 'required',
        ]);

        $this->add('email', 'text', [
            'label' => trans('labels.email'),
            'rules' => 'required|email|unique:users,email,id,'.$this->getUserId(),
        ]);

        $this->add('acl_group', 'select', [
            'label' => trans('labels.role'),
            'rules' => 'required',
            'choices' => $this->aclGroup->pluck('name','id')->toArray(),
            'attr' => [
                'placeholder' => trans('labels.select_default'),
            ]
        ]);

        $this->add('password', 'password', [
            'label' => trans('labels.password'),
            'rules' => 'required',
        ]);

        $this->add('image_id', 'image', [
            'label' => trans('labels.image'),
            'rules' => 'image',
            'attr' => [
                'image_path' => $this->getModel()->image_main ? '/storage/'.$this->getModel()->image_main->original_image_path : null,
                'image_name' => $this->getModel()->image_main ? substr($this->getModel()->image_main->original_image_path, 37) : null
            ]
        ]);

        $this->add('submit', 'submit', [
            'label' => trans('labels.save'),
            'attr' => [
                'class' => 'form-control btn btn-success'
            ]
        ]);
    }

    public function getUser():?User
    {
        return $this->getData('user');
    }

    public function getUserId():?int
    {
        return $this->getUser() ? $this->getUser()->id : null;
    }
}