<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 16.11.2019
 * Time: 14:56
 */

namespace App\Forms\Fields;


use App\Services\ImageSaveFile;
use http\Env\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Request as R;
use Kris\LaravelFormBuilder\Fields\InputType;

class ImageType extends InputType
{
    public function getTemplate()
    {
        return 'fields.image';
    }

    public function alterFieldValues(&$value): void
    {
        $value = $this->getAlterValue($value);
    }

    protected function getAlterValue($value): ?string
    {
        if ($value instanceof UploadedFile)
            return $this->getValueFromFile($value);

        if ($this->isImageToRemove())
            return null;

        if ($this->parent->getModel())
            return $this->parent->getModel()->{$this->getRealName()};

        return null;
    }

    protected function getValueFromFile(UploadedFile $value): ?int
    {
        return (app(ImageSaveFile::class)->create($value))->id;
    }

    protected function isImageToRemove(): bool
    {
        return !!$this->parent->getRequest()->get('remove_image_' . $this->getRealName());
    }
}