<?php

namespace App\Observers;

use App\Models\Local;
use App\Repositories\LocalStatsRepository;

class LocalObserver
{
    /**
     * Handle the local "created" event.
     *
     * @param  \App\Local  $local
     * @return void
     */
    public function creating(Local $local)
    {
        $localStatsRepository = new LocalStatsRepository();
        $localStats = $localStatsRepository->findOrNew();
        $localStatsRepository->store($localStats);
        $local->local_stats_id = $localStats->id;
    }

    /**
     * Handle the local "updated" event.
     *
     * @param  \App\Local  $local
     * @return void
     */
    public function updated(Local $local)
    {
        //
    }

    /**
     * Handle the local "deleted" event.
     *
     * @param  \App\Local  $local
     * @return void
     */
    public function deleted(Local $local)
    {
        //
    }

    /**
     * Handle the local "restored" event.
     *
     * @param  \App\Local  $local
     * @return void
     */
    public function restored(Local $local)
    {
        //
    }

    /**
     * Handle the local "force deleted" event.
     *
     * @param  \App\Local  $local
     * @return void
     */
    public function forceDeleted(Local $local)
    {
        //
    }
}
