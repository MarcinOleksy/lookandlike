<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 01.04.2020
 * Time: 23:13
 */

namespace App\Observers;


use App\Models\LocalRating;
use App\Repositories\LocalRatingRepository;
use App\Repositories\LocalRepository;
use App\Repositories\LocalStatsRepository;

class LocalRatingObserver
{
    protected $localStatsRepository;
    protected $localRepository;
    protected $localRatingRepository;

    public function __construct(
        LocalStatsRepository $localStatsRepository,
        LocalRepository $localRepository,
        LocalRatingRepository $localRatingRepository
    )
    {
        $this->localStatsRepository = $localStatsRepository;
        $this->localRepository = $localRepository;
        $this->localRatingRepository = $localRatingRepository;
    }

    public function created(LocalRating $localRating)
    {
        $local = $this->localRepository->findOrFail($localRating->local_id);
        $newStats = $this->localRatingRepository->itemAllStatsForLocal($local);
        $localStats = $this->localStatsRepository->findOrFail($local->local_stats_id);

        $localStats->rating = $newStats->rating;
        $localStats->count_rating = $newStats->count_rating;
        $localStats->count_comments = $newStats->count_comments;

        $this->localStatsRepository->store($localStats);
    }
}