<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 20.11.2019
 * Time: 19:08
 */

namespace App\Services;

use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class QrCodeFileGenerate
{
    protected $qrCodeGenerate;

    public function __construct()
    {
        $this->qrCodeGenerate = new BaconQrCodeGenerator();
    }

    public function generateFile(string $code)
    {
        $format = config('qrcode.format');
        $code = str_replace(' ','_', $code);

        $path = config('qrcode.storage_path').$code.'.'.$format;

        $this->qrCodeGenerate
            ->format($format)
            ->margin(0)
            ->size(config('qrcode.size'))
            ->generate($code, storage_path('app/public/'.$path));

        return $path;
    }
}