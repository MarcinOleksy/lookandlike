<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 16.11.2019
 * Time: 13:02
 */

namespace App\Services;

use App\Repositories\ImageRepository;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use App\Models\Image as ImageModel;

class ImageSaveFile
{
    protected $imageRepository;

    public function __construct(
        ImageRepository $imageRepository
    )
    {
        $this->imageRepository = $imageRepository;
    }

    public function create(UploadedFile $image):ImageModel
    {
        $configOriginal = config('image_size.original_image');
        $configThumbnail = config('image_size.thumbnail_image');

        $filename = time();

        $filename_original = $filename.'-original-'.str_replace(' ','_',$image->getClientOriginalName());
        $filename_thumbnail = $filename.'-thumbnail-'.str_replace(' ','_',$image->getClientOriginalName());

        $location_original = $configOriginal['storage_path'].$filename_original;
        $location_thumbnail = $configThumbnail['storage_path'].$filename_thumbnail;

        $path = 'app/public/';

        Image::make($image->getRealPath())->resize($configOriginal['width'], $configOriginal['height'])->save(storage_path($path.$location_original));
        Image::make($image->getRealPath())->resize($configThumbnail['width'], $configThumbnail['height'])->save(storage_path($path.$location_thumbnail));

        $imageModel = new ImageModel();
        $imageModel->original_image_path = $location_original;
        $imageModel->thumbnail_image_path = $location_thumbnail;

        $imageModel->save();

        return $imageModel;
    }
}