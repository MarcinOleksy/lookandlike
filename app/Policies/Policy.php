<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 04.04.2020
 * Time: 23:08
 */

namespace App\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class Policy
{
    use HandlesAuthorization;

    protected $auth;

    protected $allow = [];
    protected $deny = [];

    protected $routeParameters = [];

    public function __construct()
    {
        $this->auth = Auth::user();
    }

    public function setRouteParameters(array $routeParameters)
    {
        $this->routeParameters = $routeParameters;
        return $this;
    }

    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    protected function checkIsAuth()
    {
        return Auth::guard()->check();
    }

    protected function getAuthUserAclGroupSlug()
    {
        return $this->auth->acl_group->slug;
    }

    protected function checkAuthUserIsDeny()
    {
        return in_array($this->auth->acl_group->slug, $this->deny);
    }

    protected function checkAuthUserIsAllow()
    {
        return in_array($this->auth->acl_group->slug, $this->allow);
    }

    protected function checkAuthUserPermission()
    {
        if(!$this->checkIsAuth())
            return false;

        if($this->checkAuthUserIsDeny())
            return false;

        return $this->checkAuthUserIsAllow();
    }
}