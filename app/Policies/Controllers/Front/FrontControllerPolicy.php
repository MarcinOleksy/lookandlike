<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 23.04.2020
 * Time: 15:48
 */

namespace App\Policies\Controllers\Front;


use App\Policies\Policy;

class FrontControllerPolicy extends Policy
{
    protected $allow = ['user'];

    public function main()
    {
        return $this->checkAuthUserPermission();
    }
}