<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.04.2020
 * Time: 00:14
 */

namespace App\Policies\Controllers\Dashboard;


use App\Policies\Policy;

class EventTypeControllerPolicy extends Policy
{
    protected $allow = ['admin'];

    public function main()
    {
        return $this->checkAuthUserPermission();
    }
}