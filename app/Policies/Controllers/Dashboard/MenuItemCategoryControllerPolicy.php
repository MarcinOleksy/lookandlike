<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 09.04.2020
 * Time: 00:15
 */

namespace App\Policies\Controllers\Dashboard;


use App\Policies\Policy;

class MenuItemCategoryControllerPolicy extends Policy
{
    protected $allow = ['admin'];

    public function main()
    {
        return $this->checkAuthUserPermission();
    }
}