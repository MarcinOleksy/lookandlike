<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 07.04.2020
 * Time: 21:01
 */

namespace App\Policies\Controllers\Dashboard;


use App\Policies\Policy;
use App\Repositories\LocalRepository;

class LocalControllerPolicy extends Policy
{
    protected $allow = ['manager', 'admin'];

    public function index()
    {

    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function edit()
    {
        if(!$this->checkAuthUserPermission())
            return false;

        if(!in_array('local_id',$this->getRouteParameters()))
            return true;

        $local_id = $this->getRouteParameters()['local_id'];

        $repo = new LocalRepository();
        $local = $repo->findOrFail($local_id);

        if($local->local_owners->contains('user_id',  $this->auth->id))
            return true;

        return false;
    }

    public function update()
    {

    }

    public function destroy()
    {

    }
}