<?php

namespace App\Policies\Controllers\Dashboard;

use App\Models\User;
use App\Policies\Policy;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserControllerPolicy extends Policy
{
    protected $allow = ['admin'];

    public function main()
    {
        return $this->checkAuthUserPermission();
    }
}
