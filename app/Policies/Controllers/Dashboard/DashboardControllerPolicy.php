<?php
/**
 * Created by PhpStorm.
 * User: Marcin-Komputer
 * Date: 08.04.2020
 * Time: 23:16
 */

namespace App\Policies\Controllers\Dashboard;


use App\Policies\Policy;

class DashboardControllerPolicy extends Policy
{
    protected $allow = ['manager', 'admin'];

    public function main()
    {
        return $this->checkAuthUserPermission();
    }
}