<?php

return [
    'original_image' => [
            'width' => 1200,
            'height' => 800,
            'format' => 'png',
            'storage_path' => 'images/originals/'
        ],
    'thumbnail_image' => [
            'width' => 360,
            'height' => 280,
            'format' => 'png',
            'storage_path' => 'images/thumbnails/'
        ]
];