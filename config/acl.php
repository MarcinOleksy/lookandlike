<?php

return [

    /*
    |--------------------------------------------------------------------------
    |  Models
    |--------------------------------------------------------------------------
    |
    | When using this package, we need to know which Eloquent Model should be used
    | to retrieve your groups and permissions. Of course, it is just the basics models
    | needed, but you can use whatever you like.
    |
     */
    'models' => [
        /*
         | The model you want to use as User Model must use MateusJunges\ACL\Traits\UsersTrait
         */
        'user'       => \App\Models\User::class,

        /*
         | The model you want to use as Permission model must use the MateusJunges\ACL\Traits\PermissionsTrait
         */
        'permission' => Junges\ACL\Http\Models\Permission::class,

        /*
         | The model you want to use as Group model must use the MateusJunges\ACL\Traits\GroupsTrait
         */
        'group'      => Junges\ACL\Http\Models\Group::class,
    ],
    /*
    |--------------------------------------------------------------------------
    | Tables
    |--------------------------------------------------------------------------
    | Specify the basics authentication tables that you are using.
    | Once you required this package, the following tables are
    | created by default when you run the command
    |
    | php artisan migrate
    |
    | If you want to change this tables, please keep the basic structure unchanged.
    |
     */
    'tables' => [
        'groups'                      => 'acl_groups',
        'permissions'                 => 'acl_permissions',
        'users'                       => 'users',
        'group_has_permissions'       => 'acl_group_has_permissions',
        'user_has_permissions'        => 'acl_user_has_permissions',
        'user_has_groups'             => 'acl_user_has_groups',
    ],

    /*
     |
     |If you want to customize your tables, set this flag to "true"
     | */
    'custom_migrations' => true,

    'groups' => [
        'admin' => 'Administrator',
        'manager' => 'Zarządca',
        'user' => 'Użytkownik'
    ],

    'permissions' => [
        'dashboard' => [
            'passwordChange' => [
                'dashboard.passwordChange.showChangePasswordForm' => 'Panel | Zmiana hasła formularz.',
                'dashboard.passwordChange.store' => 'Panel | Zmiana hasła zapisz.',
                'dashboard.passwordChange.success' => 'Panel | Zmiana hasła potwierdzenie.',
            ],
            'accountChange' => [
                'dashboard.accountChange.edit' => 'Panel | Edytuj profil formularz.',
                'dashboard.accountChange.update' => 'Panel | Edytuj profil zapisz.',
            ],
            'locals' => [
                'dashboard.locals.index' => 'Panel | Lista lokali.',
                'dashboard.locals.create' => 'Panel | Utwórz lokalu formularz.',
                'dashboard.locals.store' => 'Panel | Utwórz lokalu zapisz.',
                'dashboard.locals.edit' => 'Panel | Edytuj lokalu formularz.',
                'dashboard.locals.update' => 'Panel | Edytuj lokalu zapisz.',
                'dashboard.locals.delete' => 'Panel | Usuń lokalu eventu.',
            ],
            'locals.menu' => [
                'dashboard.locals.menu.index' => 'Panel | Lista menu lokalu.',
                'dashboard.locals.menu.store' => 'Panel | Utwórz menu lokalu zapisz.',
                'dashboard.locals.menu.delete' => 'Panel | Usuń menu lokalu eventu.',
            ],
            'locals.menu.items' => [
                'dashboard.locals.menu.items.index' => 'Panel | Lista pozycji w menu lokalu.',
                'dashboard.locals.menu.items.create' => 'Panel | Utwórz pozycje w menu lokalu formularz.',
                'dashboard.locals.menu.items.store' => 'Panel | Utwórz pozycje w menu lokalu zapisz.',
                'dashboard.locals.menu.items.edit' => 'Panel | Edytuj pozycje w menu lokalu formularz.',
                'dashboard.locals.menu.items.update' => 'Panel | Edytuj pozycje w menu lokalu zapisz.',
                'dashboard.locals.menu.items.delete' => 'Panel | Usuń pozycje w menu lokalu.',
            ],
            'visitCodes' => [
                'dashboard.visitCodes.index' => 'Panel | Lista kodów visyt lokali.',
                'dashboard.visitCodes.create' => 'Panel | Utwórz kod wizyty formularz.',
                'dashboard.visitCodes.show' => 'Panel | Pokaż kod wizyty zapisz.',
            ],
            'ratings' => [
                'dashboard.ratings.index' => 'Panel | Lista opini lokalu.',
            ],
            'opening_hours' => [
                'dashboard.opening_hours.edit' => 'Panel | Edytuj godziny pracy lokalu formularz.',
                'dashboard.opening_hours.update' => 'Panel | Edytuj godziny pracy lokalu zapisz.',
            ],
            'localsTypes' => [
                'dashboard.localsTypes.index' => 'Panel | Lista typów lokali.',
                'dashboard.localsTypes.create' => 'Panel | Utwórz typ lokalu formularz.',
                'dashboard.localsTypes.store' => 'Panel | Utwórz typ lokalu zapisz.',
                'dashboard.localsTypes.edit' => 'Panel | Edytuj typ lokalu formularz.',
                'dashboard.localsTypes.update' => 'Panel | Edytuj typ lokalu zapisz.',
                'dashboard.localsTypes.delete' => 'Panel | Usuń typ lokalu eventu.',
            ],
            'menuItemsCategories' => [
                'dashboard.menuItemsCategories.index' => 'Panel | Lista kategorii pozycji z menu.',
                'dashboard.menuItemsCategories.create' => 'Panel | Utwórz kategorie pozycji z menu formularz.',
                'dashboard.menuItemsCategories.store' => 'Panel | Utwórz kategorie pozycji z menu zapisz.',
                'dashboard.menuItemsCategories.delete' => 'Panel | Usuń kategoria pozycji z menu eventu.',
            ],
            'events' => [
                'dashboard.events.index' => 'Panel | Lista eventów.',
                'dashboard.events.create' => 'Panel | Utwórz event formularz.',
                'dashboard.events.store' => 'Panel | Utwórz event zapisz.',
                'dashboard.events.edit' => 'Panel | Edytuj event formularz.',
                'dashboard.events.update' => 'Panel | Edytuj event zapisz.',
                'dashboard.events.delete' => 'Panel | Usuń event eventu.',
            ],
            'eventsPeriodic' => [
                'dashboard.eventsPeriodic.index' => 'Panel | Lista eventów cyklicznych.',
                'dashboard.eventsPeriodic.create' => 'Panel | Utwórz event cykliczny formularz.',
                'dashboard.eventsPeriodic.store' => 'Panel | Utwórz event cykliczny zapisz.',
                'dashboard.eventsPeriodic.edit' => 'Panel | Edytuj event cykliczny formularz.',
                'dashboard.eventsPeriodic.update' => 'Panel | Edytuj event cykliczny zapisz.',
                'dashboard.eventsPeriodic.delete' => 'Panel | Usuń event cykliczny eventu.',
            ],
            'eventsTypes' => [
                'dashboard.eventsTypes.index' => 'Panel | Lista typów eventu.',
                'dashboard.eventsTypes.create' => 'Panel | Utwórz typ eventu formularz.',
                'dashboard.eventsTypes.store' => 'Panel | Utwórz typ eventu zapisz.',
                'dashboard.eventsTypes.edit' => 'Panel | Edytuj typ eventu formularz.',
                'dashboard.eventsTypes.update' => 'Panel | Edytuj typ eventu zapisz.',
                'dashboard.eventsTypes.delete' => 'Panel | Usuń typ eventu.',
            ],
            'users' => [
                'dashboard.users.index' => 'Panel | Lista użytkowników.',
                'dashboard.users.create' => 'Panel | Utwórz użytkownika formularz.',
                'dashboard.users.store' => 'Panel | Utwórz użytkowników zapisz.',
                'dashboard.users.edit' => 'Panel | Edytuj użytkownika formularz.',
                'dashboard.users.update' => 'Panel | Edytuj użytkownika zapisz.',
                'dashboard.users.delete' => 'Panel | Usuń użytkownika.',
            ]
        ],

        'front' => [
            'logout' => [
                'front.logout' => 'Front | Wyloguj się.'
            ],
            'passwordChange' => [
                'front.passwordChange.showChangePasswordForm' => 'Front | Zmiana hasła formularz.',
                'front.passwordChange.store' => 'Front | Zmiana hasła zapisz.',
                'front.passwordChange.success' => 'Front | Zmiana hasła potwierdzenie.',
            ],
            'accountChange' => [
                'front.accountChange.edit' => 'Front | Edytuj profil formularz.',
                'front.accountChange.update' => 'Front | Edytuj profil zapisz.',
            ],
            'locals' => [
                'front.locals.index' => 'Front | Lista lokali.',
                'front.locals.show' => 'Front | Pokaż lokal.',
            ],
            'locals.visit_codes' => [
                'dashboard.locals.visit_codes.showFormVisitCode' => 'Front | Kod wizyty formularz.',
                'dashboard.locals.visit_codes.showRatingForm' => 'Front | Weryfikacja kodu i wyświetlenie formularza opini.',
            ],
            'locals.ratings' => [
                'dashboard.locals.ratings.index' => 'Panel | Lista opinie lokalu.',
                'dashboard.locals.ratings.create' => 'Panel | Utwórz opinie lokalu formularz.',
                'dashboard.locals.ratings.store' => 'Panel | Utwórz opinie lokalu zapisz.',
            ],
            'events' => [
                'front.events.index' => 'Front | Lista eventów.',
                'front.events.show' => 'Front | Pokaż event.',
            ]
        ]
    ],

    'groups_permissions' => [
        'admin' => [
            'dashboard.passwordChange.showChangePasswordForm',
            'dashboard.passwordChange.store',
            'dashboard.passwordChange.success',

            'dashboard.accountChange.edit',
            'dashboard.accountChange.update',

            'dashboard.locals.index',
            'dashboard.locals.create',
            'dashboard.locals.store',
            'dashboard.locals.edit',
            'dashboard.locals.update',
            'dashboard.locals.delete',

            'dashboard.locals.menu.index',
            'dashboard.locals.menu.store',
            'dashboard.locals.menu.delete',

            'dashboard.locals.menu.items.index',
            'dashboard.locals.menu.items.create',
            'dashboard.locals.menu.items.store',
            'dashboard.locals.menu.items.edit',
            'dashboard.locals.menu.items.update',
            'dashboard.locals.menu.items.delete',

            'dashboard.visitCodes.index',
            'dashboard.visitCodes.create',
            'dashboard.visitCodes.show',

            'dashboard.ratings.index',

            'dashboard.opening_hours.edit',
            'dashboard.opening_hours.update',

            'dashboard.localsTypes.index',
            'dashboard.localsTypes.create',
            'dashboard.localsTypes.store',
            'dashboard.localsTypes.edit',
            'dashboard.localsTypes.update',
            'dashboard.localsTypes.delete',

            'dashboard.menuItemsCategories.index',
            'dashboard.menuItemsCategories.create',
            'dashboard.menuItemsCategories.store',
            'dashboard.menuItemsCategories.delete',

            'dashboard.events.index',
            'dashboard.events.create',
            'dashboard.events.store',
            'dashboard.events.edit',
            'dashboard.events.update',
            'dashboard.events.delete',

            'dashboard.eventsPeriodic.index',
            'dashboard.eventsPeriodic.create',
            'dashboard.eventsPeriodic.store',
            'dashboard.eventsPeriodic.edit',
            'dashboard.eventsPeriodic.update',
            'dashboard.eventsPeriodic.delete',

            'dashboard.eventsTypes.index',
            'dashboard.eventsTypes.create',
            'dashboard.eventsTypes.store',
            'dashboard.eventsTypes.edit',
            'dashboard.eventsTypes.update',
            'dashboard.eventsTypes.delete',

            'dashboard.users.index',
            'dashboard.users.create',
            'dashboard.users.store',
            'dashboard.users.edit',
            'dashboard.users.update',
            'dashboard.users.delete',
        ],
        'manager' => [
            'dashboard.passwordChange.showChangePasswordForm',
            'dashboard.passwordChange.store',
            'dashboard.passwordChange.success',

            'dashboard.accountChange.edit',
            'dashboard.accountChange.update',

            'dashboard.locals.index',
            'dashboard.locals.create',
            'dashboard.locals.store',
            'dashboard.locals.edit',
            'dashboard.locals.update',
            'dashboard.locals.delete',

            'dashboard.locals.menu.index',
            'dashboard.locals.menu.store',
            'dashboard.locals.menu.delete',

            'dashboard.locals.menu.items.index',
            'dashboard.locals.menu.items.create',
            'dashboard.locals.menu.items.store',
            'dashboard.locals.menu.items.edit',
            'dashboard.locals.menu.items.update',
            'dashboard.locals.menu.items.delete',

            'dashboard.visitCodes.index',
            'dashboard.visitCodes.create',
            'dashboard.visitCodes.show',

            'dashboard.ratings.index',

            'dashboard.opening_hours.edit',
            'dashboard.opening_hours.update',

            'dashboard.menuItemsCategories.index',
            'dashboard.menuItemsCategories.create',
            'dashboard.menuItemsCategories.store',
            'dashboard.menuItemsCategories.delete',

            'dashboard.events.index',
            'dashboard.events.create',
            'dashboard.events.store',
            'dashboard.events.edit',
            'dashboard.events.update',
            'dashboard.events.delete',

            'dashboard.eventsPeriodic.index',
            'dashboard.eventsPeriodic.create',
            'dashboard.eventsPeriodic.store',
            'dashboard.eventsPeriodic.edit',
            'dashboard.eventsPeriodic.update',
            'dashboard.eventsPeriodic.delete',
        ],
        'user' => [
            'front.logout',
            'front.passwordChange.showChangePasswordForm',
            'front.passwordChange.store',
            'front.passwordChange.success',
            'front.accountChange.edit',
            'front.accountChange.update',
            'front.locals.index',
            'front.locals.show',
            'dashboard.locals.visit_codes.showFormVisitCode',
            'dashboard.locals.visit_codes.showRatingForm',
            'dashboard.locals.ratings.index',
            'dashboard.locals.ratings.create',
            'dashboard.locals.ratings.store',
            'front.events.index',
            'front.events.show'
        ]
    ]
];
