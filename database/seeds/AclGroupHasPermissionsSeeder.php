<?php

use Illuminate\Database\Seeder;
use Junges\ACL\Http\Models\Group;

class AclGroupHasPermissionsSeeder extends Seeder
{
    protected $configPath = 'acl';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loadedPermissions = \Junges\ACL\Http\Models\Permission::query()->get();
        $configGroupsPermissions = config($this->configPath.'.groups_permissions');

        if(count($configGroupsPermissions))
            foreach ($configGroupsPermissions as $groupSlug => $permissionsSlugs)
            {
                try {
                    /** @var Group $group */
                    $group = Group::query()->where('slug', $groupSlug)->firstOrFail();
                    $group->revokeAllPermissions();
                    $group->assignPermissions($loadedPermissions->whereIn('slug', $permissionsSlugs)->all());
                } catch (Exception $exception)
                {
                    var_dump($exception->getMessage());
                }
            }
    }
}
