<?php

use Illuminate\Database\Seeder;

class AclPermissionsSeeder extends Seeder
{
    protected $permissionsConfigPath = '';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = config('acl.permissions');

        if(count($permissions))
            foreach ($permissions as $systemKey => $systemPermissions)
                foreach ($systemPermissions as $permissionKey => $permissionRoutes)
                    foreach ($permissionRoutes as $permissionSlug => $permissionName)
                        try {
                            \Junges\ACL\Http\Models\Permission::query()->create([
                                'slug' => $permissionSlug,
                                'name' => $permissionName
                            ]);
                        } catch(Exception $exception){
                            var_dump($exception->getMessage());
                        }
    }
}
