<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->aclPermissions();
    }

    public function aclPermissions()
    {
        $this->call(AclGroupsSeeder::class);
        $this->call(AclPermissionsSeeder::class);
        $this->call(AclGroupHasPermissionsSeeder::class);
        $this->call(UsersDefaultSeeder::class);
    }
}
