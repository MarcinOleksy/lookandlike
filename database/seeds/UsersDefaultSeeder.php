<?php

use Illuminate\Database\Seeder;

class UsersDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminGroup = \Junges\ACL\Http\Models\Group::query()->where('slug','admin')->firstOrFail();

        try {
            $user = \App\Models\User::query()->create([
                'name' => 'Marcin Oleksy',
                'email' => 'marcinoleksy94@gmail.com',
                'password' => '$2y$10$wvg3JQ981sEd9jpqaY/GDOpOrG2IqTnr1ByrdBBfYzk8rirAw4Kke'
            ]);

            $user->assignGroup([$adminGroup]);
        } catch(Exception $exception){
            var_dump($exception->getMessage());
        }


    }
}
