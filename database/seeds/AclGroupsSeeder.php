<?php

use Illuminate\Database\Seeder;

class AclGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = config('acl.groups');

        foreach ($groups as $groupSlug => $groupName)
            try {
                \Junges\ACL\Http\Models\Group::query()->create([
                    'slug' => $groupSlug,
                    'name' => $groupName
                ]);
            } catch(Exception $exception){
                var_dump($exception->getMessage());
            }
    }
}
