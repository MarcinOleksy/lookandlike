<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locals_ratings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('local_id');
            $table->unsignedBigInteger('code_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->enum('rating', array(1,2,3,4,5,6,7,8,9,10));
            $table->text('comment',500)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('local_id')
                ->on('locals')
                ->references('id');

            $table->foreign('code_id')
                ->on('locals_visit_codes')
                ->references('id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('image_id')
                ->references('id')
                ->on('images');

            $table->unique(array('local_id','code_id'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locals_ratings');
    }
}

