<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsPeriodicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_periodic', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('local_id');
            $table->unsignedBigInteger('image_id');
            $table->string('title',255);
            $table->text('description');
            $table->enum('status', ['implemented','suspended'])->default('implemented');
            $table->enum('day_of_week', ['1','2','3','4','5','6','0'])->nullable()->comment('1=monday; 2=tuesday; 3=wednesday; 4=thursday; 5=friday; 6=saturday; 0=sunday');
            $table->time('time_start');
            $table->time('duration');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('type_id')
                ->references('id')
                ->on('events_types');

            $table->foreign('local_id')
                ->references('id')
                ->on('locals');

            $table->foreign('image_id')
                ->references('id')
                ->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_periodic');
    }
}
