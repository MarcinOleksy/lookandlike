<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('local_stats_id');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->string('name');
            $table->text('description');
            $table->string('address');
            $table->decimal('latitude',30,15);
            $table->decimal('longitude',30,15);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('local_stats_id')
                ->references('id')
                ->on('locals_stats');

            $table->foreign('type_id')
                ->references('id')
                ->on('locals_types');

            $table->foreign('image_id')
                ->references('id')
                ->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locals');
    }
}
