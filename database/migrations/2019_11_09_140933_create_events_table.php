<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_periodic_id')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('local_id');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->string('title',255);
            $table->text('description')->nullable();
            $table->text('program')->nullable();
            $table->enum('status', ['prepared','started','completed'])->default('prepared');
            $table->dateTime('start_at');
            $table->dateTime('finish_at');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('event_periodic_id')
                ->references('id')
                ->on('events_periodic');

            $table->foreign('type_id')
                ->references('id')
                ->on('events_types');

            $table->foreign('local_id')
                ->references('id')
                ->on('locals');

            $table->foreign('image_id')
                ->references('id')
                ->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
