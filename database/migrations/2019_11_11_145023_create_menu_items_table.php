<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('menu_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->text('components')->nullable();
            $table->decimal('price', 4,2);

            $table->foreign('menu_id')
                ->references('id')
                ->on('menu');

            $table->foreign('category_id')
                ->references('id')
                ->on('menu_item_categories');

            $table->foreign('image_id')
                ->references('id')
                ->on('images');

            $table->unique(array('menu_id','category_id','name'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
