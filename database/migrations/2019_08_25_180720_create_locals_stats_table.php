<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locals_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('rating',2,1)->default(0);
            $table->integer('count_rating')->default(0);
            $table->integer('count_comments')->default(0);
            $table->integer('count_followers')->default(0);
            $table->integer('count_images')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locals_stats');
    }
}
